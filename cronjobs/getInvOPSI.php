<?php

// veraltet mittlerweile, mysql-driver und sql-queries muessten entsprechend getInvEmpirum.php angepasst werden
// Script fuellen mit Daten aus Inventar OPSI-DB nach Tabelle inventar lizenzDB
// schachi 2014-06-30

  $db1 = mysql_connect('<host>','<user>','<passwort>');
  if ( !$db1 ) {
    die ('Error connecting to the database' . mysql_error($db1));
  }
  mysql_select_db( 'opsi', $db1 ) or die("Could not select DB: ".mysql_error($db1));

  //$sql = "SELECT ProductNameShort AS software, InvSoftware.Developer AS publisher, InvSoftware.VERSION AS version, InvComputer.Computername AS instName, InvComputer.InvDate AS instDate FROM InvSoftware JOIN InvComputer ON InvSoftware.client_id = InvComputer.client_id WHERE InvComputer.Computername like 'vm-pengo' OR InvComputer.Computername = 'vm-schachi' OR InvComputer.Computername = 'packager'";

  $sql = "SELECT DISTINCT clientId FROM `SOFTWARE_CONFIG`";
  //$sql = "SELECT DISTINCT clientId FROM `SOFTWARE_CONFIG` WHERE clientId = 'vm-schachi.mpi-magdeburg.mpg.de'";
  $query1 = mysql_query($sql, $db1) or die(mysql_error());
  $count1 = mysql_num_rows($query1);
  //echo $count1.' PC\'s'."\n";
  if ( $count1 >= 1 ) {
    $db2 = mysql_connect('localhost','<user>','<password>');
    if ( !$db2 ) {
      die ('Error connecting to the database' . mysql_error($db2));
    }
    mysql_select_db( 'mpidb_it_licman', $db2 ) or die("Could not select DB: ".mysql_error($db2));
    $sql = "DELETE FROM `mpi_inventar` WHERE `lock` = '0'";
    mysql_query($sql, $db2) or die(mysql_error());

    while ($result1 = mysql_fetch_assoc($query1)) {
      $comp = $result1['clientId'];
      $sql = "SELECT name AS software, version, clientId AS instName, firstseen AS instDate, licenseKey AS instKey FROM `SOFTWARE_CONFIG` WHERE clientId = '$comp'";
      $query2 = mysql_query($sql, $db1) or die(mysql_error());
      $count2 = mysql_num_rows($query2);
      if ( $count2 >= 1 ) {
        $sql = array(); 
        $row = mysql_fetch_assoc($query2);
        while ($row = mysql_fetch_assoc($query2)) {
          $sonder    = array("'","\\");
          $software  = str_replace($sonder, "", $row['software']);
          $instName  = strstr($row['instName'], '.', true);
          //echo '('.$software.', '.$instName.', '.$row['instDate'].', '.$row['instKey'].', \'empirum\', CURRENT_TIMESTAMP)'."\n";
          $sql[] = '(\''.$software.'\', \''.$instName.'\', \''.$row['instDate'].'\', \''.$row['instKey'].'\', \'opsi\', CURRENT_TIMESTAMP)';
        }
        mysql_query('INSERT IGNORE INTO `mpi_inventar` (`software`, `instName`, `instDate`, `instKey`, `bearbeiter`, `zeitstempel`) VALUES '.implode(',', $sql), $db2);
        //echo ('INSERT IGNORE INTO `mpi_inventar` (`software`, `publisher`, `instName`, `bearbeiter`, `zeitstempel`) VALUES '.implode(',', $sql));
        //echo $comp.' importiert mit '.$count2.' Eintraegen'."\n";
      }
    mysql_free_result($query2);
    }  
    // schreibe alle Results aus suchFilter nach mpi_inventar, view dauert sonst einfach zu lange zum anzeigen
    $sql = <<<EOT
  UPDATE mpi_inventar AS inv
   INNER JOIN con_softInv AS con ON
    status = '1' AND (
    ((searchEn = '1') AND (versionEn = '0') AND (searchCmd = '0') AND (inv.software LIKE searchStr)) OR
    ((searchEn = '1') AND (versionEn = '1') AND (searchCmd = '0') AND (inv.software LIKE searchStr) AND (inv.version = con.version)) OR
    ((searchEn = '1') AND (versionEn = '0') AND (searchCmd = '1') AND (inv.software RLIKE IF(searchStr = '',NULL,searchStr))) OR
    ((searchEn = '1') AND (versionEn = '1') AND (searchCmd = '1') AND (inv.software RLIKE IF(searchStr = '',NULL,searchStr)) AND (inv.version = con.version)) OR
    ((searchEn = '0') AND (versionEn = '0') AND (inv.software = con.software)) OR
    ((searchEn = '0') AND (versionEn = '1') AND (inv.software = con.software) AND (inv.version = con.version))
    )
  SET inv.conID = con.conID, inv.softID = con.softID, inv.verID = con.verID, inv.lizenzID = con.lizenzID, inv.keyID = con.keyID;
EOT;
    mysql_query($sql) or die(mysql_error($db2));
    mysql_close($db2);
  }
  //echo $count1.' PC\'s'."\n";
  mysql_free_result($query1);
  mysql_close($db1);

?>
