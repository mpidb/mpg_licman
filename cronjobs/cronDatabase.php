<?php

// Cronjob Skript fuer Aktionen ausserhalb der DB
// z.B. Mailbenachrichtigung, Loeschen verwaister Ablagen etc.
// schachi 2017-01-19

  // stelle sicher das dieses Skript in einem Subdir liegt, normalerweise im Ordner cronjobs, sonst gibt es kausale Problem :-(
  chdir(__DIR__);
  chdir('../');
  //print_r (realpath(__DIR__).' '.getcwd()."\n");
  if (!is_readable('conf.ini') ) trigger_error ('Error loading config file from here '.getcwd()."\n");

  $conf = array();
  $conf = parse_ini_file('conf.ini', true);
  //print_r ($conf);
  if ( !isset( $conf['_database'] ) ) trigger_error ('Error loading config file. No database specified.');
  $dbinfo =& $conf['_database'];
  if ( !is_array( $dbinfo ) || !isset($dbinfo['host']) || !isset( $dbinfo['user'] ) || !isset( $dbinfo['password'] ) || !isset( $dbinfo['name'] ) ) {
    trigger_error ('Error loading config file.  The database information was not entered correctly.');
  }
  $db = mysqli_connect($dbinfo['host'], $dbinfo['user'], $dbinfo['password'], $dbinfo['name'] );
  if ( !$db ) trigger_error ('Failed to connect to MySQL database: '.mysqli_connect_error($db)."\n");

  $debug = 0;  // 1 = ausgabe und keine Mail an User
  $mailto = 0; // nur bei Debug = 1 aktiv, 0 = keine Mail senden
  //$mailto = 'schachi@mpi-magdeburg.mpg.de'; // nur bei Debug = 1 aktiv, Mail an User $mailto


  // url
  $url = 'http';
  if ( isset($conf['_own']['ssl'])) {
    if ($conf['_own']['ssl'] == 1) $url = 'https';
  }
  $path = (basename(realpath('./')));
  $host = shell_exec("hostname -f | tr -d '\n'");
  $url  = $url.'://'.$host.'/'.$path;
  if ($debug) print_r($url."\n");


  // Script fuer das berechnen aller versionierten Softwareversionen
  // schreibt das Ergebnis in die Tabelle show_version
  // lege alle DS an, welche versioniert sind und noch nicht existieren
  // schachi 2016-02-15
  $sql = "INSERT IGNORE INTO show_version (verID) SELECT DISTINCT verID FROM con_verLic WHERE 1";
  mysqli_query($db, $sql) OR trigger_error ('Query failed: '.mysqli_error($db)."\n");
  // hole von jeder software eine version
  $sql = "SELECT ver.verID FROM mpi_version AS ver INNER JOIN con_verLic AS con ON con.verID = ver.verID GROUP BY softID";
  $result = mysqli_query($db, $sql) OR trigger_error ('Query failed: '.mysqli_error($db)."\n");
  $count = mysqli_num_rows($result);
  if ( $count >= 1 ) {
    while($row = mysqli_fetch_assoc($result)) {
      $softID = $row['verID'];
      $sql = "CALL proc_version($softID)";
      if ($debug) echo "$sql\n";
      mysqli_query($db, $sql) OR trigger_error ('Query failed: '.mysqli_error($db)."\n");
    }
  }


  // loesche alle <table>__history, welche nicht erwuenscht sind
  // Xataface hat nur einen globalen Schalter ON/OFF fuer history, aber wer braucht denn alle histories?
  $sql = "SELECT reiter FROM view_reiter WHERE reiter IN (SELECT CONCAT(lst.reiter, '__history') AS table_his FROM list_reiter AS lst LEFT JOIN view_reiter AS vReit ON lst.reiter = vReit.reiter WHERE lst.history = '0' AND vReit.table_type = 'BASE TABLE' AND lst.reiter NOT LIKE '%__history') AND table_type = 'BASE TABLE';";
  $result = mysqli_query($db, $sql) OR trigger_error ('Query failed: '.mysqli_error($db)."\n");
  $count = mysqli_num_rows($result);
  if ( $count >= 1 ) {
    while($row = mysqli_fetch_assoc($result)) {
      $table = $row['reiter'];
      $sql = "DROP TABLE IF EXISTS $table;"; 
      if ($debug) echo "$sql\n";
      mysqli_query($db, $sql) OR trigger_error ('Query failed: '.mysqli_error($db)."\n");
    }
  }


  // loesche alle Ablagen, welche keine Verbindung mehr haben
  $sql = "DELETE FROM mpi_ablage WHERE softID IS NULL AND vertragID IS NULL AND lizenzID IS NULL AND srvID IS NULL;";
  mysqli_query($db, $sql) OR trigger_error ('Query failed: '.mysqli_error($db)."\n");
  $sql = "DELETE mpi_notiz FROM mpi_notiz WHERE softID IS NULL AND lizenzID IS NULL;";
  mysqli_query($db, $sql) OR trigger_error ('Query failed: '.mysqli_error($db)."\n");


  // Script fuer automatisches senden von emails, wenn Ablaufdatum naht.
  // schachi 2017-01-19 V1.3
  function sendMail($db, $pre, $betr, $delay, $base, $url, $debug, $mailto) {
    switch ($betr) {
      case 'Support':
      case 'Lizenz':
        $table = 'mpi_lizenz';
        $field = 'lizenzID';
        break;
      case 'Vertrag':
        $table = 'mpi_vertrag';
        $field = 'vertragID';
        $ende  = 'heute!';
        break;
    default:
      trigger_error ('Error choice subject.');
    }
    $sql = <<<EOT
  SELECT name, lizenzID, vertragID, email, expireDate, anz, body
  FROM view_sendMail
  WHERE ( expireDate = ADDDATE( CURDATE(), $delay )) AND ( body = '$betr' );
EOT;
    $result = mysqli_query($db, $sql) OR trigger_error ('Query failed: '.mysqli_error($db)."\n");
    $count = mysqli_num_rows($result);
    if ($debug) print_r ("\n$sql\n$count $pre $delay\n");
    if ( $count >= 1 ) {
      while($row = mysqli_fetch_array($result)) {
        $tabID = $row[$field];
        $name  = utf8_encode ($row['name']);
        $datum = $row['expireDate'];
        $anz   = $row['anz'];
        $mail  = $row['email'];
        if ( $delay > 0 ) $ende = 'in '.$delay.' Tagen.'; else $ende = 'heute!';
        $body  = $pre.$betr.' "'.$name.'" endet '.$ende;
        $text  = "$betr: $name\nAblaufdatum: $datum\nStatus: $pre\nAnzahl verbundener Lizenzen: $anz\n";
        $link  = "Link: $url/index.php?-table=$table&-action=browse&$field=$tabID\n";
        $head  = "From: Database ".$base." <".$mail.">\n";
        $head .= "Content-Type: text/plain; charset=utf-8\n";
        $head .= "MIME-Version: 1.0\n";
        if ($debug) {
          print_r( "$mail\n$body\n${text}${link}\n$head\n" );
          if ($mailto != '0') mail( $mailto, $body, $text.$link, $head );
        } else {
          mail( $mail, $body, $text.$link, $head );
        }
      }
    }
  }

  if (!isset( $conf['_own']['notify'] )) $delay = 30; else $delay = $conf['_own']['notify'];
  if (!isset( $conf['_own']['dn'] )) $dn = 'mpi-magdeburg.mpg.de'; else $dn = $conf['_own']['dn'];
  $base = $dbinfo['name'];
  $betr = array('Support','Lizenz','Vertrag');
  foreach ($betr as $body) {
    sendMail( $db, '[PREINFO]',  $body, '90',   $base, $url, $debug, $mailto );
    sendMail( $db, '[INFO] ',    $body, $delay, $base, $url, $debug, $mailto );
    sendMail( $db, '[WARNUNG] ', $body, '7',    $base, $url, $debug, $mailto );
    sendMail( $db, '[WICHTIG] ', $body, '0',    $base, $url, $debug, $mailto );
  }


  mysqli_close($db);

?>
