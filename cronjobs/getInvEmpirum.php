<?php

// Script fuellen mit Daten aus Inventar Empirum-DB nach Tabelle inventar lizenzDB
// schachi 2014-06-30 V1.0 - begin
// schachi 2016-06-02 V1.1 - update mysqli driver
// schachi 2016-06-07 V1.2 - update mssqli driver, debug/verbose - switch
// schachi 2016-06-29 V1.3 - update mssqli error

  // requirements: php7.0-sybase
  $dbmy = mysqli_connect('localhost', 'debian-sys-maint', '<password>', 'mpidb_mpg_licman');
  if ( !$dbmy ) trigger_error ('Failed to connect to MySQL database: '.mysqli_connect_error($dbmy)."\n");

  $hostname = "192.168.20.87";
  $port = 1433;
  $dbname = "<empDatabase>";
  $username = "<mpidb>";
  $pw = "<password>";
  $debug = 0;
  $verbose = 0;
  $key = 0;

  try {
    $dbms = new PDO ("dblib:host=$hostname:$port;dbname=$dbname","$username","$pw");
  }
  catch (PDOException $pe) {
    trigger_error ("Failed to get DB handle：" . $pe->getMessage() . "\n");
  }
  $sqlpc = "SELECT Computername FROM dbo.InvComputer";
  // test nur einen pc importieren
  //$sqlpc = "SELECT Computername FROM dbo.InvComputer WHERE Computername = 'EMPIRUM'";
  // Prepare-Statement - wird spaeter mit Computer ergaenzt
  $sqlsw = <<<EOT
    SELECT
      OperatingSystem AS software, 'Microsoft' AS publisher, '--' AS version, Computername AS instName, InvDate AS instDate
    FROM
      InvComputer
    WHERE
      Computername = :computer
    UNION
    SELECT
      soft.ProductNameShort AS software, soft.Developer AS publisher, soft.Version AS version, comp.Computername AS instName, comp.InvDate AS instDate
    FROM
      InvSoftware AS soft
      JOIN InvComputer AS comp ON soft.client_id = comp.client_id WHERE comp.Computername = :computer AND soft.ProductNameShort NOT LIKE '%hotfix%' AND soft.ProductNameShort NOT LIKE '%Update for %';
EOT;

  $stpc = $dbms->query($sqlpc);
  $rowpc = $stpc->fetchAll(PDO::FETCH_ASSOC);
  $cntpc = count($rowpc);
  if ($debug) echo "Anzahl Computer: $cntpc\n";

  if ( $cntpc > 0 ) {
    //print_r($rowpc);   //[643] => Array ( [Computername] => WIN7-TEST1 )

    // wenn Daten vorhanden, loesche alle letzten Inventories
    $sqldel = "DELETE FROM `mpi_inventar` WHERE `bearbeiter` = 'empirum'";
    mysqli_query($dbmy, $sqldel) OR trigger_error ('Query delete Inventar failed: '.mysqli_error($dbmy)."\n");

    // hole Software fuer jeden Computer
    foreach ($rowpc as $keypc => $column) {
      $key++;
      $comp = strtolower(preg_replace('/\s\s+/', '', $column['Computername']));
      if ($debug) echo $comp.' '.$keypc."\n";
      $stsw = $dbms->prepare($sqlsw);
      $stsw->execute(array(':computer' => $comp));
      $exsw = $stsw->rowCount();
      if ($debug) echo "RowCount Computer: $exsw\n";
      if ( $exsw != 0 ) {
        $cntsw = 0;
        while ($rowsw = $stsw->fetch(PDO::FETCH_ASSOC)) {
          $cntsw++;
          $sonder = array("'","\\");
          $software = str_replace($sonder, "", $rowsw['software']);
          $version   = $rowsw['version'];
          $publisher = $rowsw['publisher'];
          $instName  = strtolower(preg_replace('/\s\s+/', '', $rowsw['instName']));
          // echo '('.$software.', '.$version.', '.$publisher.', '.$instName.', '.$rowsw['instDate'].', \'empirum\')'."\n";
          $sqlins[] = '(\''.$software.'\', \''.$version.'\', \''.$publisher.'\', \''.$instName.'\', \'empirum\')';

          if ($verbose) echo "$comp - $software - $cntsw - $keypc\n";
        }
        mysqli_query($dbmy, 'INSERT IGNORE INTO `mpi_inventar` (`software`, `version`, `publisher`, `instName`, `bearbeiter`) VALUES '.implode(',', $sqlins)) OR trigger_error ('Query insert Inventar failed: '.mysqli_error($dbmy)."\n");
        // echo ('INSERT IGNORE INTO `mpi_inventar` (`software`, `version`, `publisher`, `instName`, `bearbeiter`) VALUES '.implode(',', $sqlins));
      }
      if ($debug) echo "$cntsw importierte Datensaetze fuer Computer: $comp\n";
    }
    unset($stsw);
    if ($debug) echo "Anzahl Computer in Empirum: $cntpc \n";

    // eigentlich per cronjob, aber sicher ist sicher
    // schreibe alle Results aus suchFilter nach mpi_inventar, view dauert sonst einfach zu lange zum anzeigen
    $sql = <<<EOT
    UPDATE mpi_inventar AS inv
    INNER JOIN con_softInv AS con ON
      status = '1' AND (
      ((searchEn = '1') AND (versionEn = '0') AND (searchCmd = '0') AND (inv.software LIKE searchStr)) OR
      ((searchEn = '1') AND (versionEn = '1') AND (searchCmd = '0') AND (inv.software LIKE searchStr) AND (inv.version = con.version)) OR
      ((searchEn = '1') AND (versionEn = '0') AND (searchCmd = '1') AND (inv.software RLIKE IF(searchStr = '',NULL,searchStr))) OR
      ((searchEn = '1') AND (versionEn = '1') AND (searchCmd = '1') AND (inv.software RLIKE IF(searchStr = '',NULL,searchStr)) AND (inv.version = con.version)) OR
      ((searchEn = '0') AND (versionEn = '0') AND (inv.software = con.software)) OR
      ((searchEn = '0') AND (versionEn = '1') AND (inv.software = con.software) AND (inv.version = con.version))
    )
    SET inv.conID = con.conID, inv.softID = con.softID, inv.verID = con.verID, inv.lizenzID = con.lizenzID, inv.keyID = con.keyID;
EOT;
    mysqli_query($dbmy, $sql) OR trigger_error ('Query update Inventory failed: '.mysqli_error($dbmy)."\n");
   
  }

  unset($stpc);
  unset($dbms);
  mysqli_close($dbmy);

?>
