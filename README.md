## Einleitung ##

Diese Lizenzverwaltung ist die Ursache diese Projektes. Sie war auch die aufwendigste und zeitintensivste Datenbank waehrend der Erstellung. Alle anderen DB's wurden step-by-step auf diese gleiche Plattform portiert.<br>
Eine ausfuehrliche Doku existiert auf dem Init-Wiki <https://wiki.init.mpg.de/share/Lizenzverwaltungssoftware/xataface><br>
<br>
Hier nur ein paar Stichpunkte:
* Erfassung von Software, Versionen, Lizenzen, Vereinbarungen, LizenzSchluessel, Installationen, Lizenzmodelle, Kosten, Dateiablagen, Weblinks, Nutzrechten, Hersteller, Lieferanten, Nutzer, DB-User
* Userverwaltung mit Rechtevergabe
* Benachrichtigung per Mail Support- bzw. Lizenzablauf von Lizenzen und Vereinbarungen
* Manuelle Eingabe von Installationen
* Suchfiltereingabe fuer automatische Zuordnen von Installationen aus dem Inventory zu Lizenzen
* Ablagemoeglichkeit mit Referenz zu Lizenz/Software/Vereinbarung fuer Lizenzschluessel, Rechnungen, Angebote und was sonst noch gespeichert werden moechte
* Inventory-Verwaltung (taeglicher Import aus anderen DB's, wie Empirum,OPSI usw.)
* Auswertungen verbrauchter bzw. vorhandener Lizenzen
* Versionsabhaengige Lizenzzuordnung
* versionierte Berechnung von Lizenzen mit Downgraderechten
* Auswertung Kosten (jaehrlich, Kostenstelle, Software, Lizenz etc.)
* farbliche Kennzeichnung Status Lizenzen
* Verwaltung Lizenzserver

## Projekt downloaden ##

**Evtl. vorh. DB und Filesystem vorher sichern**<br>
<br>
Wenn noch keine andere Datenbank existiert, ist es empfehlenswert die Full-Version des tar-Files herunterzuladen. 
Ansonsten von common/db_export das tar-File herunterladen oder per Befehl
~~~bash
git clone https://gitlab.mpcdf.mpg.de/mpidb/mpg_licman.git
~~~
in das Wurzelverzeichnis des Webserver klonen.

## Installation ##

siehe LIESMICH.txt in Folder install<br>
Dort liegen auch alle noch notwendigen SQL-Skripte fuer die DB-Erstellung.

## Screenshot ##

<a href="install/db_licman.png" title="Ueberblick Datenbanken"><img src="install/db_licman.png" align="left" height="100" width="100"></a>
<br><br><br><br>

## Lizenzbedingungen ##

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.<br>
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.<br>  
See the GNU General Public License for more details.<br>
See also the file LICENSE.txt here
