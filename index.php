<?php
/**
 * File: index.php
 * Description:
 * -------------
 *
 * This is an entry file for this Dataface Application.  To use your application
 * simply point your web browser to this file.
 */

  // Limit erhoehen bei Auswertung Lizenzen
  if ( !isset($_REQUEST['-limit']) and @$_REQUEST['-table'] == 'view_software' ) {
     $_REQUEST['-limit'] = $_GET['-limit'] = '200';
  }
  // Limit erhoehen bei Auswertung Inventar
  if ( !isset($_REQUEST['-limit']) and @$_REQUEST['-table'] == 'view_invCon' ) {
     $_REQUEST['-limit'] = $_GET['-limit'] = '300';
  }

 // disbale table views in db - sonst kommt immer Fehlermeldung in apache
define('XATAFACE_DISABLE_PROXY_VIEWS',true);

require_once '../xataface/dataface-public-api.php';
df_init(__FILE__, "../xataface")->display();
