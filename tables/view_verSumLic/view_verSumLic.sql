-- summiere lizenzen pro version entsprechend zugehoerigkeit version zu lizenz
CREATE OR REPLACE VIEW view_verSumLic AS
SELECT
 ver.verID,
 -- ver.softID,
 SUM( lic.anzahl ) AS anzLizenz
FROM
 mpi_version AS ver
 LEFT JOIN con_verLic  AS con ON con.verID = ver.verID
 LEFT JOIN mpi_lizenz AS lic ON lic.lizenzID = con.lizenzID
WHERE
 lic.status = '1'
GROUP BY
 ver.verID
;


-- wird erstmal nicht gebraucht
-- summiere lizenzen pro version entsprechend zugehoerigkeit version zu lizenz
SELECT
 ver.softID,
 lic.lizenzID,
 SUM( vuse.useLizenz ) AS useLizenz
FROM
 mpi_version AS ver
 LEFT JOIN con_verLic  AS con ON ver.verID = con.verID
 LEFT JOIN mpi_lizenz AS lic ON lic.lizenzID = con.lizenzID
 LEFT JOIN view_verVerb vuse ON ver.verID = vuse.verID
WHERE lic.lizenzID IS NOT NULL AND vuse.useVersion IS NOT NULL
GROUP BY 
 lic.lizenzID
;


