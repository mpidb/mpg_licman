-- zeige rest von verfuegbaren und verbrauchten lizenzen pro version
-- aber nur welche downgraderecht haben
-- V0.9.04

DROP FUNCTION IF EXISTS func_version;
DELIMITER $$
CREATE FUNCTION func_version ( iVerID SMALLINT(6) )
RETURNS SMALLINT(6)
DETERMINISTIC
BEGIN
  DECLARE freeVersion SMALLINT(6) DEFAULT '0';
  DECLARE Ot, Gt, Lt, It, St, Pt, Dt, Nt, Mt, Pg SMALLINT(6) DEFAULT '0';
  DECLARE endRow TINYINT DEFAULT '0';
  DECLARE forSoft1 CURSOR FOR
   SELECT Ov, Lv, Iv FROM tmp_version ORDER BY Ov ASC;
  DECLARE forSoft2 CURSOR FOR
   SELECT Ov, Pv FROM tmp_version ORDER BY Ov DESC;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET endRow = 1;
  SELECT softID INTO St FROM mpi_version WHERE verID = iVerID;
  DROP TEMPORARY TABLE IF EXISTS tmp_version;
  CREATE TEMPORARY TABLE IF NOT EXISTS tmp_version ENGINE=MEMORY AS (
   SELECT ver.verID, ver.orderID AS Ov, IFNULL( vlic.anzLizenz, '0' ) AS Lv, IFNULL( vuse.useLizenz, '0' ) AS Iv, '123456' AS Dv,'123456' AS Pv, '123456' AS Rv
   FROM mpi_version AS ver
    LEFT JOIN view_verVerb   vuse ON vuse.verID = ver.verID
    LEFT JOIN view_verLic    vlic ON vlic.verID = ver.verID
   WHERE ver.verID IN (SELECT verID FROM con_verLic) AND ver.softID = St
   ORDER BY orderID );
  SELECT MAX(Ov) INTO Mt FROM tmp_version;

  OPEN forSoft1;
  getSoft: LOOP
  FETCH forSoft1 INTO Ot, Lt, It;
  IF endRow = 1 THEN LEAVE getSoft; END IF;
  SET Dt = (Lt - It + Nt);
  IF Dt < '0' AND Mt != Ot THEN -- hoechste Version merken, auch wenn negativ
   SET Pt = '0';
   SET Nt = Dt;
  ELSE
   SET Pt = Dt;
   SET Nt = '0'; 
  END IF;
  UPDATE tmp_version SET Pv = Pt, Dv = Dt, Rv = '0' WHERE Ov = Ot;
  END LOOP getSoft;
  CLOSE forSoft1;

  SET endRow = '0';
  OPEN forSoft2;
  getSoft: LOOP
  FETCH forSoft2 INTO Ot, Pt;
  IF endRow = 1 THEN LEAVE getSoft; END IF;
  SET Pg = (Pg + Pt);
  UPDATE tmp_version SET Rv = Pg WHERE Ov = Ot;
  -- IF iOrderID = Ot THEN SET ofreeVersion = Pg; END IF;
  END LOOP getSoft;
  CLOSE forSoft2;

  SELECT Rv INTO freeVersion FROM tmp_version WHERE verID = iVerID;
  RETURN freeVersion;
  -- SELECT Rv INTO ofreeVersion FROM tmp_version WHERE Ov = iOrderID;
  -- SELECT * FROM tmp_version;
  DROP TEMPORARY TABLE tmp_version;
END;
$$
DELIMITER ;

-- aufruf
-- CALL proc_version(iSoftID);



