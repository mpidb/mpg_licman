-- zeige rest von verfuegbaren und verbrauchten lizenzen pro version
-- aber nur welche downgraderecht haben
-- V0.9.04

DROP PROCEDURE IF EXISTS proc_version;
DELIMITER $$
CREATE PROCEDURE proc_version ( IN iSoftID SMALLINT(6) )
 BEGIN
  DECLARE Vt, Ot, Gt, Lt, It, St, Pt, Dt, Nt, Mt, Pg SMALLINT(6) DEFAULT '0';
  DECLARE endRow TINYINT DEFAULT '0';
  DECLARE forSoft1 CURSOR FOR
   SELECT verID, IFNULL(anzLizenz,'0'), IFNULL(useVersion,'0') FROM view_version WHERE softID = iSoftID ORDER BY orderID ASC;
  DECLARE forSoft2 CURSOR FOR
   SELECT ver.verID, IFNULL(shw.restVersion,'0') FROM show_version AS shw LEFT JOIN mpi_version AS ver ON ver.verID = shw.verID WHERE ver.softID = iSoftID ORDER BY ver.orderID DESC;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET endRow = 1;
  SELECT verID INTO Mt FROM mpi_version WHERE orderID = ( SELECT MAX(orderID) FROM mpi_version WHERE softID = iSoftID ) AND softID = iSoftID;

  OPEN forSoft1;
  getSoft: LOOP
  FETCH forSoft1 INTO Vt, Lt, It;
  IF endRow = 1 THEN LEAVE getSoft; END IF;
  SET Dt = (Lt - It + Nt);
  UPDATE show_version SET diffVersion = Dt WHERE verID = Vt;
  IF Dt < '0' AND Mt != Vt THEN -- hoechste Version merken, auch wenn negativ
   SET Pt = '0';
   SET Nt = Dt;
  ELSE
   SET Pt = Dt;
   SET Nt = '0';
  END IF;
  UPDATE show_version SET restVersion = Pt WHERE verID = Vt;
  END LOOP getSoft;
  CLOSE forSoft1;

  SET endRow = '0';
  OPEN forSoft2;
  getSoft: LOOP
  FETCH forSoft2 INTO Vt, Pt;
  IF endRow = 1 THEN LEAVE getSoft; END IF;
  SET Pg = (Pg + Pt);
  UPDATE show_version SET freeVersion = Pg WHERE verID = Vt;
  END LOOP getSoft;
  CLOSE forSoft2;
 END;
$$
DELIMITER ;

-- aufruf
-- CALL proc_version(iSoftID);



