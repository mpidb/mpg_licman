-- zeige rest von verfuegbaren und verbrauchten lizenzen pro version
-- aber nur welche downgraderecht haben
-- V0.9.04

DROP PROCEDURE IF EXISTS proc_version;
DELIMITER $$
CREATE PROCEDURE proc_version ( IN iVerID SMALLINT(6) )
proc_label:BEGIN
  DECLARE Vt, Ot, Rt, Lt, It, St, Pt, Dt, Nt, Mt, Tt, Pg SMALLINT(6) DEFAULT '0';
  DECLARE endRow TINYINT DEFAULT '0';
  DECLARE forSoft1 CURSOR FOR
    SELECT verID, orderID, IFNULL(anzLizenz,'0'), IFNULL(useLizenz,'0') FROM view_version WHERE softID = St ORDER BY orderID ASC;
  DECLARE forSoft2 CURSOR FOR
    SELECT ver.verID, diffVersion, IFNULL(shw.restVersion,'0') FROM show_version AS shw LEFT JOIN mpi_version AS ver ON ver.verID = shw.verID WHERE ver.softID = St ORDER BY ver.orderID DESC;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET endRow = 1;
  IF iVerID IS NULL THEN LEAVE proc_label; END IF;  -- raus wenn NULL ist
  SELECT softID INTO St FROM mpi_version WHERE verID = iVerID;
  IF St IS NULL THEN LEAVE proc_label; END IF;  -- raus wenn NULL ist
  SELECT MAX(orderID) INTO Mt FROM mpi_version WHERE softID = St;

  OPEN forSoft1;
  getSoft: LOOP
  FETCH forSoft1 INTO Vt, Ot, Lt, It;
  IF endRow = 1 THEN LEAVE getSoft; END IF;
  SET Dt = (Lt - It + Nt);
  IF Dt < '0' AND Mt != Ot THEN  -- bei hoechster Version Wert nicht aendern
    SET Pt = '0';
    SET Nt = Dt;
  ELSE
    SET Pt = Dt;
    SET Nt = '0';
  END IF;
  IF Dt < '0' THEN SET Rt = Lt; ELSE SET Rt = Lt - Dt; END IF; -- verbrauchte Lizenz pro Version berechnen
  UPDATE show_version SET diffVersion = Dt, verbVersion = Rt, restVersion = Pt WHERE verID = Vt;
  END LOOP getSoft;
  CLOSE forSoft1;

  SET endRow = '0';
  OPEN forSoft2;
  getSoft: LOOP
  FETCH forSoft2 INTO Vt, Dt, Pt;
  IF endRow = 1 THEN LEAVE getSoft; END IF;
  IF Pg < 0 AND Dt < 0 THEN 
    SET Tt = Pg;
  ELSE
    SET Pg = (Pg + Pt);
    SET Tt = Pg;
  END IF;
  UPDATE show_version SET freeVersion = Tt WHERE verID = Vt;
  -- IF Pt < '0' THEN Set Pg = '0'; END IF;   -- negative Werte nicht addieren
  END LOOP getSoft;
  CLOSE forSoft2;
END;
$$
DELIMITER ;

-- aufruf
-- CALL proc_version(iVerID);



