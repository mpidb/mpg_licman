<?php

class tables_show_version {

  // only authenticated user; edit in view not allowed; check role lower READ ONLY
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    if ( strpos($user->val('role'),'VIEW GROUP ') !== false) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

  function getTitle(&$record) {
    $anzLiz = $record->strval('anzLizenz');
    if ( $anzLiz == NULL ) $anzLiz = '0'; elseif ( $anzLiz == '0' ) return 'unbegrenzt';
    $useLiz = $record->display('useLizenz');
    if ( $useLiz == NULL ) $useLiz = '0';
    $table  = 'mpi_version';
    $action = 'browse';
    $field  = 'verID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT version FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return $name.' : '.$anzLiz.' - '.$useLiz.' = '.$record->display('freeLizenz');
  }

  function softID__renderCell( &$record ) {
    $table  = 'mpi_software';
    $action = 'browse';
    $field  = 'softID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql    = "SELECT software FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function verID__renderCell( &$record ) {
    $table  = 'mpi_version';
    $action = 'browse';
    $field  = 'verID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT version FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function anzLizenz__renderCell(&$record) {
    $anz = $record->strval('anzLizenz');
    if ( $anz == NULL ) return '-';
    if ( $anz == '0' ) return '&infin;';
    return $anz;
  }

  function useVersion__renderCell(&$record) {
    $use = $record->strval('useVersion');
    if ( $use == NULL ) return '-';
    return $use;
  }

/*
  function freeVersion__renderCell(&$record) {
    if ( $record->strval('anzLizenz') == '0' ) return '&infin;';
    $free = $record->strval('freeLizenz');
    if ( $free < 0 ) return '<font color="red">'.$free.'</font>';
    return $free;
  }
*/

}

?>
