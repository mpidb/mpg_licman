<?php

class tables_view_kosten {

  // only authenticated user; edit in view not allowed; check role lower READ ONLY
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    if ( strpos($user->val('role'),'VIEW GROUP ') !== false) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

  function block__before_result_list_content() {
    import('Dataface/RecordReader.php');
    $app =& Dataface_Application::getInstance();
    $query = $app->getQuery();
    $records = new Dataface_RecordReader($query, 30, false);
    $miete = array();
    $wartg = array();
    $summe = array();
    foreach ($records as $record) {
      $waehr = ($record->val('waehrung'));
      $miete[$waehr] = $miete[$waehr] + $record->val('miete');
      $wartg[$waehr] = $wartg[$waehr] + $record->val('wartung');
      $summe[$waehr] = $miete[$waehr] + $wartg[$waehr];
    }
    
    echo '<table><thead></thead>';
    echo  '<tbody>';
    echo   '<div class="resultlist-filters"><h3>Summe Filter: </h3><ul>';
    echo   'Miete: ';
    foreach ($miete as $field => $value) {
      echo $value.' ';
      switch ($field) {
        case 'Euro':   echo '&euro;'; break;
        case 'Dollar': echo '&#36;';  break;
        case 'Pfund':  echo '&pound;';break;
        default: echo $value;
      }
      echo '&nbsp;&nbsp;&nbsp';
    }
    echo    '&nbsp;&nbsp;&nbspWartung: ';
    unset($field);
    unset($value);
    foreach ($wartg as $field => $value) { 
      echo $value.' ';
      switch ($field) {
        case 'Euro':   echo '&euro;'; break;
        case 'Dollar': echo '&#36;';  break;
        case 'Pfund':  echo '&pound;';break;
        default: echo $value;
      }
      echo '&nbsp;&nbsp;&nbsp';
    }
    unset($field);
    unset($value);
    echo    '&nbsp;&nbsp;&nbspGesamt: ';
    foreach ($summe as $field => $value) {
      echo '<span style="color:red;">'.$value.'</span> ';
      switch ($field) {
        case 'Euro':   echo '&euro;'; break;
        case 'Dollar': echo '&#36;';  break;
        case 'Pfund':  echo '&pound;';break;
        default: echo $value;
      }
      echo '&nbsp;&nbsp;&nbsp';
    }
    echo    '</ul>';
    echo   '</div>';
    echo  '</tbody>';
    echo '</table>';
  }

  function kostenID__renderCell(&$record) {
    $table  = 'mpi_kosten';
    $action = 'browse';
    $field  = 'kostenID';
    $tabID  = $record->val($field);
    $name   = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }


}
?>

