CREATE OR REPLACE VIEW view_kosten AS
SELECT
 kost.kostenID,
 ablg.vorgang,
 (SELECT software FROM mpi_software WHERE softID = ablg.softID) AS software,
 (SELECT vertrag FROM mpi_vertrag WHERE vertragID = ablg.vertragID) AS vertrag,
 (SELECT bezeichnung FROM mpi_lizenz WHERE lizenzID = ablg.lizenzID) AS bezeichnung,
 kost.buchung,
 kost.kostenstelle,
 kost.waehrung,
 kost.miete,
 kost.wartung,
 kost.notiz
FROM
 mpi_kosten AS kost
LEFT JOIN
 mpi_ablage AS ablg ON kost.ablageID = ablg.ablageID

