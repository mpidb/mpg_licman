<?php

class tables_view_lizenz {

  // weil fields.ini verlinkt, deshalb hier
  function __sql__() {
    return "SELECT vlic.*, soft.software AS softFilter FROM view_lizenz AS vlic LEFT JOIN mpi_software AS soft ON vlic.softID = soft.softID ORDER BY software";
  }

  // only authenticated user; edit in view not allowed; check role lower READ ONLY
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    if ( strpos($user->val('role'),'VIEW GROUP ') !== false) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

  function getTitle(&$record) {
    if ($record->strval('anzLizenz') == NULL) return;
    if (($record->strval('anzLizenz') == '0') OR ($record->strval('modell') == 'Concurrent')) return 'unbegrenzt';
    if ($record->val('verID') == NULL) return $record->display('anzLizenz').' - '.$record->display('useLizenz').' = '.$record->display('freeLizenz');
    $table  = 'mpi_version';
    $action = 'browse';
    $field  = 'verID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT version FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return $name.' : '.$record->display('anzLizenz').' - '.$record->display('useLizenz').' = '.$record->display('freeLizenz');
  }

  function valuelist__viewInv(){
       return array(0=>'man', 1=>'auto');
  }

  function tabID__renderCell(&$record) {
    $tabID  = $record->val('tabID');
     $action = 'list';
    if ($record->strval('inventar') == '1') {
      $table = 'mpi_inventar';
      $field = 'conID';
      $name2 = 'auto';
    } else {
      $table = 'mpi_install';
      $field = 'installID';
      $name2 = 'manu';
    }
    $name   = str_pad($tabID, 6, 0, STR_PAD_LEFT).' : '.$name2;
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a>';
  }

  function instName__renderCell(&$record) {
    $action = 'browse';
    $tabID  = $record->val('tabID');
    $name   = str_pad($tabID, 6, 0, STR_PAD_LEFT).' : '.$record->val('instName');
    if ($record->strval('inventar') == '1') {
      $field = 'conID';
      $insNa = 'instName';
      $table = 'mpi_inventar';
    } else {
      $field = 'installID';
      $insNa = 'instID';
      $table = 'mpi_install';
    }
    $instID = $record->val($insNa);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}&${insNa}=${instID}";
    return '<a href="'.$url.'">'.$name.'</a>';
  }

  function softID__renderCell( &$record ) {
    $table  = 'mpi_software';
    $action = 'browse';
    $field  = 'softID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql    = "SELECT software FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function verID__renderCell( &$record ) {
    $table  = 'mpi_version';
    $action = 'browse';
    $field  = 'verID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT version FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function lizenzID__renderCell( &$record ) {
    $table  = 'mpi_lizenz';
    $action = 'browse';
    $field  = 'lizenzID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT IFNULL(bezeichnung, lizenzID) FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function anzLizenz__renderCell(&$record) {
    if ( $record->strval('anzLizenz') == NULL ) return '-';
    if ( $record->strval('anzLizenz') == '0' ) return '&#48';
    return $record->strval('anzLizenz');
  }

  function useLizenz__renderCell(&$record) {
    if ( $record->strval('useLizenz') == NULL ) return '-';
    if ( $record->strval('useLizenz') == '0' ) return '&#48';
    return $record->strval('useLizenz');
  }

  // freie lizenz unter 0 = rot, die 0 ist fake sonst garkeine Anzeige
  function freeLizenz__renderCell(&$record) {
    if ( $record->strval('anzLizenz') == '0' ) return '&infin;';
    if ( $record->strval('modell') == 'Concurrent' ) return '&infin;';
    $free = $record->strval('freeLizenz');
    if ( $free == '0' ) return '&#48';
    if ( $free < '0' ) return '<font color="red">'.$free.'</font>';
    return $free;
  }

  function anzDubli__rendercell(&$record) {
    if ( $record->val('anzDubli') == '0' ) return;
    return $record->val('anzDubli');
  } 

/*
  function gPreis__rendercell(&$record) {
    if ( $record->val('gPreis') > '0' ) return '<div style="text-align:right;">'.$record->val('gPreis').' &euro;</div>';
    return;
  }
*/

}
?>
