-- alle Lizenzen ohne Downgrade-Version
CREATE OR REPLACE VIEW view_lizenz AS
SELECT
 tabID,
 softID,
 verID,
 lizenzID,
 inventar,
 anzLizenz,
 -- count(lizenzID) AS anzInstall,
 ( SUM(useLizenz) ) AS useLizenz,
 IF (anzLizenz = 0, 0, (anzLizenz - ( SUM(useLizenz) ))) AS freeLizenz,
 modell
FROM
 view_instLic
WHERE
 lizenzID NOT IN (SELECT lizenzID FROM con_verLic)
GROUP BY
 lizenzID
;


