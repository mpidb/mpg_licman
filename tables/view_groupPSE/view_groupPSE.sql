-- view auf spezielle Ansicht und Berechtigung nur fuer Gruppe PSE
-- expiry uebrschreibt lic.support
CREATE OR REPLACE VIEW view_groupPSE AS
SELECT
 lic.*,
 IF(ver.status = 1 AND ver.setExpLizenz = 1 AND ver.expiry IS NOT NULL, ver.expiry, lic.expSupport) AS expiry
FROM
 mpi_lizenz AS lic
 LEFT JOIN mpi_vertrag AS ver ON lic.vertragID = ver.vertragID
WHERE
 (lic.kostenstelle = '10408' OR lic.kostenstelle = 'PSE') AND lic.status = 1
ORDER BY
 lic.vertragID, lic.lizenzID
;


