<?php

class tables_view_groupPSE {

  // only authenticated user; edit in view not allowed; check role lower READ ONLY
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    $role = $user->val('role');
    // specially role VIEW GROUP PSE here and other group NO ACCESS
    if ( $role === 'VIEW GROUP PSE') return Dataface_PermissionsTool::getRolePermissions($role);
    if ( strpos($role,'VIEW GROUP ') !== false) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY'); //else VIEW
  }

  // nochmaiger hinweis das vieles geloesscht wird
  function block__after_delete_form_message() {
    echo '<div style="color: red">Achtung: Bei der L&ouml;schung einer Lizenz werden kaskadenartig alle abh&aumlngigen Datens&auml;tze wie Schl&uuml;ssel, Versionszuordnung mit gel&ouml;scht!</dev>';
  }

  function block__after_result_list_content() {
    echo '<span style="background-color:#ccf;"> Support ist abgelaufen. </span>&nbsp;';
    echo '<span style="background-color:#fcc;"> Lizenz ist abgelaufen. </span>&nbsp;&nbsp;&nbsp;&nbsp;';
    echo 'Lizenz oder Support';
    echo '<span style="background-color:#cfc;"> ist g&uuml;ltig. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#ffc;"> l&auml;uft heute ab. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#cff;"> endet demn&auml;chst. </span>&nbsp;|&nbsp;';
    echo '<span> ist inaktiv. </span>&nbsp;&nbsp;&nbsp;&nbsp;';
     echo 'Ablaufdatum <font color="blue">TT.MM.JJJJ</font> aus Vereinbarung';
  }

  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    // return $table;  // ohne color
    if ( $record->strval('status') == '0' ) return $table.'normal';
    $expLic = $record->strval('expLizenz');
    $expSup = $record->strval('expSupport');
    $tabID  = $record->val('vertragID');
    if ($tabID > 0) {
      $tabvt = 'mpi_vertrag';
      $field  = 'vertragID';
      $sql = "SELECT expiry FROM $tabvt WHERE $field = '$tabID' AND status = '1' AND expiry IS NOT NULL";
      list($datum) = xf_db_fetch_row(xf_db_query($sql, df_db()));
      if (isset($datum)) $expSup = $datum;
      $sql = "SELECT expiry FROM $tabvt WHERE $field = '$tabID' AND status = '1' AND expiry IS NOT NULL AND setExpLizenz = '1'";
      list($datum) = xf_db_fetch_row(xf_db_query($sql, df_db()));
      if (isset($datum)) $expLic = $datum;
    }
    if ( $expLic == NULL AND $expSup == NULL ) return $table.'normal';
    $app = Dataface_Application::getInstance();
    $delay = $app->_conf['_own']['notify'];
    if (!is_numeric($delay)) $delay = 30;
    // xx Tage vorher Status auf Gelb 
    $hysDate = date('Y-m-d',mktime(0,0,0,date("m"),date("d")+$delay,date("Y")));
    $curDate = date('Y-m-d');
    if     ( $expLic != NULL AND $expLic <  $curDate ) return $table.'toless';
    elseif ( $expSup != NULL AND $expSup <  $curDate ) return $table.'lesser';
    elseif ( $expLic != NULL AND $expLic == $curDate ) return $table.'equal';
    elseif ( $expSup != NULL AND $expSup == $curDate ) return $table.'equal';
    elseif ( $expLic != NULL AND $expLic <= $hysDate ) return $table.'narrow';
    elseif ( $expSup != NULL AND $expSup <= $hysDate ) return $table.'narrow';
    elseif ( $expLic != NULL AND $expLic >  $hysDate ) return $table.'enough';
    elseif ( $expSup != NULL AND $expSup >  $hysDate ) return $table.'enough';
    return $table.'normal';
  }

  function valuelist__status() {
       return array(0=>'inaktiv', 1=>'aktiv');
  }

  // link auf interne db
  function softID__renderCell(&$record) {
    $table  = 'mpi_software';
    $action = 'browse';
    $field  = 'softID';
    $tabID  = $record->val($field);
    $name   = $record->strval('softFilter');  // vom __sql__ in fields.ini
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function verID__renderCell( &$record ) {
    $table  = 'mpi_version';
    $action = 'browse';
    $field  = 'verID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT version FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function vertragID__renderCell( &$record ) {
    if ( $record->val('vertragID') == NULL ) return;
    $table = 'mpi_vertrag';
    $action = 'browse';
    $field  = 'vertragID';
    $tabID  = $record->val($field);
    $name   = $tabID;
    $sql    = "SELECT vertrag FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function useLizenz__renderCell(&$record) {
    $use = $record->strval('useLizenz');
    return '<div style="text-align:right;">'.$use.'</div>';
  }

  function freeLizenz__renderCell(&$record) {
    if ( $record->strval('anzahl') == '0' ) return '<div style="text-align:right;">&infin;</div>';
//    if ( $record->strval('modell') == 'Concurrent' ) return '<div style="text-align:right;">&infin;</div>';
    $free = $record->strval('freeLizenz');
    if ( $free == '0' ) return '<div style="text-align:right;">&#48;</div>';
    if ( $free < '0' ) return '<div style="text-align:right;"><font color="red">'.$free.'</font></div>';
    return '<div style="text-align:right;">'.$free.'</div>';
  }

  // xataface loescht sonst einfach sonderzeichen wie <=
  function version__rendercell(&$record) {
    return $record->strval('version');
  }

  function modell__renderCell(&$record) {
    $modell = $record->strval('modell');
    $nutzer = $record->strval('nutzer');
    if (!empty($nutzer) and $nutzer != 'MPI') {
      $modell = "$modell ($nutzer)";
    }
    return $modell;
  }

  function support__display(&$record) {
    if ($record->val('support') == '1') return 'ja'; else return 'nein';
  }

  function srvID__renderCell( &$record ) {
    $table  = 'mpi_licSrv';
    $action = 'browse';
    $field  = 'srvID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT CONCAT(server, IF(alias IS NOT NULL, CONCAT(' (',alias,')'),'')) FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function expLizenz__htmlValue(&$record) {
    $datum = $record->strval('expLizenz');
    if ($datum == NULL) return;
    return date('d.m.Y', strtotime($datum));
  }

  // nur in liste wird vertragsende angezeigt
  function expLizenz__rendercell(&$record) {
    $tabID  = $record->val('vertragID');
    if ($tabID > 0) {
      $table = 'mpi_vertrag';
      $field  = 'vertragID';
      $sql = "SELECT expiry FROM $table WHERE $field = '$tabID' AND status = '1' AND expiry IS NOT NULL AND setExpLizenz = '1'";
      list($datum) = xf_db_fetch_row(xf_db_query($sql, df_db()));
      if (isset($datum)) return '<font color="blue">'.date('d.m.Y', strtotime($datum)).'</font>';
    }
    $datum = $record->strval('expLizenz');
    if ($datum == NULL) return;
    return date('d.m.Y', strtotime($datum));
  }

  // nur in liste wird vertragsende angezeigt
  function expiry__rendercell(&$record) {
    $tabID  = $record->val('vertragID');
    if ($tabID > 0) {
      $table = 'mpi_vertrag';
      $field  = 'vertragID';
      $sql = "SELECT expiry FROM $table WHERE $field = '$tabID' AND status = '1' AND expiry IS NOT NULL";
      list($datum) = xf_db_fetch_row(xf_db_query($sql, df_db()));
      if (isset($datum)) return '<font color="blue">'.date('d.m.Y', strtotime($datum)).'</font>';
    }
    $datum = $record->strval('expiry');
    if ($datum == NULL) return;
    return date('d.m.Y', strtotime($datum));
  }

  function expiry__htmlValue(&$record) {
    $datum = $record->strval('expiry');
    if ($datum == NULL) return;
    return date('d.m.Y', strtotime($datum));
  }

  function update__display(&$record)  {
    if ($record->val('update') == '1') return 'ja'; else return 'nein';
  }

  function nachricht__display(&$record)  {
    if ($record->val('nachricht') == '1') return 'ja'; else return 'nein';
  }

  function gPreis__rendercell(&$record) {
    if ( $record->val('gPreis') > 0 ) return '<div style="text-align:right;">'.$record->val('gPreis').' &euro;</div>';
    return;
  }

  function inventar__rendercell(&$record) {
    $table  = 'list_inventar';
    $action = 'browse';
    $tabID  = $record->strval('inventar');
    if ($tabID == NULL) return;
    $tabID2 = $record->strval('unterNr');
    $name   = $record->strval('inventar').':'.$record->display('unterNr');
    $sql    = "SELECT anlage FROM list_inventar WHERE anlage = '$tabID' AND unterNr = '$tabID2'";
    list($anlage) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    if (isset($anlage)) {
      $url = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&anlage=$tabID&unterNr=$tabID2";
      return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
    }
    if ($record->strval('status') != 'verschrottet') return '<font color="red">'.$name.'</font>';
    return '<font color="gray">'.$name.'</font>';
  }

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record) {
    if ($record->strval('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }


}
?>
