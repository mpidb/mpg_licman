<?php

class tables_mpi_notiz { 

  function softID__renderCell(&$record) {
    $tabID = $record->val('softID');
    $sql = "SELECT software FROM mpi_software WHERE softID = '$tabID'";
    list($sw) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return $sw;
  }

  function lizenzID__renderCell( &$record ) {
    $tabID = $record->strval('lizenzID');
    $table = 'mpi_lizenz';
    return '<a href="'.DATAFACE_SITE_HREF.'?-table='.$table.'&-action=browse&-mode=list&lizenzID='.$tabID.'">'.$tabID.'</a>';
  }

  // Zeitanzeige aendern
  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) {
      return "";
    } else {
      return date('d.m.Y', strtotime($record->strval('zeitstempel')));
    }
  }

// User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

}
?>
