--
-- Trigger AFTER `mpi_lizenz`
-- starte procedure fuer versionierte Berechnung

--
-- Trigger AFTER insert mpi_lizenz
--

DROP TRIGGER IF EXISTS set_lic_ins;
DELIMITER $$
CREATE TRIGGER set_lic_ins AFTER INSERT ON mpi_lizenz
 FOR EACH ROW BEGIN
  IF NEW.verID IS NOT NULL THEN CALL proc_version(NEW.verID); END IF;
 END;
$$
DELIMITER ;


--
-- Trigger AFTER update mpi_lizenz
--

DROP TRIGGER IF EXISTS set_lic_upd;
DELIMITER $$
CREATE TRIGGER set_lic_upd AFTER UPDATE ON mpi_lizenz
 FOR EACH ROW BEGIN
  IF OLD.verID IS NOT NULL THEN CALL proc_version(OLD.verID); END IF;
  IF NEW.verID IS NOT NULL THEN CALL proc_version(NEW.verID); END IF;
 END;
$$
DELIMITER ;


--
-- Trigger AFTER delete mpi_lizenz
--

DROP TRIGGER IF EXISTS set_lic_del;
DELIMITER $$
CREATE TRIGGER set_lic_del AFTER DELETE ON mpi_lizenz FOR EACH ROW
 BEGIN
  IF OLD.verID IS NOT NULL THEN CALL proc_version(OLD.verID); END IF;
 END;
$$
DELIMITER ;


