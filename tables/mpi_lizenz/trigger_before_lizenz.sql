--
-- disabled, anders geloest
-- setze autom. dates, wenn mit vertrag verbunden
-- Trigger BEFORE `mpi_lizenz`

DROP TRIGGER IF EXISTS `set_lic_ins`;
DELIMITER $$
CREATE TRIGGER `set_lic_ins` BEFORE INSERT ON `mpi_lizenz`
 FOR EACH ROW BEGIN
  SET NEW.expSupport = (SELECT expiry FROM mpi_vertrag WHERE vertragID = NEW.vertragID);
  SET NEW.expLizenz  = (SELECT expiry FROM mpi_vertrag WHERE vertragID = NEW.vertragID AND setExpLizenz = '1');
 END;
$$
DELIMITER ;




-- ab hier alt trigger


-- geht leider nicht bei mysql - loop
DELIMITER //
DROP TRIGGER IF EXISTS `set_lic_upd`//
CREATE TRIGGER `set_lic_upd` BEFORE UPDATE ON `mpi_lizenz`
 FOR EACH ROW UPDATE mpi_lizenz SET gPreis = ePreis * anzahl WHERE lizenzID = NEW.lizenzID;
//

--- alternativ waehrend update mpi-software - ergibt auch loop
DELIMITER //
DROP TRIGGER IF EXISTS `set_lic_upd`//
CREATE TRIGGER `set_lic_upd` BEFORE UPDATE ON `mpi_software`
 FOR EACH ROW UPDATE mpi_lizenz SET gPreis = ePreis * anzahl WHERE softID = NEW.softID;
//

