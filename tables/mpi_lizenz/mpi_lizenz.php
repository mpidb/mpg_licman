<?php

class tables_mpi_lizenz { 

  // button update & remove in views weg
  function rel_verbrauch__permissions($record) {
    return array( 'remove related record' => 0, 'update related records' => 0 );
  }

  // glanceview and selectbox
  function getTitle(&$record) {
    return $record->display('bezeichnung');
  }

  // Anzeige in 'add releated record'
  function titleColumn() {
    return "IFNULL(bezeichnung, lizenzID)";
  }

  // nochmaiger hinweis das vieles geloesscht wird
  function block__after_delete_form_message() {
    echo '<div style="color: red">Achtung: Bei der L&ouml;schung einer Lizenz werden kaskadenartig alle abh&aumlngigen Datens&auml;tze wie Schl&uuml;ssel, Versionszuordnung mit gel&ouml;scht!</dev>';
  }

  function block__after_result_list_content() {
    echo '<span style="background-color:#ccf;"> Support ist abgelaufen. </span>&nbsp;';
    echo '<span style="background-color:#fcc;"> Lizenz ist abgelaufen. </span>&nbsp;&nbsp;&nbsp;&nbsp;';
    echo 'Lizenz oder Support';
    echo '<span style="background-color:#cfc;"> ist g&uuml;ltig. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#ffc;"> l&auml;uft heute ab. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#cff;"> endet demn&auml;chst. </span>&nbsp;|&nbsp;';
    echo '<span> ist inaktiv. </span>&nbsp;&nbsp;&nbsp;&nbsp;';
     echo 'Ablaufdatum <font color="blue">TT.MM.JJJJ</font> aus Vereinbarung';
  }

  function block__before_email_widget() {
    $table  = 'list_email';
    $action = 'new';
    $url  = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}";
    echo '<a href='.$url.'><img src="/xataface/images/add_icon.gif" alt="" width="17" height="17"> Neue Email hinzufügen</a>';
  }

  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    // return $table;  // ohne color
    if ( $record->strval('status') == '0' ) return $table.'normal';
    $expLic = $record->strval('expLizenz');
    $expSup = $record->strval('expSupport');
    $tabID  = $record->val('vertragID');
    if ($tabID > 0) {
      $tabvt = 'mpi_vertrag';
      $field  = 'vertragID';
      $sql = "SELECT expiry FROM $tabvt WHERE $field = '$tabID' AND status = '1' AND expiry IS NOT NULL";
      list($datum) = xf_db_fetch_row(xf_db_query($sql, df_db()));
      if (isset($datum)) $expSup = $datum;
      $sql = "SELECT expiry FROM $tabvt WHERE $field = '$tabID' AND status = '1' AND expiry IS NOT NULL AND setExpLizenz = '1'";
      list($datum) = xf_db_fetch_row(xf_db_query($sql, df_db()));
      if (isset($datum)) $expLic = $datum;
    }
    if ( $expLic == NULL AND $expSup == NULL ) return $table.'normal';
    $app = Dataface_Application::getInstance();
    $delay = $app->_conf['_own']['notify'];
    if (!is_numeric($delay)) $delay = 30;
    // xx Tage vorher Status auf Gelb 
    $hysDate = date('Y-m-d',mktime(0,0,0,date("m"),date("d")+$delay,date("Y")));
    $curDate = date('Y-m-d');
    if     ( $expLic != NULL AND $expLic <  $curDate ) return $table.'toless';
    elseif ( $expSup != NULL AND $expSup <  $curDate ) return $table.'lesser';
    elseif ( $expLic != NULL AND $expLic == $curDate ) return $table.'equal';
    elseif ( $expSup != NULL AND $expSup == $curDate ) return $table.'equal';
    elseif ( $expLic != NULL AND $expLic <= $hysDate ) return $table.'narrow';
    elseif ( $expSup != NULL AND $expSup <= $hysDate ) return $table.'narrow';
    elseif ( $expLic != NULL AND $expLic >  $hysDate ) return $table.'enough';
    elseif ( $expSup != NULL AND $expSup >  $hysDate ) return $table.'enough';
    return $table.'normal';
  }

  function valuelist__status() {
       return array(0=>'inaktiv', 1=>'aktiv');
  }

  // link auf interne db
  function softID__renderCell(&$record) {
    $table  = 'mpi_software';
    $action = 'browse';
    $field  = 'softID';
    $tabID  = $record->val($field);
    $name   = $record->strval('softFilter');  // vom __sql__ in fields.ini
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function verID__renderCell( &$record ) {
    $table  = 'mpi_version';
    $action = 'browse';
    $field  = 'verID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT version FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function vertragID__renderCell( &$record ) {
    if ( $record->val('vertragID') == NULL ) return;
    $table = 'mpi_vertrag';
    $action = 'browse';
    $field  = 'vertragID';
    $tabID  = $record->val($field);
    $name   = $tabID;
    $sql    = "SELECT vertrag FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }
/*
  function anzahl__renderCell(&$record) {
    $anz = $record->val('anzahl');
    if ($anz == 0) return '&infin;';
    return $anz;
  }
*/
  function useLizenz__renderCell(&$record) {
    $use = $record->strval('useLizenz');
    return '<div style="text-align:right;">'.$use.'</div>';
  }

  function freeLizenz__renderCell(&$record) {
    if ( $record->strval('anzahl') == '0' ) return '<div style="text-align:right;">&infin;</div>';
//    if ( $record->strval('modell') == 'Concurrent' ) return '<div style="text-align:right;">&infin;</div>';
    $free = $record->strval('freeLizenz');
    if ( $free == '0' ) return '<div style="text-align:right;">&#48;</div>';
    if ( $free < '0' ) return '<div style="text-align:right;"><font color="red">'.$free.'</font></div>';
    return '<div style="text-align:right;">'.$free.'</div>';
  }

  // xataface loescht sonst einfach sonderzeichen wie <=
  function version__rendercell(&$record) {
    return $record->strval('version');
  }

  function modell__renderCell(&$record) {
    $modell = $record->strval('modell');
    $nutzer = $record->strval('nutzer');
    if (!empty($nutzer) and $nutzer != 'MPI') {
      $modell = "$modell ($nutzer)";
    }
    return $modell;
  }

  // display(list+browse) funktioniert nicht fuer html-code
  function expLizenz__htmlValue(&$record) {
    $datum = $record->strval('expLizenz');
    if ($datum == NULL) return;
    return date('d.m.Y', strtotime($datum));
  }

  // nur in liste wird vertragsende angezeigt
  function expLizenz__rendercell(&$record) {
    $tabID  = $record->val('vertragID');
    if ($tabID > 0) {
      $table = 'mpi_vertrag';
      $field  = 'vertragID';
      $sql = "SELECT expiry FROM $table WHERE $field = '$tabID' AND status = '1' AND expiry IS NOT NULL AND setExpLizenz = '1'";
      list($datum) = xf_db_fetch_row(xf_db_query($sql, df_db()));
      if (isset($datum)) return '<font color="blue">'.date('d.m.Y', strtotime($datum)).'</font>';
    }
    $datum = $record->strval('expLizenz');
    if ($datum == NULL) return;
    return date('d.m.Y', strtotime($datum));
  }

  function support__display(&$record) {
    if ($record->val('support') == '1') return 'ja'; else return 'nein';
  }

  function srvID__renderCell( &$record ) {
    $table  = 'mpi_licSrv';
    $action = 'browse';
    $field  = 'srvID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT CONCAT(server, IF(alias IS NOT NULL, CONCAT(' (',alias,')'),'')) FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

   // display(list+browse) funktioniert nicht fuer html-code
  function expSupport__htmlValue(&$record) {
    $datum = $record->strval('expSupport');
    if ($datum == NULL) return;
    return date('d.m.Y', strtotime($datum));
  }

  // nur in liste wird vertragsende angezeigt
  function expSupport__rendercell(&$record) {
    $tabID  = $record->val('vertragID');
    if ($tabID > 0) {
      $table = 'mpi_vertrag';
      $field  = 'vertragID';
      $sql = "SELECT expiry FROM $table WHERE $field = '$tabID' AND status = '1' AND expiry IS NOT NULL";
      list($datum) = xf_db_fetch_row(xf_db_query($sql, df_db()));
      if (isset($datum)) return '<font color="blue">'.date('d.m.Y', strtotime($datum)).'</font>';
    }
    $datum = $record->strval('expSupport');
    if ($datum == NULL) return;
    return date('d.m.Y', strtotime($datum));
  }

  function update__display(&$record)  {
    if ($record->val('update') == '1') return 'ja'; else return 'nein';
  }

  function nachricht__display(&$record)  {
    if ($record->val('nachricht') == '1') return 'ja'; else return 'nein';
  }

  function gPreis__rendercell(&$record) {
    if ( $record->val('gPreis') > 0 ) return '<div style="text-align:right;">'.$record->val('gPreis').' &euro;</div>';
    return;
  }

  function inventar__rendercell(&$record) {
    $table  = 'list_inventar';
    $action = 'browse';
    $tabID  = $record->strval('inventar');
    if ($tabID == NULL) return;
    $tabID2 = $record->strval('unterNr');
    $name   = $record->strval('inventar').':'.$record->display('unterNr');
    $sql    = "SELECT anlage FROM list_inventar WHERE anlage = '$tabID' AND unterNr = '$tabID2'";
    list($anlage) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    if (isset($anlage)) {
      $url = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&anlage=$tabID&unterNr=$tabID2";
      return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
    }
    if ($record->strval('status') != 'verschrottet') return '<font color="red">'.$name.'</font>';
    return '<font color="gray">'.$name.'</font>';
  }

  function bestellnummer__rendercell(Dataface_Record $record) {
    $name   = $record->strval('bestellnummer');
    if ($record->strval('bestellung.bestellnummer') != NULL) {
      $table  = 'view_inv_geraete';
      $action = 'list';
      $field  = 'bestellnummer';
      $tabID  = $record->strval($field);
      $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
      return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
    }
    return $name;
  }

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record) {
    if ($record->strval('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  function beforeSave(&$record) {
    // User automatisch bei neu und aendern setzen
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);

    // Leeres Feld durch NULL ersetzen
    if ( $record->strval('expLizenz' ) == '' ) $record->setValue('expLizenz', NULL);
    if ( $record->strval('expSupport') == '' ) $record->setValue('expSupport', NULL);

    if (($record->val('nachricht') == '1') AND ($record->strval('email') == NULL)) {
      return Dataface_Error::permissionDenied('Eine Aktivierung ohne Email macht keinen Sinn!');
    }
    if (($record->val('nachricht') == '1') AND ($record->strval('expLizenz') == NULL) AND ($record->strval('expSupport') == NULL)) {
      return Dataface_Error::permissionDenied('Eine Aktivierung ohne Ablaufdatum macht keinen Sinn!');
    }

    // softID direkt und softID version duerfen nicht unterschiedlich sein, verID per __sql__ in fields.ini
    if (($record->val('verID') != NULL)) {
      $verID = $record->val('verID');
      $sql = "SELECT softID FROM mpi_version WHERE verID = '$verID'";
      list($soft2) = xf_db_fetch_row(xf_db_query($sql, df_db()));
      $soft1 = $record->val('softID');
      if ( $soft1 != $soft2 ) {
        return Dataface_Error::permissionDenied('mpi_izenz: Zugewiesene Software -'.$soft2.'- passt nicht zur Versionssoftware -'.$soft1.'-!');
      }
    }

    // setze bezeichnung mit sinnvollen Werten, falls nicht benutzt
    $bezeich = $record->strval('bezeichnung');
    $softID  = $record->val('softID');
    $licID   = $record->val('lizenzID');
    if (($bezeich == NULL) OR ($bezeich == '--')) {
      $sql = "SELECT software FROM mpi_software WHERE softID = '$softID'";
      list($soft) = xf_db_fetch_row(xf_db_query($sql, df_db()));
      $record->setValue('bezeichnung', $soft.' : '.$record->strval('version').' : '.$record->strval('modell').' : '.$record->val('anzahl'));
    }
  }

/*
  // wahrscheinlich uerberfluessig, daher erstmal raus
  function __import__csv(&$data, $defaultValues=array()) {
    // build an array of Dataface_Record objects that are to be inserted based
    // on the CSV file data.
    $records = array();
    // first split the CSV file into an array of rows.
    $rows = explode("\n", $data);
    // wenn Kopfzeile vorhanden
    // array_shift($rows); 
    foreach ( $rows as $row ) {
      // bug leere Zeile am Ende
      if ( !trim($row) ) continue;
      // We iterate through the rows and parse the name, phone number, and email 
      // addresses to that they can be stored in a Dataface_Record object.
      list($softID,$modell,$status,$bezeichnung,$anzahl,$version,$vertrag,$expLizenz,$nachricht,$email,$support,$expSupport,$update,$nutzer,$key,$ablage,$lieferant,$bestellnummer,$artikelnummer,$ePreis,$bemerkung,$bearbeiter) = explode(',', $row);
      $record = new Dataface_Record('mpi_lizenz', array());

      // We insert the default values for the record.
      $record->setValues($defaultValues);

      // Now we add the values from the CSV file.
      $record->setValues(array(
      'softID'=>$softID,
      'modell'=>$modell,
      'status'=>$status,
      'bezeichnung'=>$bezeichnung,
      'anzahl'=>$anzahl,
      'version'=>$version,
      'vertrag'=>$vertrag,
      'expLizenz'=>$expLizenz,
      'nachricht'=>$nachricht,
      'email'=>$email,
      'support'=>$support,
      'expSupport'=>$expSupport,
      'update'=>$update,
      'nutzer'=>$nutzer,
      'key'=>$key,
      'ablage'=>$ablage,
      'lieferant'=>$lieferant,
      'bestellnummer'=>$bestellnummer,
      'artikelnummer'=>$artikelnummer,
      'ePreis'=>$ePreis,
      'bemerkung'=>$bemerkung,
      'bearbeiter'=>$bearbeiter,
      ));
      // Now add the record to the output array.
      $records[] = $record;
    }
    // Now we return the array of records to be imported.
    return $records;
  }
*/

}
?>
