-- eleminiere doppelte instID's pro lizenzID, tabelle install_man dominant
-- basierend auf mpi_lizenz, view_install
CREATE OR REPLACE VIEW view_instLic AS
SELECT
 vins.tabID,
 vins.instName,
 vins.softID,
 vins.verID,
 vins.lizenzID,
 vins.inventar,
 vins.useLizenz,
 vins.anzTreffer,
 lic.anzahl AS anzLizenz,
 (count(lic.lizenzID) - 1 ) AS anzDubli,
 ( lic.ePreis * lic.anzahl ) AS gPreis,
 lic.modell,
 vins.bearbeiter
FROM
 mpi_lizenz AS lic
 INNER JOIN view_install AS vins ON lic.lizenzID = vins.lizenzID
WHERE
 lic.status = '1'
GROUP BY
 lic.lizenzID, vins.instName
;


