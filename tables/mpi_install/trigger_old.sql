--old
DELIMITER //
DROP TRIGGER IF EXISTS `set_instlic_ins`//

CREATE TRIGGER `set_instlic_ins` AFTER INSERT ON `mpi_install`
 FOR EACH ROW BEGIN
  UPDATE mpi_software SET anzLizenz = (SELECT SUM(anzahl) FROM mpi_lizenz WHERE softID = ANY ( SELECT softID FROM mpi_lizenz WHERE lizenzID = NEW.lizenzID ) AND status = '1') WHERE softID = ( SELECT softID FROM mpi_lizenz WHERE lizenzID = NEW.lizenzID );
  UPDATE mpi_software SET instLizenz = (SELECT SUM(anzahl) FROM mpi_install WHERE lizenzID = ANY (SELECT lizenzID FROM mpi_lizenz WHERE softID = ( SELECT softID FROM mpi_lizenz WHERE lizenzID = NEW.lizenzID ))) WHERE softID = ( SELECT softID FROM mpi_lizenz WHERE lizenzID = NEW.lizenzID );
  UPDATE mpi_software SET restLizenz = anzLizenz - instLizenz WHERE softID = ( SELECT softID FROM mpi_lizenz WHERE lizenzID = NEW.lizenzID );
  INSERT IGNORE INTO list_pcname SET instID = NEW.instID, instName = (SELECT name FROM view_pc WHERE tabID = NEW.instID);
 END
//


DELIMITER //
DROP TRIGGER IF EXISTS `set_instlic_upd`//

CREATE TRIGGER `set_instlic_upd` AFTER UPDATE ON `mpi_install`
 FOR EACH ROW BEGIN
  UPDATE mpi_software SET anzLizenz = (SELECT SUM(anzahl) FROM mpi_lizenz WHERE softID = ANY ( SELECT softID FROM mpi_lizenz WHERE lizenzID = OLD.lizenzID ) AND status = '1') WHERE softID = ( SELECT softID FROM mpi_lizenz WHERE lizenzID = OLD.lizenzID );
  UPDATE mpi_software SET instLizenz = (SELECT SUM(anzahl) FROM mpi_install WHERE lizenzID = ANY (SELECT lizenzID FROM mpi_lizenz WHERE softID = ( SELECT softID FROM mpi_lizenz WHERE lizenzID = OLD.lizenzID ))) WHERE softID = ( SELECT softID FROM mpi_lizenz WHERE lizenzID = OLD.lizenzID );
  UPDATE mpi_software SET restLizenz = anzLizenz - instLizenz WHERE softID = ( SELECT softID FROM mpi_lizenz WHERE lizenzID = OLD.lizenzID );
  IF NEW.lizenzID != OLD.lizenzID THEN
   UPDATE mpi_software SET anzLizenz = (SELECT SUM(anzahl) FROM mpi_lizenz WHERE softID = ANY ( SELECT softID FROM mpi_lizenz WHERE lizenzID = NEW.lizenzID ) AND status = '1') WHERE softID = ( SELECT softID FROM mpi_lizenz WHERE lizenzID = NEW.lizenzID );
   UPDATE mpi_software SET instLizenz = (SELECT SUM(anzahl) FROM mpi_install WHERE lizenzID = ANY (SELECT lizenzID FROM mpi_lizenz WHERE softID = ( SELECT softID FROM mpi_lizenz WHERE lizenzID = NEW.lizenzID ))) WHERE softID = ( SELECT softID FROM mpi_lizenz WHERE lizenzID = NEW.lizenzID );
   UPDATE mpi_software SET restLizenz = anzLizenz - instLizenz WHERE softID = ( SELECT softID FROM mpi_lizenz WHERE lizenzID = NEW.lizenzID );
  END IF;
  UPDATE mpi_key SET lizenzID = NEW.lizenzID WHERE keyID = NEW.keyID AND multikey = '0';
 END
//


DELIMITER //
DROP TRIGGER IF EXISTS `set_instlic_del`//

CREATE TRIGGER `set_instlic_del` AFTER DELETE ON `mpi_install`
 FOR EACH ROW BEGIN
  UPDATE mpi_software SET anzLizenz = (SELECT SUM(anzahl) FROM mpi_lizenz WHERE softID = ANY ( SELECT softID FROM mpi_lizenz WHERE lizenzID = OLD.lizenzID ) AND status = '1') WHERE softID = ( SELECT softID FROM mpi_lizenz WHERE lizenzID = OLD.lizenzID );
  UPDATE mpi_software SET instLizenz = (SELECT SUM(anzahl) FROM mpi_install WHERE lizenzID = ANY (SELECT lizenzID FROM mpi_lizenz WHERE softID = ( SELECT softID FROM mpi_lizenz WHERE lizenzID = OLD.lizenzID ))) WHERE softID = ( SELECT softID FROM mpi_lizenz WHERE lizenzID = OLD.lizenzID );
  UPDATE mpi_software SET restLizenz = anzLizenz - instLizenz WHERE softID = ( SELECT softID FROM mpi_lizenz WHERE lizenzID = OLD.lizenzID );
 END
//
