--
-- Trigger AFTER `mpi_install`
--
-- sichere alten pc-namnen, falls pc-db eintraege mal geloescht werden
-- starte procedure fuer versionierte Berechnung

--
-- Trigger AFTER insert mpi_install
--

DROP TRIGGER IF EXISTS set_ins_ins;
DELIMITER $$
CREATE TRIGGER set_ins_ins AFTER INSERT ON mpi_install
 FOR EACH ROW BEGIN
  INSERT IGNORE INTO list_pcname SET instID = NEW.instID, instName = (SELECT name FROM view_pc WHERE tabID = NEW.instID);
  IF NEW.verID IS NOT NULL AND NEW.lizenzID IS NOT NULL THEN CALL proc_version(NEW.verID); END IF;
 END;
$$
DELIMITER ;


--
-- Trigger AFTER update mpi_install
--

DROP TRIGGER IF EXISTS set_ins_upd;
DELIMITER $$
CREATE TRIGGER set_ins_upd AFTER UPDATE ON mpi_install
 FOR EACH ROW BEGIN
  INSERT IGNORE INTO list_pcname SET instID = NEW.instID, instName = (SELECT name FROM view_pc WHERE tabID = NEW.instID);
  UPDATE list_pcname SET instName = (SELECT name FROM view_pc WHERE tabID = NEW.instID) WHERE instID = NEW.instID;
  IF NEW.verID IS NOT NULL AND NEW.lizenzID IS NOT NULL THEN CALL proc_version(NEW.verID); END IF;
  IF OLD.verID IS NOT NULL AND OLD.lizenzID IS NOT NULL THEN CALL proc_version(OLD.verID); END IF;
 END;
$$
DELIMITER ;


--
-- Trigger AFTER delete mpi_install
--

DROP TRIGGER IF EXISTS set_ins_del;
DELIMITER $$
CREATE TRIGGER set_ins_del AFTER DELETE ON mpi_install FOR EACH ROW
 BEGIN
  IF OLD.verID IS NOT NULL AND OLD.lizenzID IS NOT NULL THEN CALL proc_version(OLD.verID); END IF;
 END;
$$
DELIMITER ;

