<?php

class tables_mpi_install { 

  function getTitle(&$record) {
    $tabID = $record->val('instID');
    $sql = "SELECT name FROM view_pc WHERE tabID = '$tabID'";
    list($pc) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return $pc;
  }

  function block__after_instID_widget() {
    $app    = Dataface_Application::getInstance();
    $path   = $app->_conf['_own']['inv_path'];
    $urlAdd = "/$path/index.php?-action=new&-table=mpi_geraete";
    echo '<a href='.$urlAdd.'><img src="/xataface/images/add_icon.gif" alt="" width="17" height="17"> Neuen Computer hinzufügen (Ext.)</a>';
  }

  function block__after_result_list_content() {
    echo 'Installation';
    echo '<span style="background-color:#cfc;"> ist aktiv. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#edf3fe;"> ist inaktiv. </span>&nbsp;';
  }

 // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    if ( $record->strval('status') == '0' ) return $table.'normal';
    return $table.'enough';
  }

  function valuelist__status(){
       return array(0=>'inaktiv', 1=>'aktiv');
  }

  function instName__rendercell(&$record) {
    $table  = 'list_pcname';
    $action = 'browse';
    $field  = 'instID';
    $tabID  = $record->val($field);
    $name   = $record->val('instName');
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function instID__renderCell(&$record) {
    $app    = Dataface_Application::getInstance();
    $path   = $app->_conf['_own']['inv_path'];
    $table  = 'mpi_geraete';
    $action = 'browse';
    $field  = 'tabID';
    $tabID  = $record->val('instID');
    $url    = "/$path/index.php?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql    = "SELECT name FROM view_pc WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    if (!isset($name)) $name = '--';
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function softID__renderCell( &$record ) {
    $table  = 'mpi_software';
    $action = 'browse';
    $field  = 'softID';
    $tabID  = $record->val($field);
    $name   = $record->display($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function verID__renderCell( &$record ) {
    $table  = 'mpi_version';
    $action = 'browse';
    $field  = 'verID';
    $tabID  = $record->val($field);
    $name   = $record->display($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function lizenzID__renderCell( &$record ) {
    $table  = 'mpi_lizenz';
    $action = 'browse';
    $field  = 'lizenzID';
    $tabID  = $record->val($field);
    $name   = $record->display($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function keyID__renderCell( &$record ) {
    $table  = 'mpi_key';
    $action = 'browse';
    $field  = 'keyID';
    $tabID  = $record->val($field);
    $name   = $record->display($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  // Zeitanzeige aendern
  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

}

?>
