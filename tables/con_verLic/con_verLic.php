<?php

class tables_con_verLic { 

  function getTitle(&$record) {
    return $record->display('verID').' : '.$record->display('lizenzID');
    //return $record->val('autoID');
  }

  function softID__renderCell( &$record ) {
    $table  = 'mpi_software';
    $action = 'browse';
    $field  = 'softID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql    = "SELECT software FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function verID__renderCell( &$record ) {
    $table  = 'mpi_version';
    $action = 'browse';
    $field  = 'verID';
    $tabID  = $record->val($field);
    $name   = $tabID;
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql    = "SELECT CONCAT(soft.software,' : ',ver.version) FROM $table AS ver JOIN mpi_software AS soft ON soft.softID = ver.softID WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function lizenzID__renderCell( &$record ) {
    $table  = 'mpi_lizenz';
    $action = 'browse';
    $field  = 'lizenzID';
    $tabID  = $record->val($field);
    $name   = $tabID;
    $sql    = "SELECT IFNULL(bezeichnung, lizenzID) FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  // check ob beide zu einer software gehoeren
  function beforeSave(&$record) {
    $verID = $record->val('verID');
    $licID = $record->val('lizenzID');
    // nur im edit mode checken
    if (( $verID > '0' ) AND ( $licID > '0' )) {
      $sql = "SELECT softID FROM mpi_version WHERE verID = '$verID'";
      list($soft1) = xf_db_fetch_row(xf_db_query($sql, df_db()));
      $sql = "SELECT softID FROM mpi_lizenz WHERE lizenzID = '$licID'";
      list($soft2) = xf_db_fetch_row(xf_db_query($sql, df_db()));
      if ( $soft1 != $soft2 ) {
        return Dataface_Error::permissionDenied('con_verLic: Zugewiesene Version - '.$verID.' und zugeordnete Lizenz -'.$licID.'- haben keine identische Software!');
      }
    }
  }

}
?>
