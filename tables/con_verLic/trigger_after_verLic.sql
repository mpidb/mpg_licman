-- add,del DS in show_version
-- dies ginge auch per function-aufruf pro raw, aber der dauert wegen der func-Aufrufe zu lange
--
-- Trigger AFTER insert `con_verLic`
--

DROP TRIGGER IF EXISTS set_con_ins;
DELIMITER $$
CREATE TRIGGER set_con_ins AFTER INSERT ON con_verLic FOR EACH ROW
 BEGIN 
  INSERT IGNORE INTO show_version (verID) VALUES (NEW.verID);
  CALL proc_version(NEW.verID);
 END;
$$
DELIMITER ;


--
-- Trigger AFTER update `con_verLic`
--

-- OLD und NEW nur notwendig, wenn wechsel zur einer anderen software und das geht eigentlich nicht
DROP TRIGGER IF EXISTS set_con_upd;
DELIMITER $$
CREATE TRIGGER set_con_upd AFTER UPDATE ON con_verLic FOR EACH ROW
 BEGIN 
  CALL proc_version(OLD.verID);
 END;
$$
DELIMITER ;

--
-- Trigger AFTER delete `con_verLic`
--

DROP TRIGGER IF EXISTS set_con_del;
DELIMITER $$
CREATE TRIGGER set_con_del AFTER DELETE ON con_verLic FOR EACH ROW
 BEGIN
  DELETE FROM show_version WHERE verID = OLD.verID AND OLD.verID NOT IN (SELECT verID FROM con_verLic);
  CALL proc_version(OLD.verID);
 END;
$$
DELIMITER ;
