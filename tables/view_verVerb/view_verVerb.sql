-- zeige verbrauchte lizenzen pro version
-- nur ds wo auch verbrauch existiert
-- V 0.9.03
CREATE OR REPLACE VIEW view_verVerb AS
SELECT
 ver.verID,
 -- ver.softID,
 SUM(vins.uselizenz) AS useLizenz
FROM
 mpi_version AS ver
 INNER JOIN view_instLic AS vins ON ver.verID = vins.verID
GROUP BY
 ver.verID
;


