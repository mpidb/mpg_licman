-- auswertung verbundene lizenzen
CREATE OR REPLACE VIEW view_vertrag AS
SELECT
 vertragID,
 count(vertragID) AS anzVertrag
FROM
 mpi_lizenz
WHERE
 vertragID IS NOT NULL
GROUP BY
 vertragID
;


