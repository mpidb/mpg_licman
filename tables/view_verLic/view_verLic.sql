-- zeige anzahl verfuegbarer lizenzen pro version
-- version muss in lizenz zugeordnet sein und status aktiv haben
-- V 0.9.03
CREATE OR REPLACE VIEW view_verLic AS
SELECT
 ver.verID,
 lic.lizenzID,
 IF ( count(lic.modell) > 1, 'Mixed', lic.modell ) AS modell,
 SUM( lic.anzahl ) AS anzLizenz
FROM
 mpi_version AS ver
 LEFT JOIN mpi_lizenz AS lic ON lic.verID = ver.verID
WHERE
 lic.verID IS NOT NULL AND lic.status = 1
GROUP BY
 ver.verID
;


-- zeige verbrauchte und zugeordnete Lizenzen 
-- V0.9.02
SELECT
 ver.softID,
 vins.lizenzID,
 SUM(vins.uselizenz) AS useLizenz
FROM
 mpi_version AS ver
 INNER JOIN view_instLic AS vins ON ver.verID = vins.verID
GROUP BY
 vins.lizenzID
;

 
-- summiere lizenzen pro version nach zugehoerigkeit version zu lizenz
SELECT
 ver.softID,
 lic.lizenzID,
 SUM( vuse.useVersion ) AS sumVerbrauch
FROM
 mpi_version AS ver
 LEFT JOIN con_verLic  AS con ON ver.verID = con.verID
 LEFT JOIN mpi_lizenz AS lic ON lic.lizenzID = con.lizenzID
 LEFT JOIN view_verVerb vuse ON ver.verID = vuse.verID
GROUP BY lic.lizenzID
;

