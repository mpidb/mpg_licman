-- loesche DS wenn keine Bezug mehr existiert
-- wenn ich auf tabelle arbeite, kann ich nicht triggern auf der selbigen - geht nur in mysql nicht
-- Can't update table 'mpi_ablage' in stored function/trigger because it is already used by statement which invoked this stored function/trigger. 
-- geloest ueber mpi_ablage.php
--
-- Trigger AFTER update `mpi_ablage`
--


-- ERROR wenn in ablage selbst save

DROP TRIGGER IF EXISTS `set_abl_del`;
DELIMITER $$
CREATE TRIGGER `set_abl_del` AFTER UPDATE ON `mpi_ablage` FOR EACH ROW
 BEGIN
  DELETE FROM mpi_ablage WHERE
   ablageID = OLD.ablageID AND OLD.softID IS NULL AND OLD.vertragID IS NULL AND OLD.lizenzID IS NULL;
 END;
$$
DELIMITER ;

