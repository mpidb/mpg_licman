<?php

class tables_mpi_ablage { 

  // glanceview
  function getTitle(&$record) {
    return $record->display('vorgang').' : '.$record->display('file_filename');
  }

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }
  
  function vertragID__renderCell( &$record ) {
    if ( $record->val('vertragID') == NULL ) return;
    $table  = 'mpi_vertrag';
    $action = 'browse';
    $field  = 'vertragID';
    $tabID  = $record->val($field);
    $name   = $record->display($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function softID__renderCell(&$record) {
    if ( $record->val('softID') == NULL ) return;
    $table  = 'mpi_software';
    $action = 'browse';
    $field  = 'softID';
    $tabID  = $record->val($field);
    $name   = $record->display($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function lizenzID__renderCell( &$record ) {
    if ( $record->val('lizenzID') == NULL ) return;
    $table  = 'mpi_lizenz';
    $action = 'browse';
    $field  = 'lizenzID';
    $tabID  = $record->val($field);
    $name   = $record->display($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function srvID__renderCell( &$record ) {
    $table  = 'mpi_licSrv';
    $action = 'browse';
    $field  = 'srvID';
    $tabID  = $record->val($field);
    $name   = $record->display($field);
    //$sql = "SELECT CONCAT(server, IF(alias IS NOT NULL, CONCAT(' (',alias,')'),'')) FROM $table WHERE $field = '$tabID'";
    //list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function file_filename__renderCell( &$record ) {
    $table  = 'mpi_ablage';
    $action = 'getBlob';
    $field  = 'ablageID';
    $tabID  = $record->val($field);
    $name   = $record->strval('file_filename');
    //$url = $record->getURL('-action=getBlob');
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&-field=file&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
    //return '<a href="'.DATAFACE_SITE_HREF.'?-action=getBlob&-table='.$table.'&-field=file&-index=0&ablageID='.$tabID.'">'.$file.'</a>';
  }

  // Zeitanzeige aendern
  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);

    // das gibt probleme
    //if ($record->val('related_lizenzID')) $record->setValue('lizenzID', $record->val('related_lizenzID'));

    if ($record->val('file_filename') == NULL) {
      return Dataface_Error::permissionDenied('Was soll denn eine Abage ohne Fileupload!');
    }
    if (($record->val('softID') == NULL) AND ($record->val('vertragID') == NULL) AND ($record->val('lizenzID') == NULL) AND ($record->val('srvID') == NULL)) {
      return Dataface_Error::permissionDenied('Wenigstens eine Zuordnung sollte es schon geben!');
    }
  }

/*
  // per cronjob ausgefuehrt
  // loesche - wenn du gerade mal hier bist - global alle nicht mehr verlinkten ablagen, aber nicht den aktuellen DS (nur wegen loop in trigger hier geloest)
  function afterSave(&$record) {
    $tabID = $record->val('ablageID');
    $sql = "DELETE FROM mpi_ablage WHERE ablageID != '$tabID' AND softID IS NULL AND vertragID IS NULL AND lizenzID IS NULL";
    $res = xf_db_query($sql, df_db());
  }
*/

}
?>
