<?php

class tables_list_lieferant { 

  function email__renderCell( &$record ) {
    $mail = $record->strval('email');
    return '<a href="mailto:'.$mail.'">'.$mail.'</a>';
  }

  function webseite__renderCell( &$record ) {
    $web = $record->strval('webseite');
    if (($web != NULL) AND (strpos($web,'://') === FALSE )) {
      return '<a href="http://'.$web.'">http://'.$web.'</a>';
    }
    return '<a href="'.$web.'">'.$web.'</a>';
  }

  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) {
      return "";
    } else {
      return date('d.m.Y', strtotime($record->strval('zeitstempel')));
    }
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    // setze user
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

}

?>
