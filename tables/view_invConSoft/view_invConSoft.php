<?php

class tables_view_invConSoft {

  // only authenticated user; edit in view not allowed; check role lower READ ONLY
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    if ( strpos($user->val('role'),'VIEW GROUP ') !== false) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

  function getTitle(&$record) {
    return $record->strval('software').' : '.$record->strval('version');
  }

  function conID__renderCell( &$record ) {
    $table  = 'con_softInv';
    $action = 'browse';
    $field  = 'conID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $name   = $tabID;
    return '<a href="'.$url.'">'.$name.'</a></div>';
  }

  function software__renderCell( &$record ) {
    $table  = 'mpi_inventar';
    $action = 'list';
    $soft   = $record->strval('software');
    $vers   = $record->strval('version');
    $name   = $soft;
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&software=${soft}&version=${vers}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

}
?>

