-- nur fuer anzeige relationship con_softInv
CREATE OR REPLACE VIEW view_invConSoft AS
SELECT
 conID,
 software,
 version,
 publisher,
 count(software) AS anzahl,
 bearbeiter
FROM
 mpi_inventar
WHERE
 conID IS NOT NULL
GROUP BY
 software, version
;
