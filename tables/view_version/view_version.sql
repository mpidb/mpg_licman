-- zeige rest von verfuegbaren und verbrauchten lizenzen pro version
-- aber nur welche downgraderecht haben
-- mit function
-- V0.9.04
CREATE OR REPLACE VIEW view_version AS
SELECT
 ver.softID,
 ver.verID,
 ver.orderID,
 vlic.lizenzID,
 vlic.modell,
 vlic.anzLizenz,
 vuse.useLizenz,
 shw.diffVersion,
 shw.restVersion,
 shw.verbVersion,
 shw.freeVersion
FROM
 mpi_version AS ver
 LEFT JOIN view_verVerb vuse ON vuse.verID = ver.verID
 LEFT JOIN view_verLic  vlic ON vlic.verID = ver.verID
 LEFT JOIN show_version  shw ON shw.verID  = ver.verID
WHERE
 ver.verID IN (SELECT verID FROM con_verLic)
ORDER BY
 softID, orderID, version
;

