<?php

class tables_view_version {

  // only authenticated user; edit in view not allowed; check role lower READ ONLY
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    if ( strpos($user->val('role'),'VIEW GROUP ') !== false) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

  function getTitle(&$record) {
    $anzLiz = $record->strval('anzLizenz');
    if ( $anzLiz == NULL ) $anzLiz = '0'; elseif ( $anzLiz == '0' ) return 'unbegrenzt';
    $useLiz = $record->display('useLizenz');
    if ( $useLiz == NULL ) $useLiz = '0';
    $table  = 'mpi_version';
    $action = 'browse';
    $field  = 'verID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT version FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return $name.' : '.$anzLiz.' - '.$useLiz.' = '.$record->display('freeLizenz');
  }

  function softID__renderCell( &$record ) {
    $table  = 'mpi_software';
    $action = 'browse';
    $field  = 'softID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql    = "SELECT software FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function verID__renderCell( &$record ) {
    $table  = 'mpi_version';
    $action = 'browse';
    $field  = 'verID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT version FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function lizenzID__renderCell( &$record ) {
    $table  = 'mpi_lizenz';
    $action = 'browse';
    $field  = 'lizenzID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT bezeichnung FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function anzLizenz__renderCell(&$record) {
    $anz = $record->strval('anzLizenz');
    if ( $anz == NULL ) return '-';
    if ( $anz == '0' ) return '&infin;';
    return $anz;
  }

  function useLizenz__renderCell(&$record) {
    $use = $record->strval('useLizenz');
    if ( $use == NULL ) return '-';
    return $use;
  }

  function freeVersion__renderCell(&$record) {
    if ( $record->strval('anzLizenz') == '0' ) return '&infin;';
    $free = $record->strval('freeVersion');
    if ( $free < '0' ) return '<font color="red">'.$free.'</font>';
    if ( $free == '0' ) return '&#48';
    return $free;
  }

/*
  // fruehere Berechung der Lizenzen
  function useLizenz__renderCell( &$record ) {
    $softID = $record->val('softID');
    $tabID  = $record->val('verID');
    $vers   = $record->val('version');
    $ordn   = $record->val('orderID');
    $free   = $record->val('freeVersion');
    $max    = '99';
    // abfrage des davorliegenden restwertes, wenn vorhanden
    $sql  = "SELECT IFNULL(SUM( freeVersion ), 0) AS freeLizenz FROM view_version WHERE ( orderID BETWEEN '0' AND ( $ordn -1 )) AND softID = $softID";
    list($last) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    if ( $last < '0' ) {
      $sql  = "SELECT IFNULL( SUM( freeVersion ), 0 ) AS freeLizenz FROM view_version WHERE ( orderID BETWEEN '0' AND $ordn ) AND softID = $softID";
    } else {
      // finde den ersten ordnungswert
      $sql  = "SELECT min(orderID) FROM mpi_version WHERE softID = $softID";
      list($min) = xf_db_fetch_row(xf_db_query($sql, df_db()));
      if (( $min == $ordn ) OR ( $free >= '0' )) $max = $ordn;
      $sql  = "SELECT IFNULL( SUM( freeVersion ), 0 ) AS freeLizenz FROM view_version WHERE ( orderID BETWEEN $ordn AND $max ) AND softID = $softID";
    }
    list($value) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    //print_r ($sql);
    if ( $value == '0' ) return '&#48;';
    return $value;
  }
*/
/*
  function gPreis__rendercell(&$record) {
    if ( $record->val('gPreis') > '0' ) return '<div style="text-align:right;">'.$record->val('gPreis').' &euro;</div>';
    return;
  }
*/

}
?>
