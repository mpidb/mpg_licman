<?php

class tables_mpi_kosten { 

  // glanceview
  function getTitle(&$record) {
    return $record->display('buchung').' : '.$record->display('kostenstelle');
  }

  function ablageID__renderCell( &$record ) {
    $field = 'ablageID';
    $table = 'mpi_ablage';
    $tabID = $record->strval($field);
    $sql = "SELECT vorgang FROM mpi_ablage WHERE ablageID='$tabID'";
    list($vorgang) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<a href="'.DATAFACE_SITE_HREF.'?-table='.$table.'&-action=view&-mode=list&'.$field.'='.$tabID.'">'.$vorgang.'</a>';
  }

  // Zeitanzeige aendern
  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) {
      return "";
    } else {
      return date('d.m.Y', strtotime($record->strval('zeitstempel')));
    }
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

  function block__before_result_list_content() {
    import('Dataface/RecordReader.php');
    $app =& Dataface_Application::getInstance();
    $query = $app->getQuery();
    $records = new Dataface_RecordReader($query, 30, false);
    $miete = array();
    $wartg = array();
    $summe = array();
    foreach ($records as $record) {
      $waehr = ($record->val('waehrung'));
      $miete[$waehr] = $miete[$waehr] + $record->val('miete');
      $wartg[$waehr] = $wartg[$waehr] + $record->val('wartung');
      $summe[$waehr] = $miete[$waehr] + $wartg[$waehr];
    }
    
    echo '<table><thead></thead>';
    echo  '<tbody>';
    echo   '<div class="resultlist-filters"><h3>Summe Filter: </h3><ul>';
    echo   'Miete: ';
    foreach ($miete as $field => $value) {
      echo $value.' ';
      switch ($field) {
        case 'Euro':   echo '&euro;'; break;
        case 'Dollar': echo '&#36;';  break;
        case 'Pfund':  echo '&pound;';break;
        default: echo $value;
      }
      echo '&nbsp;&nbsp;&nbsp';
    }
    echo    '&nbsp;&nbsp;&nbspWartung: ';
    unset($field);
    unset($value);
    foreach ($wartg as $field => $value) { 
      echo $value.' ';
      switch ($field) {
        case 'Euro':   echo '&euro;'; break;
        case 'Dollar': echo '&#36;';  break;
        case 'Pfund':  echo '&pound;';break;
        default: echo $value;
      }
      echo '&nbsp;&nbsp;&nbsp';
    }
    unset($field);
    unset($value);
    echo    '&nbsp;&nbsp;&nbspGesamt: ';
    foreach ($summe as $field => $value) {
      echo '<span style="color:red;">'.$value.'</span> ';
      switch ($field) {
        case 'Euro':   echo '&euro;'; break;
        case 'Dollar': echo '&#36;';  break;
        case 'Pfund':  echo '&pound;';break;
        default: echo $value;
      }
      echo '&nbsp;&nbsp;&nbsp';
    }
    echo    '</ul>';
    echo   '</div>';
    echo  '</tbody>';
    echo '</table>';
  }

}
?>

