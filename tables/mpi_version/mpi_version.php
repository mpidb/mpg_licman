<?php

class tables_mpi_version { 

 // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }

  // button update & remove in views weg
  function rel_verbrauch__permissions($record) {
    return array( 'remove related record' => 0, 'update related records' => 0 );
  }

  function getTitle(&$record) {      
    return $record->display('softFilter').' : '.$record->display('version');
  }

  // Anzeige in 'add releated record'
  function titleColumn() {
    return "CONCAT(softFilter,' : ',version)";
  }

/*
  // zeige nur verfuegbare Ordnungen, funktioniert zwar, blendet aber vorhandene aus - wie bei depselect, daher aus 
  function valuelist__ordnung() {
    $curRec =& Dataface_Application::getInstance()->getRecord();
    $softID = $curRec->strval('softID');
    static $ordn = -1;
    if ( !is_array($ordn )) {
        $ordn = array();
        $res = xf_db_query("SELECT orderID, ordnung FROM list_ordnung WHERE orderID NOT IN (SELECT orderID FROM mpi_version WHERE softID = $softID ) ORDER BY orderID", df_db());
        if ( !$res ) throw new Exception(xf_db_error(df_db()));
        while ($row = xf_db_fetch_row($res) ) $ordn[$row[0]] = $row[1];
    }
    return $ordn;
  }
*/

  // link auf interne db
  function softID__renderCell(&$record) {
    $table  = 'mpi_software';
    $action = 'browse';
    $field  = 'softID';
    $tabID  = $record->val($field);
    $name   = $record->val('softFilter');
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function gPreis__rendercell(&$record) {
    if ( $record->val('gPreis') > 0 ) return '<div style="text-align:right;">'.$record->val('gPreis').' &euro;</div>';
    return;
  }

  // Zeitanzeige aendern
  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

// User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

}

?>
