<?php

class tables_view_software {

  // only authenticated user; edit in view not allowed; check role lower READ ONLY
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    if ( strpos($user->val('role'),'VIEW GROUP ') !== false) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

  function getTitle(&$record) {
    $anzLiz = $record->strval('anzLizenz');
    $name   = '';
    $field  = 'verID';
    $tabID  = $record->val($field);
    if ( $tabID != NULL ) {
      $table  = 'mpi_version';
      $action = 'browse';
      $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
      $sql = "SELECT version FROM $table WHERE $field = '$tabID'";
      list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
      $vers = 'V'.$name.' : ';
    }
    if ( $anzLiz == NULL ) $anzLiz = '0';
    elseif ( $anzLiz == '0' ) return  $vers.'unbegrenzt';
    //elseif ( $record->strval('modell') == 'Concurrent' ) return  $vers.'concurrent';
    $useLiz = $record->display('useLizenz');
    if ( $useLiz == NULL ) {
      return $vers.$anzLiz.'(vh) : '.$record->display('verbLizenz').'(vb) : '.$record->display('freeLizenz').'(vf)';
    }
    return $vers.$anzLiz.'(vh) : '.$useLiz.'(zg) : '.$record->display('verbLizenz').'(vb) : '.$record->display('freeLizenz').'(vf)';
  }

  function softID__renderCell( &$record ) {
    $table  = 'mpi_software';
    $action = 'browse';
    $field  = 'softID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql    = "SELECT software FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function verID__renderCell( &$record ) {
    $table  = 'mpi_version';
    $action = 'browse';
    $field  = 'verID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT version FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function lizenzID__renderCell( &$record ) {
    $table  = 'mpi_lizenz';
    $action = 'browse';
    $field  = 'lizenzID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT bezeichnung FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function anzLizenz__renderCell(&$record) {
    if ( $record->strval('anzLizenz') == NULL ) return '-';
    if ( $record->strval('anzLizenz') == '0' ) return '&#48';
    return $record->strval('anzLizenz');
  }

  function useLizenz__renderCell(&$record) {
    //if ( $record->strval('useLizenz') == NULL ) return '-';
    if ( $record->strval('useLizenz') == '0' ) return '&#48';
    return $record->strval('useLizenz');
  }

  function freeLizenz__renderCell(&$record) {
    if ( $record->strval('anzLizenz') == '0' ) return '&infin;';
    if ( $record->strval('modell') == 'Concurrent' ) return '&infin;';
    $free = $record->strval('freeLizenz');
    if ( $free == '0' ) return '&#48';
    if ( $free < '0' ) return '<font color="red">'.$free.'</font>';
    return $free;
  }

/*
  function gPreis__rendercell(&$record) {
    if ( $record->val('gPreis') > '0' ) return '<div style="text-align:right;">'.$record->val('gPreis').' &euro;</div>';
    return;
  }
*/

}
?>
