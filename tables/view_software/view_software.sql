-- union aus view_lizenz und view_version
-- V0.9.04
CREATE OR REPLACE VIEW view_software AS
SELECT
 softID,
 verID,
 lizenzID,
 modell,
 anzLizenz,
 NULL AS useLizenz,
 useLizenz AS verbLizenz,
 freeLizenz
FROM 
 view_lizenz
UNION ALL
SELECT
 softID,
 verID,
 lizenzID,
 modell,
 anzLizenz,
 useLizenz,
 verbVersion AS verbLizenz,
 freeVersion AS freeLizenz
FROM
 view_version
;


