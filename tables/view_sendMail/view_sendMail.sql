CREATE OR REPLACE VIEW view_sendMail AS
 SELECT
  soft.software AS name,
  lic.lizenzID,
  vert.vertragID,
  lic.expSupport AS expireDate,
  1 As anz,
  lic.email,
  CAST('Support' AS CHAR CHARACTER SET utf8) COLLATE utf8_unicode_ci AS body,
  vert.expiry, vert.nachricht, NULL AS setExpLizenz
 FROM
  mpi_lizenz AS lic
  LEFT JOIN mpi_software AS soft ON lic.softID = soft.softID
  LEFT JOIN mpi_vertrag AS vert ON lic.vertragID = vert.vertragID
 WHERE
  ( lic.expSupport IS NOT NULL ) AND
  ( lic.status = 1 ) AND
  ( lic.nachricht = 1 ) AND
  ( lic.email LIKE '%@%.%' ) AND
  (( vert.nachricht = 0 ) OR ( vert.expiry IS NULL ))
 UNION ALL
 SELECT
  soft.software AS name,
  lic.lizenzID,
  vert.vertragID,
  lic.expLizenz AS expireDate,
  1 As anz,
  lic.email,
  CAST('String' AS CHAR CHARACTER SET utf8) COLLATE utf8_unicode_ci AS body,
  vert.expiry, vert.nachricht, vert.setExpLizenz
 FROM
  mpi_lizenz AS lic
  LEFT JOIN mpi_software AS soft ON lic.softID = soft.softID
  LEFT JOIN mpi_vertrag AS vert ON lic.vertragID = vert.vertragID
 WHERE
  ( lic.expLizenz IS NOT NULL ) AND
  ( lic.status = 1 ) AND
  ( lic.nachricht = 1 ) AND
  ( lic.email LIKE '%@%.%' ) AND
  (( vert.nachricht = 0 ) OR ( vert.expiry IS NULL ) OR ( vert.setExpLizenz = 0 ))
 UNION ALL
 SELECT
  vert.vertrag AS name,
  lic.lizenzID,
  vert.vertragID,
  vert.expiry AS expireDate,
  count(lic.lizenzID) AS anz,
  vert.email,
  CAST('Vertrag' AS CHAR CHARACTER SET utf8) COLLATE utf8_unicode_ci AS body,
  vert.expiry, vert.nachricht, vert.setExpLizenz
 FROM
  mpi_vertrag AS vert
  LEFT JOIN mpi_lizenz AS lic ON vert.vertragID = lic.vertragID
  LEFT JOIN mpi_software AS soft ON lic.softID = soft.softID
 WHERE
  ( vert.expiry IS NOT NULL ) AND
  ( vert.status = 1 ) AND
  ( vert.nachricht = 1 ) AND
  ( vert.email LIKE '%@%.%' )
 GROUP BY
  vert.vertragID
 ORDER BY name
;


-- alte php-prog variable
-- $opt   = 'AND (( vert.nachricht = 0 ) OR ( vert.expiry IS NULL ) OR ( IF( '$field' = 'lic.expLizenz', vert.setExpLizenz, 1 ) = 0 ))'
