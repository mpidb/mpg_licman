<?php

class tables_view_sendMail { 

  // only authenticated user; edit in view not allowed; check role lower READ ONLY
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    if ( strpos($user->val('role'),'VIEW GROUP ') !== false) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

  function software__renderCell( &$record ) {
    $table  = 'mpi_software';
    $action = 'browse';
    $field  = 'software';
    $name   = $record->val($field);
    $sql    = "SELECT softID FROM $table WHERE $field = '$name'";
    list($tabID) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&softID=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function lizenzID__renderCell( &$record ) {
    $table  = 'mpi_lizenz';
    $action = 'browse';
    $field  = 'lizenzID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT bezeichnung FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function vertragID__renderCell( &$record ) {
    if ( $record->strval('vertragID') == NULL ) return;
    $table = 'mpi_vertrag';
    $action = 'browse';
    $field  = 'vertragID';
    $tabID  = $record->strval($field);
    $name   = $tabID;
    $sql    = "SELECT vertrag FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function expireDate__rendercell(&$record) {
    $datum = $record->strval('expireDate');
    if ($datum == NULL) return;
    return date('d.m.Y', strtotime($datum));
  }

  function expiry__rendercell(&$record) {
    $datum = $record->strval('expiry');
    if ($datum == NULL) return;
    return '<font color="blue">'.date('d.m.Y', strtotime($datum)).'</font>';
  }

  function nachricht__display(&$record)  {
    if ($record->val('nachricht') == '1') return 'ja'; else return 'nein';
  }

  function setExpLizenz__display(&$record)  {
    if ($record->val('setExpLizenz') == '1') return 'ja'; else return 'nein';
  }

}
?>
