CREATE OR REPLACE VIEW view_invCon AS
SELECT
 *
FROM
 mpi_inventar
WHERE
 conID IS NOT NULL
;

-- full search
-- CREATE OR REPLACE VIEW view_invCon_full AS
SELECT
 inv.software,
 inv.version,
 inv.instName,
 con.conID,
 con.softID,
 con.verID,
 con.lizenzID
FROM
 mpi_inventar AS inv
 LEFT JOIN con_softInv AS con ON
 ((searchEn = '1') AND (versionEn = '0') AND (searchCmd = '0') AND (inv.software LIKE searchStr)) OR
 ((searchEn = '1') AND (versionEn = '1') AND (searchCmd = '0') AND (inv.software LIKE searchStr) AND (inv.version = con.version)) OR
 ((searchEn = '1') AND (versionEn = '0') AND (searchCmd = '1') AND (inv.software RLIKE searchStr)) OR
 ((searchEn = '1') AND (versionEn = '1') AND (searchCmd = '1') AND (inv.software RLIKE searchStr) AND (inv.version = con.version)) OR
 ((searchEn = '0') AND (versionEn = '0') AND (inv.software = con.software)) OR
 ((searchEn = '0') AND (versionEn = '1') AND (inv.software = con.software) AND (inv.version = con.version))
ORDER BY instName, software
;
