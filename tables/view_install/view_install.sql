-- auswertung lizenzzaehlung
-- schmeiss alle DS raus die keiner lizenz zugeordnet sind
-- wegen subquery in views hier der erste extra view
CREATE OR REPLACE VIEW view_install AS
SELECT
 tabID,
 instName,
 softID,
 verID,
 lizenzID,
 inventar,
 useLizenz,
 anzTreffer,
 bearbeiter
FROM
 view_invInst
WHERE
 lizenzID IS NOT NULL
;


