<?php

class tables_mpi_licSrv { 

  // glanceview and selectbox
  function getTitle(&$record) {
    if ( $record->display('alias') != NULL ) return $record->display('server').' ('.$record->display('alias').')';
    return $record->display('server');
  }

  function softID__renderCell(&$record) {
    $table  = 'mpi_software';
    $action = 'browse';
    $field  = 'softID';
    $tabID  = $record->val($field);
    $sql    = "SELECT software FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function server__renderCell(&$record) {
    $app    = Dataface_Application::getInstance();
    $path   = $app->_conf['_own']['inv_path'];
    $table  = 'mpi_geraete';
    $action = 'browse';
    $field  = 'tabID';
    $name   = $record->strval('server');
    $sql    = "SELECT tabID FROM view_pc WHERE name = '$name'";
    list($tabID) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    if (isset($tabID)) { 
      $url    = "/$path/index.php?-table=${table}&-action=${action}&${field}=${tabID}";
      return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
    }
    return $name;
  }

  // Zeitanzeige aendern
  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) {
      return "";
    } else {
      return date('d.m.Y', strtotime($record->strval('zeitstempel')));
    }
  }

// User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

}

?>
