-- nur wegen depSelect angelegt
CREATE OR REPLACE VIEW sort_inventar AS
SELECT
 software,
 version,
 publisher,
 count(software) AS anzahl
FROM
 mpi_inventar
GROUP BY
 software, version
ORDER BY
 software
