-- zeige als grafted field wo schluessel bereits verwendet worden
CREATE OR REPLACE VIEW view_keyUsed AS
SELECT
 sch.keyID,
 sch.multikey,
 CASE
  WHEN man.instID > 0 AND aut.conID > 0 THEN 'AM'
  WHEN man.instID > 0 THEN 'M'
  WHEN aut.conID > 0 THEN 'A'
  ELSE '-'
 END AS used,
 CASE
  WHEN man.instID > 0 AND aut.conID > 0 THEN COUNT(man.keyID) + COUNT(aut.keyID)
  WHEN man.instID > 0 THEN COUNT(man.keyID)
  WHEN aut.conID > 0 THEN COUNT(aut.keyID)
  ELSE COUNT(sch.keyID) -1
 END AS anz,
 IF (sch.multikey = 0 AND man.instID > 0, 0, 1) AS freeIns
FROM mpi_key AS sch
 LEFT JOIN con_softInv AS aut ON sch.keyID = aut.keyID
 LEFT JOIN mpi_install AS man ON sch.keyID = man.keyID
GROUP BY sch.keyID

-- falsche zaehlung
SELECT
 sch.keyID,
 sch.multikey,
 CASE 
  WHEN man.instID > 0 AND aut.conID > 0 THEN 'AM'
  WHEN man.instID > 0 THEN 'M'
  WHEN aut.conID > 0 THEN 'A'
  ELSE '-'
 END AS used,
 IF (sch.multikey = 0 AND man.instID > 0, 0, 1) AS freeIns,
 COUNT(sch.keyID) AS anz
FROM mpi_key AS sch
 LEFT JOIN con_softInv AS aut ON sch.keyID = aut.keyID
 LEFT JOIN mpi_install AS man ON sch.keyID = man.keyID
GROUP BY sch.keyID

