-- hole daten von externer pc db (eigene Version)
CREATE OR REPLACE VIEW view_pc AS 
SELECT
 tabID, name, art
FROM
 mpidb_it_inv.mpi_geraete
ORDER BY
 name
;

-- hole daten von lokaler pc tabelle (mpg-version ohne licman)
CREATE OR REPLACE VIEW mpidb_mpg_licman.view_pc AS
SELECT
 tabID, name, art
FROM
 mpi_geraete
ORDER BY
 name
;

-- hole daten von externer pc db (mpg-version mit licman)
CREATE OR REPLACE VIEW mpidb_mpg_licman.view_pc AS
SELECT
 tabID, name, art
FROM
 mpidb_mpg_inv.mpi_geraete
ORDER BY
 name
;

