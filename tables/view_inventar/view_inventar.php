<?php

class tables_view_inventar { 

  // only authenticated user; edit in view not allowed; check role lower READ ONLY
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    if ( strpos($user->val('role'),'VIEW GROUP ') !== false) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }

  function anlage__renderCell( &$record ) {
    $table  = 'view_inventar';
    $action = 'browse';
    $field  = 'anlage';
    $tabID  = $record->val($field);
    $name   = $record->display($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:right;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function unterNr__renderCell( &$record ) {
    $name   = $record->strval('unterNr');
    return '<div style="text-align:right;">'.$name.'</div>';
  }

  function lizenzID__renderCell( &$record ) {
    $table  = 'mpi_lizenz';
    $action = 'browse';
    $field  = 'lizenzID';
    $licID  = $record->val($field);
    if ($licID == NULL) {
      $name    = $record->strval('bezeichnung');
      $anlage  = $record->strval('anlage');
      $unterNr = $record->strval('unterNr');
      $url = DATAFACE_SITE_HREF."?-table=${table}&-action=new&bezeichnung=${name}&inventar=${anlage}&unterNr=${unterNr}";
      return '<a href="'.$url.'" style="text-align:right;" title="Neue Lizenz ['.$name.'] hinzufügen"><img src="images/addBlau.png"></img></a>';
    }
    $name   = $record->display($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${licID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  // Formatiere Zeitstempel auf Deutsch
  function invDatum__display(&$record)  {
    if ($record->strval('invDatum') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('invDatum')));
  }

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record)  {
    if ($record->strval('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    // setze user
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

}

?>
