-- view auf list_inventar, beschleinigt tabellenaufbau
CREATE OR REPLACE VIEW view_inventar AS
 SELECT
  inv .*,
  CONCAT(inv.anlage,':',inv.unterNr) AS inventar,
  lic.lizenzID AS lizenzID,
  IF(lic.lizenzID IS NULL, 0, 1) AS exist
 FROM
  list_inventar AS inv
  LEFT JOIN mpi_lizenz AS lic ON inv.anlage = lic.inventar AND inv.unterNr = lic.unterNr
 ;

