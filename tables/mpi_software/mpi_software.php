<?php

class tables_mpi_software { 

  function block__before_fineprint() {
    $app    = Dataface_Application::getInstance();
    $fs_ver = $app->_conf['_own']['version'];
    $mailto = $app->_conf['_own']['mailto'];
    $mname  = $app->_conf['_own']['mailname'];
    $sql    = "SELECT MAX(version) FROM dataface__version";
    list($db_ver) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $db_ver = str_pad($db_ver, 4, 0, STR_PAD_LEFT);
    $fs_rep = str_replace('.','',$fs_ver);
    if ( $fs_rep == $db_ver )
      echo 'Version <span style="color: green">FS: '.$fs_ver.'</span> = <span style="color: green">DB: '.$db_ver.'</span> &copy;<a href="mailto:'.$mailto.'">'.$mname.'</a>';
  }

  function block__before_body() {
    $app    = Dataface_Application::getInstance();
    $fs_ver = $app->_conf['_own']['version'];
    $sql    = "SELECT MAX(version) FROM dataface__version";
    list($db_ver) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $db_ver = str_pad($db_ver, 4, 0, STR_PAD_LEFT);
    $fs_rep = str_replace('.','',$fs_ver);
    if ( $fs_rep != $db_ver ) {
      if ( $fs_rep > $db_ver ) {
        echo 'Version <span style="color: green">FS: '.$fs_ver.'</span> &ne; <span style="color: red">DB: '.$db_ver.'</span>';
      } else {
        echo 'Version <span style="color: red">FS: '.$fs_ver.'</span> &ne; <span style="color: green">DB: '.$db_ver.'</span>';
      }
    }
  }

  function block__after_delete_form_message() {
    echo '<div style="color: red">Achtung: Bei der L&ouml;schung einer Software werden kaskadenartig alle abh&aumlngigen Datens&auml;tze wie Version, Installation, SuchFilter, Links mit gel&ouml;scht!</dev>';
  }

  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }

  // button update & remove in views weg
  function rel_verbrauch__permissions($record) {
    return array( 'remove related record' => 0, 'update related records' => 0 );
  }

  function gPreis__rendercell(&$record) {
    if ( $record->val('gPreis') > 0 ) return '<div style="text-align:right;">'.$record->val('gPreis').' &euro;</div>';
    return;
  }

  function hersteller__renderCell( &$record ) {
    $tabID = $record->strval('hersteller');
    $table = 'list_hersteller';
    return '<a href="'.DATAFACE_SITE_HREF.'?-table='.$table.'&-action=browse&-mode=list&hersteller='.$tabID.'">'.$tabID.'</a>';
  }

  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    // setze user
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

/*
  // wahrscheinlich uerberfluessig, daher erstmal raus
  function __import__csv(&$data, $defaultValues=array()) {
    // build an array of Dataface_Record objects that are to be inserted based
    // on the CSV file data.
    $records = array();
    // first split the CSV file into an array of rows.
    $rows = explode("\n", $data);
    // wenn Kopfzeile vorhanden
    // array_shift($rows); 
    foreach ( $rows as $row ) {
      // bug leere Zeile am Ende
      if ( !trim($row) ) continue; 
      // We iterate through the rows and parse the name, phone number, and email 
      // addresses to that they can be stored in a Dataface_Record object.
      list($software, $version, $kategorie, $nutzrecht, $hersteller, $quelle, $bemerkung, $bearbeiter) = explode(',', $row);
      $record = new Dataface_Record('mpi_software', array());
        
      // We insert the default values for the record.
      $record->setValues($defaultValues);
       
      // Now we add the values from the CSV file.
      $record->setValues(array(
        'software'=>$software,
        'version'=>$version,
        'kategorie'=>$kategorie,
        'nutzrecht'=>$nutzrecht,
        'hersteller'=>$hersteller,
        'quelle'=>$quelle,
        'bemerkung'=>$bemerkung,
        'bearbeiter'=>$bearbeiter,
      ));
      // Now add the record to the output array.
      $records[] = $record;
    }
    // Now we return the array of records to be imported.
    return $records;
  }
*/

}
?>
