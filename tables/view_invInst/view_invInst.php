<?php

class tables_view_invInst { 

  // weil fields.ini verlinkt, deshalb hier
  function __sql__() {
    return "SELECT vins.*, soft.software AS softFilter FROM view_invInst AS vins LEFT JOIN mpi_software AS soft ON vins.softID = soft.softID ORDER BY software, lizenzID, instName";
  }

  // only authenticated user; edit in view not allowed; check role lower READ ONLY
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    if ( strpos($user->val('role'),'VIEW GROUP ') !== false) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

  function valuelist__viewInv() {
       return array(0=>'man', 1=>'auto');
  }

  function tabID__renderCell(&$record) {
    $tabID  = $record->val('tabID');
     $action = 'list';
    if ($record->strval('inventar') == '1') {
      $table = 'mpi_inventar';
      $field = 'conID';
      $name2 = 'auto';
    } else {
      $table = 'mpi_install';
      $field = 'installID';
      $name2 = 'manu';
    }
    $name   = str_pad($tabID, 6, 0, STR_PAD_LEFT).' : '.$name2;
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a>';
  }


  function instID__renderCell(&$record) {
    $app    = Dataface_Application::getInstance();
    $path   = $app->_conf['_own']['inv_path'];
    $table  = 'mpi_geraete';
    $action = 'browse';
    $field  = 'tabID';
    $tabID  = $record->val('instID');
    $url    = "/$path/index.php?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql    = "SELECT name FROM view_pc WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    if (!isset($name)) $name = '--';
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function instName__renderCell(&$record) {
    $action = 'browse';
    $tabID  = $record->val('tabID');
    $name   = str_pad($tabID, 6, 0, STR_PAD_LEFT).' : '.$record->val('instName');
    if ($record->strval('inventar') == '1') {
      $field = 'conID';
      $insNa = 'instName';
      $table = 'mpi_inventar';
    } else {
      $field = 'installID';
      $insNa = 'instID';
      $table = 'mpi_install';
    }
    $instID = $record->val($insNa);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}&${insNa}=${instID}";
    return '<a href="'.$url.'">'.$name.'</a>';
  }

  function softID__renderCell( &$record ) {
    $table  = 'mpi_software';
    $action = 'browse';
    $field  = 'softID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql    = "SELECT software FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function verID__renderCell( &$record ) {
    $table  = 'mpi_version';
    $action = 'browse';
    $field  = 'verID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT version FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function lizenzID__renderCell( &$record ) {
    $table  = 'mpi_lizenz';
    $action = 'browse';
    $field  = 'lizenzID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT IFNULL(bezeichnung, lizenzID) FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function keyID__renderCell( &$record ) {
    $table  = 'mpi_key';
    $action = 'browse';
    $field  = 'keyID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT `key` FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

}

?>
