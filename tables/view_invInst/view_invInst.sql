-- auswertung lizenzzaehlung
-- eleminiere doppelte instID zuweisung aus mpi_install & mpi_inventar
-- wegen subquery in views hier der erste extra view
-- instName muss mit rein, weil sonst view gleiche DS hat, weil manche instName nicht in pc-db
CREATE OR REPLACE VIEW view_invInst AS
SELECT
 aut.conID AS tabID,
 vpc.tabID AS instID,
 aut.instName,
 aut.softID,
 aut.verID,
 aut.lizenzID,
 aut.keyID,
 1 AS inventar,
 con.anzahl AS useLizenz,
 count(aut.instName) AS anzTreffer,
 aut.bearbeiter
FROM
 mpi_inventar AS aut
 LEFT JOIN view_pc AS vpc ON aut.instName = vpc.name
 LEFT JOIN con_softInv AS con ON con.conID = aut.conID
WHERE
 aut.conID IS NOT NULL
GROUP BY
 aut.conID, aut.instName, aut.softID, aut.verID, aut.lizenzID
UNION ALL
SELECT
 man.installID AS tabID,
 man.instID,
 vpc.name AS instName,
 man.softID,
 man.verID,
 man.lizenzID,
 man.keyID,
 0 as inventar,
 man.anzahl AS useLizenz,
 count(vpc.name) AS anzTreffer,
 bearbeiter
FROM
 mpi_install AS man
 LEFT JOIN view_pc AS vpc ON man.instID = vpc.tabID
WHERE
 man.status = '1'
GROUP BY
 man.installID, vpc.name, man.softID, man.verID, man.lizenzID
;



-- last
-- benutzt fuer pc-db und auswertung autom. Install
-- sucht auch DS wo status=0 und lizenzID=NULL, besser fuer anzeige suchstrings
-- UNION ALL ist das Zauberwort - eleminiert keine double, brauch ich aber auch nicht
-- time: 2,1s !! ohne ORDER 0,34s !!
-- UNION aus install und inventar
-- time: 7,15s zu lange
-- instName muss mit rein, weil sonst view gleiche DS hat, weil manche instName nicht in pc-db
-- CREATE OR REPLACE VIEW view_invInst AS
SELECT 
 ins.installID AS tabID,
 ins.instID,
 vpc.name AS instName,
 ins.softID,
 ins.verID,
 ins.lizenzID,
 ins.keyID,
 ins.anzahl,
 '0' AS inventar,
 count(vpc.name) AS treffer,
 ins.bearbeiter
FROM
 mpi_install AS ins
 LEFT JOIN view_pc AS vpc ON ins.instID = vpc.tabID
GROUP BY
 ins.installID, vpc.name, ins.softID, ins.verID, ins.lizenzID
UNION ALL
SELECT
 inv.conID AS tabID,
 vpc.tabID AS instID,
 inv.instName,
 inv.softID,
 inv.verID,
 inv.lizenzID,
 inv.keyID,
 '1' AS anzahl,
 '1' AS inventar,
 count(inv.instName) AS treffer,
 inv.bearbeiter
FROM
 mpi_inventar AS inv
 LEFT JOIN view_pc AS vpc ON inv.instName = vpc.name
WHERE
 inv.softID IS NOT NULL
GROUP BY
 inv.conID, inv.instName, inv.softID, inv.verID, inv.lizenzID
ORDER BY
 instName, inventar, tabID
;

