<?php

class tables_mpi_key { 

 // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }

  function getTitle(&$record) {
    return $record->val('multikey').' : '.$record->val('used').'('.$record->val('anz').') : '.$record->val('key');
  }

  function valuelist__multi(){
       return array(0=>'single', 1=>'multi');
  }

  function lizenzID__renderCell( &$record ) {
    $table  = 'mpi_lizenz';
    $action = 'browse';
    $field  = 'lizenzID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT bezeichnung FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function used__rendercell(&$record) {
    $used = $record->strval('used');
    $anz  = $record->strval('anz');
    if ( $used == 'M' ) return 'Man('.$anz.')';
    if ( $used == 'A' ) return 'Auto('.$anz.')';
    if ( $used == 'AM' ) return 'Auto/Man('.$anz.')';
    return;
  }

  // Zeitanzeige aendern
  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) {
      return "";
    } else {
      return date('d.m.Y', strtotime($record->strval('zeitstempel')));
    }
  }

  function beforeSave(&$record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

  function __import__csv(&$data, $defaultValues=array()) {
    // build an array of Dataface_Record objects that are to be inserted based
    // on the CSV file data.
    $records = array();
    // first split the CSV file into an array of rows.
    $rows = explode("\n", $data);
    // wenn Kopfzeile vorhanden
    // array_shift($rows); 
    foreach ( $rows as $row ) {
      // bug leere Zeile am Ende
      if ( !trim($row) ) continue;
      // We iterate through the rows and parse the name, phone number, and email 
      // addresses to that they can be stored in a Dataface_Record object.
      list($lizenzID,$key,$softart,$multikey,$used,$lagerort,$notiz,$bearbeiter) = explode(',', $row);
      $record = new Dataface_Record('mpi_key', array());

      // We insert the default values for the record.
      $record->setValues($defaultValues);

      // Now we add the values from the CSV file.
      $record->setValues(array(
      'lizenzID'=>$lizenzID,
      'key'=>$key,
      'softart'=>$softart,
      'multikey'=>$multikey,
      'used'=>$used,
      'lagerort'=>$lagerort,
      'notiz'=>$notiz,
      'bearbeiter'=>$bearbeiter,
      ));
      // Now add the record to the output array.
      $records[] = $record;
    }
    // Now we return the array of records to be imported.
    return $records;
  }

}
?>
