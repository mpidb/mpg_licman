<?php

class tables_mpi_vertrag { 

  function getTitle(&$record) {
    return $record->strval('hersteller').' : '.$record->strval('vertrag');
  }

  function block__after_result_list_content() {
    echo 'Vereinbarung ';
    echo '<span style="background-color:#fcc;"> ist abgelaufen. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#ffc;"> l&auml;uft heute ab. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#cff;"> endet demn&auml;chst. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#cfc;"> ist g&uuml;ltig. </span>&nbsp;|&nbsp;';
    echo '<span> ist inaktiv. </span>';
  }

  function block__before_email_widget() {
    $table  = 'list_email';
    $action = 'new';
    $url  = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}";
    echo '<a href='.$url.'><img src="/xataface/images/add_icon.gif" alt="" width="17" height="17"> Neue Email hinzufügen</a>';
  }

  function valuelist__status(){
       return array(0=>'inaktiv', 1=>'aktiv');
  }

  function create__display(&$record) {
    if ($record->val('create') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('create')));
  }

  function expiry__display(&$record) {
    if ($record->val('expiry') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('expiry')));
  }

  function hersteller__renderCell( &$record ) {
    $tabID = $record->strval('hersteller');
    $table = 'list_hersteller';
    return '<a href="'.DATAFACE_SITE_HREF.'?-table='.$table.'&-action=browse&-mode=list&hersteller='.$tabID.'">'.$tabID.'</a>';
  }

  function lieferant__renderCell( &$record ) {
    $tabID = $record->strval('lieferant');
    $table = 'list_lieferant';
    return '<a href="'.DATAFACE_SITE_HREF.'?-table='.$table.'&-action=browse&-mode=list&lieferant='.$tabID.'">'.$tabID.'</a>';
  }

  function setExpLizenz__display(&$record) {
    if ($record->val('setExpLizenz') == '1') return 'ja'; else return 'nein';
  }

  function zeitstempel__display(&$record)  {
    if ($record->val('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

 // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    // return $table;  // ohne color
    if ( $record->strval('status') == '0' ) return $table.'normal';
    if ( $record->strval('expiry') == NULL ) return $table.'normal';
    $expire = $record->strval('expiry');
    $app = Dataface_Application::getInstance();
    $delay = $app->_conf['_own']['notify'];
    if (!is_numeric($delay)) $delay = 30;
    // xx Tage vorher Status auf Gelb 
    $hysDate = date('Y-m-d',mktime(0,0,0,date("m"),date("d")+$delay,date("Y")));
    $curDate = date('Y-m-d');
    if     ( $expire <  $curDate ) return $table.'toless';
    elseif ( $expire == $curDate ) return $table.'equal';
    elseif ( $expire <= $hysDate ) return $table.'narrow';
    elseif ( $expire >  $hysDate ) return $table.'enough';
    return $table.'normal';
  }

  function beforeSave(&$record) {
    // User automatisch bei neu und aendern setzen
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);

    // Leeres Feld durch NULL ersetzen
    if ($record->strval('expiry') == '') $record->setValue('expiry', NULL); 
    if ($record->strval('create') == '') $record->setValue('create', NULL); 

    if (($record->val('nachricht') == '1') AND ($record->strval('email') == NULL)) {
      return Dataface_Error::permissionDenied('Eine Aktivierung ohne Email macht keinen Sinn!');
    }
    if (($record->val('nachricht') == '1') AND ($record->strval('expiry') == NULL)) {
      return Dataface_Error::permissionDenied('Eine Aktivierung ohne Ablaufdatum macht keinen Sinn!');
    }
  }

}
?>
