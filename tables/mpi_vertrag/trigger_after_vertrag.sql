--
-- setze autom. dates, wenn mit vertrag verbunden
-- Trigger AFTER `mpi_vertrag`
-- INSERT nicht notwendig - neuer vertrag nicht vorher in lizenz auswaehlbar

-- disabled, anders geloest
DROP TRIGGER IF EXISTS `set_ver_upd`;
DELIMITER $$
CREATE TRIGGER `set_ver_upd` AFTER UPDATE ON `mpi_vertrag`
 FOR EACH ROW BEGIN
  UPDATE mpi_lizenz SET expLizenz  = NEW.expiry WHERE vertragID = NEW.vertragID AND NEW.setExpLizenz = '1';
  UPDATE mpi_lizenz SET expSupport = NEW.expiry WHERE vertragID = NEW.vertragID;
 END;
$$
DELIMITER ;



