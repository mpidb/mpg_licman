<?php

class tables_con_softInv { 

  // button update & remove in views weg
  function rel_treffer_suchstring__permissions($record) {
    return array( 'remove related record' => 0, 'update related records' => 0 );
  }

  // button update & remove in views weg
  function rel_treffer_inventory__permissions($record) {
    return array( 'remove related record' => 0, 'update related records' => 0 );
  }

  // glanceview and selectbox
  function getTitle(&$record) {
    if ($record->strval('searchEn') == '1' ) $record->strval('searchStr'); else $record->strval('software');
  }

  function block__after_result_list_content() {
    echo 'Regel';
    echo '<span style="background-color:#cfc;"> ist aktiv. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#edf3fe;"> ist inaktiv. </span>&nbsp;';
  }

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    // return $table;  // ohne color
    if ( $record->strval('status') == '0' ) return $table.'normal';
    return $table.'enough';
  }

  function software__rendercell(&$record) {
    if ($record->strval('searchEn') == '1') return '--';
    $soft = $record->strval('software');
    if (strlen($soft) > 60) return substr($soft,0,60).' ...';
    return $record->strval('software');
  }

  function valuelist__status(){
       return array(0=>'inaktiv', 1=>'aktiv');
  }

  function version__display(&$record) {
   if ($record->strval('versionEn') == '0') return '%';
    return $record->strval('version');
  }

  function searchStr__display(&$record) {
    if ($record->strval('searchEn') == '0') return '--';
    return $record->strval('searchStr');
  }

  function searchCmd__display(&$record) {
    if ($record->strval('searchCmd') == '0') return 'LIKE';
    return 'RLIKE';
  }

  function softID__renderCell( &$record ) {
    $field  = 'softID';
    $table  = 'mpi_software';
    $action = 'browse';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql    = "SELECT software FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function verID__renderCell( &$record ) {
    $field = 'verID';
    $table = 'mpi_version';
    $action = 'browse';
    $tabID = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT version FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function lizenzID__renderCell( &$record ) {
    $field = 'lizenzID';
    $table = 'mpi_lizenz';
    $action = 'browse';
    $tabID = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT IFNULL(bezeichnung, lizenzID) FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function keyID__renderCell( &$record ) {
    $field = 'keyID';
    $table = 'mpi_key';
    $action = 'browse';
    $tabID = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT `key` FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  // Zeitanzeige aendern
  function zeitstempel__display(&$record) {
    if ($record->strval('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);


    $softw  = $record->strval('software');  
    $vers   = $record->strval('version');  
    $verEna = $record->strval('versionEn');  
    $seaEna = $record->strval('searchEn');  
    $seaCmd = $record->strval('searchCmd');  
    $seaStr = $record->strval('searchStr');  
    $tabID  = $record->strval('conID');

    // bug in related records, aus 0 wird NULL 
    if ($seaEna == NULL) $seaEna = '0';
    if ($verEna == NULL) $verEna = '0';
    if ($seaCmd == NULL) $seaCmd = '0';
    //return Dataface_Error::permissionDenied( ' + '.$seaEna.' + '.$verEna.' + '.$seaCmd);

    if (($seaEna == '1') AND ($seaStr == NULL OR $seaStr == '')) {
      return Dataface_Error::permissionDenied('Wenn Software-Suche aktiv gesetzt, dann sollte dort auch ein Wert stehen.');
    }
    if (($verEna == '1') AND ($vers == NULL OR $vers == '')) {
      return Dataface_Error::permissionDenied('Wenn Versions-Suche aktiv gesetzt, dann sollte dort auch ein Wert stehen.');
    }
    // check ob filterregel bereits woanders einen treffer gelandet hat
    if (($record->strval('status') == '1')) {
      if     (($seaEna == '1') AND ($verEna == '0') AND ($seaCmd == '0')) $where = "(software LIKE  '$seaStr')";
      elseif (($seaEna == '1') AND ($verEna == '1') AND ($seaCmd == '0')) $where = "(software LIKE  '$seaStr') AND (version = '$vers')";
      elseif (($seaEna == '1') AND ($verEna == '0') AND ($seaCmd == '1')) $where = "(software RLIKE '$seaStr')";
      elseif (($seaEna == '1') AND ($verEna == '1') AND ($seaCmd == '1')) $where = "(software RLIKE '$seaStr') AND (version = '$vers')";
      elseif (($seaEna == '0') AND ($verEna == '0')) $where = "(software = '$softw')";
      elseif (($seaEna == '0') AND ($verEna == '1')) $where = "(software = '$softw') AND (version = '$vers')";
      else   return Dataface_Error::permissionDenied('Ungueltige Parameterabfrage searchEn='.$seaEna.' versionEn='.$verEna.' searchCmd='.$seaCmd);

      $sql = "SELECT DISTINCT software, version FROM mpi_inventar WHERE $where";
      $count = xf_db_num_rows(xf_db_query($sql, df_db()));
      //return Dataface_Error::permissionDenied($count.'-'.$sql);
      if ( $count >= 1 ) {
        $query = xf_db_query($sql, df_db());
        while ($row = xf_db_fetch_assoc($query)) {
          $qSoft = $row['software'];
          $qVers = $row['version'];
          $sql = "SELECT conID FROM view_invConSoft WHERE software = '$qSoft' AND version = '$qVers'";
          list($conID) = xf_db_fetch_row(xf_db_query($sql, df_db()));
          if ( isset($conID) AND ($tabID != $conID) ) return Dataface_Error::permissionDenied('Doppelte Filtersuche: Das Ergebnis dieser Regel ('.$qSoft.' : '.$qVers.') existiert bereits (conID='.$conID.')');
        } 
      } else {
        return Dataface_Error::permissionDenied('Kein Treffer im Inventory gefunden. Diese Suchregel macht deshalb keinen Sinn.');
      }
    }

  }

}
?>
