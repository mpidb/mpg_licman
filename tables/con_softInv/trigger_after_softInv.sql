-- schreibe direkt softID, verID, lizenzID, keyID in die DS vom Inventar entsprechend autom. Suchfilter
-- dies ginge auch per View, aber der dauert wegen der vielen Inv-DS einfach zu lange, diese beschleunigt es erhebelich
--
-- Trigger AFTER insert `con_softInv`
--

DROP TRIGGER IF EXISTS `set_inv_ins`;
DELIMITER $$
CREATE TRIGGER `set_inv_ins` AFTER INSERT ON `con_softInv` FOR EACH ROW
 BEGIN 
  UPDATE mpi_inventar AS inv
   INNER JOIN con_softInv AS con ON
    NEW.status = '1' AND (
    ((NEW.searchEn = '1') AND (NEW.versionEn = '0') AND (NEW.searchCmd = '0') AND (inv.software LIKE NEW.searchStr)) OR
    ((NEW.searchEn = '1') AND (NEW.versionEn = '1') AND (NEW.searchCmd = '0') AND (inv.software LIKE NEW.searchStr) AND (inv.version = NEW.version)) OR
    ((NEW.searchEn = '1') AND (NEW.versionEn = '0') AND (NEW.searchCmd = '1') AND (inv.software RLIKE IF(NEW.searchStr = '',NULL,NEW.searchStr))) OR
    ((NEW.searchEn = '1') AND (NEW.versionEn = '1') AND (NEW.searchCmd = '1') AND (inv.software RLIKE IF(NEW.searchStr = '',NULL,NEW.searchStr)) AND (inv.version = NEW.version)) OR
    ((NEW.searchEn = '0') AND (NEW.versionEn = '0') AND (inv.software = NEW.software)) OR
    ((NEW.searchEn = '0') AND (NEW.versionEn = '1') AND (inv.software = NEW.software) AND (inv.version = NEW.version))
    ) 
   SET inv.conID = NEW.conID, inv.softID = NEW.softID, inv.verID = NEW.verID, inv.lizenzID = NEW.LizenzID, inv.keyID = NEW.keyID;
  IF NEW.verID IS NOT NULL AND NEW.lizenzID IS NOT NULL THEN CALL proc_version(NEW.verID); END IF;
 END;
$$
DELIMITER ;


--
-- Trigger AFTER update `con_softInv`
--

DROP TRIGGER IF EXISTS `set_inv_upd`;
DELIMITER $$
CREATE TRIGGER `set_inv_upd` AFTER UPDATE ON `con_softInv` FOR EACH ROW
 BEGIN 
  UPDATE mpi_inventar AS inv INNER JOIN con_softInv AS con ON
    OLD.status = '1' AND (
    ((OLD.searchEn = '1') AND (OLD.versionEn = '0') AND (OLD.searchCmd = '0') AND (inv.software LIKE OLD.searchStr)) OR
    ((OLD.searchEn = '1') AND (OLD.versionEn = '1') AND (OLD.searchCmd = '0') AND (inv.software LIKE OLD.searchStr) AND (inv.version = OLD.version)) OR
    ((OLD.searchEn = '1') AND (OLD.versionEn = '0') AND (OLD.searchCmd = '1') AND (inv.software RLIKE IF(OLD.searchStr = '',NULL,OLD.searchStr))) OR
    ((OLD.searchEn = '1') AND (OLD.versionEn = '1') AND (OLD.searchCmd = '1') AND (inv.software RLIKE IF(OLD.searchStr = '',NULL,OLD.searchStr)) AND (inv.version = OLD.version)) OR
    ((OLD.searchEn = '0') AND (OLD.versionEn = '0') AND (inv.software = OLD.software)) OR
    ((OLD.searchEn = '0') AND (OLD.versionEn = '1') AND (inv.software = OLD.software) AND (inv.version = OLD.version))
    )
   SET inv.conID = NULL, inv.softID = NULL, inv.verID = NULL, inv.lizenzID = NULL, inv.keyID = NULL;
  IF OLD.verID IS NOT NULL AND OLD.lizenzID IS NOT NULL THEN CALL proc_version(OLD.verID); END IF;
  UPDATE mpi_inventar AS inv INNER JOIN con_softInv AS con ON
    NEW.status = '1' AND (
    ((NEW.searchEn = '1') AND (NEW.versionEn = '0') AND (NEW.searchCmd = '0') AND (inv.software LIKE NEW.searchStr)) OR
    ((NEW.searchEn = '1') AND (NEW.versionEn = '1') AND (NEW.searchCmd = '0') AND (inv.software LIKE NEW.searchStr) AND (inv.version = NEW.version)) OR
    ((NEW.searchEn = '1') AND (NEW.versionEn = '0') AND (NEW.searchCmd = '1') AND (inv.software RLIKE IF(NEW.searchStr = '',NULL,NEW.searchStr))) OR
    ((NEW.searchEn = '1') AND (NEW.versionEn = '1') AND (NEW.searchCmd = '1') AND (inv.software RLIKE IF(NEW.searchStr = '',NULL,NEW.searchStr)) AND (inv.version = NEW.version)) OR
    ((NEW.searchEn = '0') AND (NEW.versionEn = '0') AND (inv.software = NEW.software)) OR
    ((NEW.searchEn = '0') AND (NEW.versionEn = '1') AND (inv.software = NEW.software) AND (inv.version = NEW.version))
    )
   SET inv.conID = NEW.conID, inv.softID = NEW.softID, inv.verID = NEW.verID, inv.lizenzID = NEW.LizenzID, inv.keyID = NEW.keyID;
  IF NEW.verID IS NOT NULL AND NEW.lizenzID IS NOT NULL THEN CALL proc_version(NEW.verID); END IF;
 END;
$$
DELIMITER ;

--
-- Trigger AFTER delete `con_softInv`
--

DROP TRIGGER IF EXISTS `set_inv_del`;
DELIMITER $$
CREATE TRIGGER `set_inv_del` AFTER DELETE ON `con_softInv` FOR EACH ROW
 BEGIN
  UPDATE mpi_inventar AS inv
   INNER JOIN con_softInv AS con ON
    OLD.status = '1' AND (
    ((OLD.searchEn = '1') AND (OLD.versionEn = '0') AND (OLD.searchCmd = '0') AND (inv.software LIKE OLD.searchStr)) OR
    ((OLD.searchEn = '1') AND (OLD.versionEn = '1') AND (OLD.searchCmd = '0') AND (inv.software LIKE OLD.searchStr) AND (inv.version = OLD.version)) OR
    ((OLD.searchEn = '1') AND (OLD.versionEn = '0') AND (OLD.searchCmd = '1') AND (inv.software RLIKE IF(OLD.searchStr = '',NULL,OLD.searchStr))) OR
    ((OLD.searchEn = '1') AND (OLD.versionEn = '1') AND (OLD.searchCmd = '1') AND (inv.software RLIKE IF(OLD.searchStr = '',NULL,OLD.searchStr)) AND (inv.version = OLD.version)) OR
    ((OLD.searchEn = '0') AND (OLD.versionEn = '0') AND (inv.software = OLD.software)) OR
    ((OLD.searchEn = '0') AND (OLD.versionEn = '1') AND (inv.software = OLD.software) AND (inv.version = OLD.version))
    )
   SET inv.conID = NULL, inv.softID = NULL, inv.verID = NULL, inv.lizenzID = NULL, inv.keyID = NULL;
  IF OLD.verID IS NOT NULL AND OLD.lizenzID IS NOT NULL THEN CALL proc_version(OLD.verID); END IF;
 END;
$$
DELIMITER ;

