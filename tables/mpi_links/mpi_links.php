<?php

class tables_mpi_links {

  function softID__renderCell( &$record ) {
    $table  = 'mpi_software';
    $action = 'browse';
    $field  = 'softID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql    = "SELECT software FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function srvID__renderCell( &$record ) {
    $table  = 'mpi_licSrv';
    $action = 'browse';
    $field  = 'srvID';
    $tabID  = $record->val($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql = "SELECT CONCAT(server, IF(alias IS NOT NULL, CONCAT(' (',alias,')'),'')) FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function link__renderCell( &$record ) {
    $web = $record->strval('link');
    if (($web != NULL) AND (strpos($web,'@') > 2 )) {
      return '<a href="mailto:'.$web.'">'.$web.'</a>';
    }
    elseif (($web != NULL) AND (strpos($web,'://') >= 3 )) {
      return '<a href="'.$web.'">'.$web.'</a>';
    }
    return '<a href="http://'.$web.'">'.$web.'</a>';
  }

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) {
      return "";
    } else {
      return date('d.m.Y', strtotime($record->strval('zeitstempel')));
    }
  }

  function beforeSave(&$record) {
    // User automatisch bei neu und aendern setzen
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

}
?>
