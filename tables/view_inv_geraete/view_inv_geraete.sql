-- hole von db inv alles passend zur bestellnummer
-- version standalone
CREATE OR REPLACE VIEW view_inv_geraete AS
 SELECT
  '000001' AS gerID,
  'fakeAlone' AS name,
  'none' AS os,
  'R0.0' AS lagerort,
  'none' AS status,
  '0123456789' AS bestellnummer,
  'alone' AS bearbeiter,
  '2016-07-22 12:38:40' AS zeitstempel
;

-- version mpi-dcts mit it_inv
CREATE OR REPLACE VIEW view_inv_geraete AS
 SELECT
  tabID AS gerID,
  name,
  os,
  lagerort,
  status,
  bestellnummer,
  bearbeiter,
  zeitstempel
 FROM
  mpidb_it_inv.mpi_geraete WHERE bestellnummer IS NOT NULL AND bestellnummer != ''
;

-- version mpg mit mpg_inv
CREATE OR REPLACE VIEW view_inv_geraete AS
 SELECT
  tabID AS gerID,
  name,
  os,
  lagerort,
  status,
  bestellnummer,
  bearbeiter,
  zeitstempel
 FROM
  mpidb_mpg_inv.mpi_geraete WHERE bestellnummer IS NOT NULL AND bestellnummer != ''
;

