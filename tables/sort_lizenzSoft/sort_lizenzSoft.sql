-- sortierfilter software in mpi_lizenz und verID fuer depselect
CREATE OR REPLACE VIEW sort_lizenzSoft AS 
SELECT
 lic.lizenzID,
 lic.softID,
 IFNULL( ver.verID,  lic.verID ) AS verID,
 lic.bezeichnung
FROM
 mpi_lizenz AS lic
 LEFT JOIN con_verLic AS con ON con.lizenzID = lic.lizenzID
 LEFT JOIN mpi_version  AS ver  ON ver.verID = con.verID
ORDER BY
 bezeichnung
;


-- sortierfilter software in lizenz
CREATE OR REPLACE VIEW sort_lizenzSoft AS 
SELECT
 lic.*,
 soft.software
FROM 
 mpi_lizenz AS lic
 LEFT JOIN con_verLic   AS con  ON con.lizenzID = lic.lizenzID
 LEFT JOIN mpi_version  AS ver  ON ver.verID = con.verID
 LEFT JOIN mpi_software AS soft ON soft.softID = ver.softID
WHERE
 soft.software IS NOT NULL OR soft.software <> ''
GROUP BY 
 lic.lizenzID
UNION ALL
SELECT
 lic.*,
 soft.software
FROM
 mpi_lizenz AS lic
 LEFT JOIN mpi_software AS soft ON soft.softID = lic.softID
WHERE
 soft.software IS NOT NULL OR soft.software <> ''
GROUP BY 
 lic.lizenzID
