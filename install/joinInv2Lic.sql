-- Aenderungen fuer Weitergabe einer global gueltigen DB mit Kopplung mpg_licman <-> mpg_inv
-- seperat auszufuehrende sql-datei
-- alle views zwischen den DB's sollten vorher als fake existieren, damit auf dem Filesytem nichts mehr geaendert werden muss
-- view_pc (mpg_licman), view_it_licman_instInv (mpg_inv)

USE mpidb_mpg_licman;
-- version mpg mit verlinkung ueber pc-name zu lizenzen
CREATE OR REPLACE VIEW mpidb_mpg_inv.view_it_licman_instInv AS
 SELECT
  vins.tabID,
  vins.instID,
  vins.instName,
  vins.softID,
  vins.verID,
  vins.lizenzID,
  vins.inventar,
  vins.useLizenz,
  vins.anzTreffer,
  vins.bearbeiter,
  soft.software,
  ver.version,
  lic.bezeichnung AS lizenz,
  sch.`key`
 FROM
  mpidb_mpg_licman.view_invInst AS vins
  LEFT JOIN mpidb_mpg_licman.mpi_software AS soft ON vins.softID = soft.softID
  LEFT JOIN mpidb_mpg_licman.mpi_version AS ver ON vins.verID = ver.verID
  LEFT JOIN mpidb_mpg_licman.mpi_lizenz AS lic ON vins.lizenzID = lic.lizenzID AND lic.status = 1
  LEFT JOIN mpidb_mpg_licman.mpi_key AS sch ON vins.keyID = sch.keyID
 ORDER BY software, instName
;


-- version mpg mit verlinkung ueber bestellnummer zu lizenzen
CREATE OR REPLACE VIEW mpidb_mpg_inv.view_licman_lizenz AS
 SELECT
  lizenzID,
  bezeichnung,
  bestellnummer,
  bearbeiter,
  zeitstempel
 FROM
  mpidb_mpg_licman.mpi_lizenz WHERE bestellnummer IS NOT NULL AND bestellnummer != '' AND status = 1
 ORDER BY
  bezeichnung
;

