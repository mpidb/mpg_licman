-- run: mysql -u root -p mpidb_mpg_licman < updateDB_1001-1022.sql
-- UPDATES immer von der niefrigsten bis zur hoechsten version ausfuehren
-- damit IF benutzt werden kann, wird eine prozedur erzeugt und am ende ausgefuehrt
-- 
-- views, funcs, procs nach moeglichkeit nur einmal in der max version ausfuehren

-- USE mpidb_mpg_licman;
DROP PROCEDURE IF EXISTS proc_update;
DELIMITER $$
CREATE PROCEDURE proc_update()
proc_label: BEGIN

-- initial value
IF ( SELECT MAX(version) FROM dataface__version ) = '0' THEN
 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('1001');
END IF;

-- mindest version vorhanden
IF ( SELECT MAX(version) FROM dataface__version ) < '1001' THEN
 LEAVE proc_label;
END IF;

-- CHANGES V1.0.01 :
-- *****************
-- fs::rsync - mpg_licman
-- db::con_softInv     - add field anzahl, moeglichkeit bei concurrent anz = 0, also disable
-- ALTER TABLE con_softInv ADD anzahl SMALLINT(6) UNSIGNED NOT NULL DEFAULT '1' AFTER status;
-- db::view_invInst    - run view_invInst/view_invInst.sql


-- CHANGES V1.0.20 :
-- *****************
-- NEW: tabelle lizenzserver verlinkt nach lizenz
-- fs::rsync - mpg_licman
-- db::mpi_licSrv      - add table mpi_licSrv
IF ( SELECT MAX(version) FROM dataface__version ) < '1020' THEN
 CREATE TABLE IF NOT EXISTS mpi_licSrv (
  `srvID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `server` varchar(25) NOT NULL,
  `alias` varchar(25) DEFAULT NULL,
  `zugriff` VARCHAR(50) DEFAULT NULL,
  `hardware` VARCHAR(200) DEFAULT NULL,
  `program` VARCHAR(200) DEFAULT NULL,
  `licFile` VARCHAR(200) DEFAULT NULL,
  `runCmd` VARCHAR(200) DEFAULT NULL,
  `status` VARCHAR(200) DEFAULT NULL,
  `client` VARCHAR(200) DEFAULT NULL,
  `port` VARCHAR(50) DEFAULT NULL,
  `beschreibung` text DEFAULT NULL,
  `bearbeiter` varchar(20) DEFAULT NULL,
  `zeitstempel` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`srvID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
 INSERT IGNORE INTO mpi_licSrv (server,beschreibung) SELECT server,lizenzID FROM mpi_lizenz WHERE server IS NOT NULL;
 UPDATE mpi_lizenz AS lic SET server = (SELECT srvID FROM mpi_licSrv AS srv WHERE lic.lizenzID = srv.beschreibung);
 ALTER TABLE mpi_lizenz CHANGE server srvID SMALLINT(6) UNSIGNED ZEROFILL NULL DEFAULT NULL;
 ALTER TABLE mpi_lizenz ADD INDEX (srvID);
 ALTER TABLE mpi_lizenz ADD FOREIGN KEY (srvID) REFERENCES mpi_licSrv (srvID) ON DELETE SET NULL ON UPDATE RESTRICT;
-- db::mpi_links      - aufpoliert
 ALTER TABLE mpi_links CHANGE autoID linksID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
 ALTER TABLE mpi_links CHANGE softID softID SMALLINT(6) UNSIGNED ZEROFILL NULL;
 ALTER TABLE mpi_links ADD srvID SMALLINT(6) UNSIGNED ZEROFILL NULL AFTER softID , ADD INDEX (srvID);
 ALTER TABLE mpi_links DROP FOREIGN KEY mpi_links_ibfk_3 , ADD FOREIGN KEY (softID) REFERENCES mpi_software (softID) ON DELETE SET NULL ON UPDATE RESTRICT;
 ALTER TABLE mpi_links ADD FOREIGN KEY (srvID) REFERENCES mpi_licSrv (srvID) ON DELETE SET NULL ON UPDATE RESTRICT;
 ALTER TABLE mpi_ablage ADD srvID SMALLINT(6) UNSIGNED ZEROFILL NULL AFTER lizenzID , ADD INDEX (srvID);
-- db::mpi_ablage      - error double save xataface - erstmal wieder raus
 DROP TRIGGER IF EXISTS set_abl_del;
-- fs::cronVersion.php - Erweiterung auf loeschen ablage wegen trigger

 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('1020');
END IF;


-- CHANGES V1.0.21 :
-- *****************
-- fs::rsync - mpg_licman
-- fs::rsync - master   - run view_favorit - sort favorit
-- db::view_software    - run view_software.sql - add field Lizenz zzugeordnet
IF ( SELECT MAX(version) FROM dataface__version ) < '1021' THEN
 ALTER TABLE mpi_ablage ADD FOREIGN KEY (srvID) REFERENCES mpi_licSrv (srvID) ON DELETE SET NULL ON UPDATE RESTRICT;

-- db::view_software    - runonce view_software.sql
 CREATE OR REPLACE VIEW view_software AS
  SELECT softID, verID, lizenzID, modell, anzLizenz, NULL AS useLizenz, useLizenz AS verbLizenz, freeLizenz
  FROM view_lizenz
  UNION ALL
  SELECT softID, verID, lizenzID, modell, anzLizenz, useLizenz, verbVersion AS verbLizenz, freeVersion AS freeLizenz
  FROM view_version;

-- db::view_favorit    - runonce view_favorit.sql
 CREATE OR REPLACE VIEW view_favorit AS SELECT * FROM list_reiter WHERE favorit = '1' ORDER BY reiter;

 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('1021');
END IF;


-- CHANGES V1.0.22 :
-- *****************
-- NEW: Vergleich Filesysten mit Datenbank-Version
-- fs::rsync - mpg_licman
IF ( SELECT MAX(version) FROM dataface__version ) < '1022' THEN

 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('1022');
END IF;


-- CHANGES V1.0.23 :
-- *****************
-- fs::mpi_lizenz     - fields.ini - vocabulary vertrag
-- fs::mpi_licSrv     - fields.ini - lnk und sql
IF ( SELECT MAX(version) FROM dataface__version ) < '1023' THEN

 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('1023');
END IF;

-- CHANGES V1.0.24 :
-- *****************
-- fs::mpi_vertrag    - fields.ini - fehler anzeige in __sql__, wenn keine Verknuepfung da
-- fs::view_vertrag   - add view fuer fehler in anzeige
IF ( SELECT MAX(version) FROM dataface__version ) < '1024' THEN
 CREATE OR REPLACE VIEW view_vertrag AS SELECT vertragID, count(vertragID) AS anzVertrag FROM mpi_lizenz WHERE vertragID IS NOT NULL GROUP BY vertragID;

 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('1024');
END IF;

-- CHANGES V1.0.25 :
-- *****************
-- fs::diverse php    - remove gethostname(), wegen fqdn
-- fs::conf.ini       - add paramter ssl, wegen https zugriff
-- fs::cronDatabase.php  - url anpassung
-- fs::*.php          - leerzeichen hinter ?> entfernt, wegen mac-os
-- fs::actions.ini    - permission suche ohne login verhindert 

IF ( SELECT MAX(version) FROM dataface__version ) < '1025' THEN

  -- keine db aenderung, nur fs
  -- mpi_software - anzeige falscher version wenn < 1000

 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('1025');
END IF;





END;
$$
DELIMITER ;

-- execute updates
CALL proc_update();
