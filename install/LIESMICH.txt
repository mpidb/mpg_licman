###
Was muss ich anpassen:

!!! Vorhandene DB oder FileSystem immer vorher sichern !!!

1. DB-User anlegen mit sql Skript install/create_DB_User.sql
    Initialer Login ist admin mit Passwort admin
    sollte man irgendwann aendern

2. Datenbank anlegen mit sql Skript install/mpidb_mpg_licman.sql
   bei Update install/updateDB_<version>.sql

3. domain, hostname, ssl, user, password unbedingt anpassen in
    conf.ini, getInvEmpirum.php bzw. getInvOPSI.php anpassen
    user, pass in mysql-DB anpassen

4. ggf. cronjobs anlegen
	5 5 * * * php /var/www/mpg_licman/cronjobs/getInvEmpirum.php
	5 6 * * * php /var/www/mpg_licman/cronjobs/cronDatabase.php

5. wenn es eine seperate User-Datenbank gibt, dann folgende SQL-Skripte ausfuehren
    mysql -p -u root < install/joinLic2User.sql

6. wenn es eine Inventar-Datenbank gibt, dann den Eintrag in conf.ini anpassen
    ini parameter [_own][inv_path] auf externen DB-Inv-Pfad setzen 

7. wenn es eine Inventar-Datenbank gibt, dann diese SQL-Skripte ausfuehren
    mysql -p -u root < install/joinInv2Lic.sql
    mysql -p -u root < install/joinLic2Inv.sql

8. Autorisierung auf ldap umstellen
    Eintrag  #auth_type = basic     disable per Raute
    Eintrag   auth_type = ldap      enable Raute entfernen


Wie starte ich sql-Skripte?
 - per import in phpmyadmin oder
 - INITIAL: per Konsole mit 'mysql -p -u root < irgendEinName.sql
 - UPDATE:  per Konsole mit 'mysql -p -u root <datenbankname> < irgendEinName.sql





