-- run: mysql -p mpidb_mpg_licman < updateDB_0903.sql
-- UPDATES immer von oben nach unten innerhalb der version ausfuehren
-- views, funcs, procs einmal direkt am ende ausfuehren, ansonsten per hand wenn einzelne versionen updated werden


-- CHANGES V0.9.03 :
-- *****************
-- UPDATE: diverse Aenderungen in den Lizenzauswertungen
-- fs::rsync - it_licman
-- db::mpi_version      - add field lizenz ordnung
 ALTER TABLE mpi_version ADD orderID TINYINT UNSIGNED ZEROFILL NOT NULL DEFAULT '1' AFTER version;
 ALTER TABLE mpi_version ADD UNIQUE softID_orderID (softID, orderID);
 CREATE TABLE IF NOT EXISTS list_ordnung ( orderID tinyint(3) unsigned zerofill NOT NULL, ordnung varchar(10) NOT NULL, UNIQUE KEY orderID (orderID), UNIQUE KEY ordnung (ordnung)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
 INSERT INTO list_ordnung (orderID, ordnung) VALUES ('01', 'L'), ('10', '1'), ('20', '2'), ('30', '3'), ('40', '4'), ('50', '5'), ('60', '6'), ('70', '7'), ('80', '8'), ('90', '9'), ('99', 'H');


-- CHANGES V0.9.04 :
-- *****************
-- UPDATE: diverse Aenderungen in den Lizenzauswertungen
-- UPDATE: clean history tables
-- fs::rsync - it_licman
-- fs::rsync - master
-- db::list_reiter      - add field history - soll history geloescht werden
 ALTER TABLE list_reiter ADD history TINYINT(1) NOT NULL DEFAULT '0' AFTER favorit;
 DROP TABLE IF EXISTS list_reiter__history;
 CREATE OR REPLACE VIEW view_favorit AS SELECT * FROM list_reiter WHERE favorit = '1';
-- db::show_version     - create table for array datas and fill with versions id's
 CREATE TABLE IF NOT EXISTS show_version (
  verID smallint(6) unsigned zerofill NOT NULL, diffVersion smallint(6) NOT NULL DEFAULT '0', restVersion smallint(6) NOT NULL DEFAULT '0', verbVersion SMALLINT(6) NOT NULL DEFAULT '0', freeVersion smallint(6) NOT NULL DEFAULT '0', PRIMARY KEY (verID));
 INSERT IGNORE INTO show_version (verID) SELECT DISTINCT verID FROM con_verLic WHERE 1;
-- db::mpi_version      - index erstellen
 ALTER TABLE mpi_version ADD INDEX (orderID);
 ALTER TABLE mpi_version ADD FOREIGN KEY (orderID) REFERENCES list_ordnung (orderID) ON DELETE RESTRICT ON UPDATE RESTRICT;
 DROP TABLE IF EXISTS mpi_version__history;
-- db::view_verLic      - run view_verLic.sql
-- db::view_verSumLic   - run view_verSumLic.sql
-- db::view_verVerb     - run view_verVerb.sql
-- db::view_version     - run view_version.sql
-- db::view_lizenz      - run view_lizenz.sql


-- CHANGES V0.9.05 :
-- *****************
-- fs::rsync - it_licman
-- fs::cronjob         - add cronVersion.php zur Lizenzberechnung as cronjob
 DROP FUNCTION IF EXISTS func_version;
-- db::view_verLic      - run view_verLic.sql
-- db::view_version     - run view_version.sql
-- db::view_software    - run view_software.sql
-- db::proc_version     - run  mysql -p mpidb_it_licman < show_version/proc_version.sql
-- db::trigger          - diverse Trigger neu laden wegen u.a. DELIMITER
 DROP TRIGGER IF EXISTS set_instlic_ins;
 DROP TRIGGER IF EXISTS set_instlic_upd;
--    mysql -p mpidb_it_licman < con_softInv/trigger_after_softInv.sql
--    mysql -p mpidb_it_licman < con_verLic/trigger_after_verLic.sql
--    mysql -p mpidb_it_licman < mpi_ablage/trigger_after_ablage.sql
--    mysql -p mpidb_it_licman < mpi_install/trigger_after_install.sql
--    mysql -p mpidb_it_licman < mpi_lizenz/trigger_after_lizenz.sql


-- CHANGES V1.0.01 :
-- *****************
-- fs::rsync - it_licman
-- db::con_softInv     - add field anzahl, moeglichkeit bei concurrent anz = 0, also disable
 ALTER TABLE con_softInv ADD anzahl SMALLINT(6) UNSIGNED NOT NULL DEFAULT '1' AFTER status;
-- db::view_invInst    - run view_invInst/view_invInst.sql


-- db::view_verLic      - runonce view_verLic.sql
CREATE OR REPLACE VIEW view_verLic AS
SELECT
 ver.verID,
 lic.lizenzID,
 IF ( count(lic.modell) > 1, 'Mixed', lic.modell ) AS modell,
 SUM( lic.anzahl ) AS anzLizenz
FROM
 mpi_version AS ver
 LEFT JOIN mpi_lizenz AS lic ON lic.verID = ver.verID
WHERE
 lic.verID IS NOT NULL AND lic.status = 1
GROUP BY
 ver.verID
;

-- db::view_verSumLic   - runonce view_verSumLic.sql
CREATE OR REPLACE VIEW view_verSumLic AS
SELECT
 ver.verID,
 -- ver.softID,
 SUM( lic.anzahl ) AS anzLizenz
FROM
 mpi_version AS ver
 LEFT JOIN con_verLic  AS con ON con.verID = ver.verID
 LEFT JOIN mpi_lizenz AS lic ON lic.lizenzID = con.lizenzID
WHERE
 lic.status = '1'
GROUP BY
 ver.verID
;

-- db::view_verVerb     - runonce view_verVerb.sql
CREATE OR REPLACE VIEW view_verVerb AS
SELECT
 ver.verID,
 -- ver.softID,
 SUM(vins.uselizenz) AS useLizenz
FROM
 mpi_version AS ver
 INNER JOIN view_instLic AS vins ON ver.verID = vins.verID
GROUP BY
 ver.verID
;

-- db::view_version     - runonce view_version.sql
CREATE OR REPLACE VIEW view_version AS
SELECT
 ver.softID,
 ver.verID,
 ver.orderID,
 vlic.lizenzID,
 vlic.modell,
 vlic.anzLizenz,
 vuse.useLizenz,
 shw.diffVersion,
 shw.restVersion,
 shw.verbVersion,
 shw.freeVersion
FROM
 mpi_version AS ver
 LEFT JOIN view_verVerb vuse ON vuse.verID = ver.verID
 LEFT JOIN view_verLic  vlic ON vlic.verID = ver.verID
 LEFT JOIN show_version  shw ON shw.verID  = ver.verID
WHERE
 ver.verID IN (SELECT verID FROM con_verLic)
ORDER BY
 softID, orderID, version
;

-- db::view_lizenz      - runonce view_lizenz.sql
CREATE OR REPLACE VIEW view_lizenz AS
SELECT
 tabID,
 softID,
 verID,
 lizenzID,
 inventar,
 anzLizenz,
 -- count(lizenzID) AS anzInstall,
 ( SUM(useLizenz) ) AS useLizenz,
 IF (anzLizenz = 0, 0, (anzLizenz - ( SUM(useLizenz) ))) AS freeLizenz,
 modell
FROM
 view_instLic
WHERE
 lizenzID NOT IN (SELECT lizenzID FROM con_verLic)
GROUP BY
 lizenzID
;


-- db::view_invInst    - runonce view_invInst/view_invInst.sql
CREATE OR REPLACE VIEW view_invInst AS
SELECT
 aut.conID AS tabID,
 vpc.tabID AS instID,
 aut.instName,
 aut.softID,
 aut.verID,
 aut.lizenzID,
 aut.keyID,
 1 AS inventar,
 con.anzahl AS useLizenz,
 count(aut.instName) AS anzTreffer,
 aut.bearbeiter
FROM
 mpi_inventar AS aut
 LEFT JOIN view_pc AS vpc ON aut.instName = vpc.name
 LEFT JOIN con_softInv AS con ON con.conID = aut.conID
WHERE
 aut.conID IS NOT NULL
GROUP BY
 aut.conID, aut.instName, aut.softID, aut.verID, aut.lizenzID
UNION ALL
SELECT
 man.installID AS tabID,
 man.instID,
 vpc.name AS instName,
 man.softID,
 man.verID,
 man.lizenzID,
 man.keyID,
 0 as inventar,
 man.anzahl AS useLizenz,
 count(vpc.name) AS anzTreffer,
 bearbeiter
FROM
 mpi_install AS man
 LEFT JOIN view_pc AS vpc ON man.instID = vpc.tabID
WHERE
 man.status = '1'
GROUP BY
 man.installID, vpc.name, man.softID, man.verID, man.lizenzID
;


-- db::view_software    - runonce view_software.sql
CREATE OR REPLACE VIEW view_software AS
SELECT
 softID,
 verID,
 lizenzID,
 modell,
 anzLizenz,
 useLizenz,
 freeLizenz
FROM
 view_lizenz
UNION ALL
SELECT
 softID,
 verID,
 lizenzID,
 modell,
 anzLizenz,
 verbVersion AS useLizenz,
 freeVersion AS freeLizenz
FROM
 view_version
;


-- db::proc_version     - runonce show_version/proc_version.sql
DROP PROCEDURE IF EXISTS proc_version;
DELIMITER $$
CREATE PROCEDURE proc_version ( IN iVerID SMALLINT(6) )
proc_label:BEGIN
  DECLARE Vt, Ot, Rt, Lt, It, St, Pt, Dt, Nt, Mt, Tt, Pg SMALLINT(6) DEFAULT '0';
  DECLARE endRow TINYINT DEFAULT '0';
  DECLARE forSoft1 CURSOR FOR
    SELECT verID, orderID, IFNULL(anzLizenz,'0'), IFNULL(useLizenz,'0') FROM view_version WHERE softID = St ORDER BY orderID ASC;
  DECLARE forSoft2 CURSOR FOR
    SELECT ver.verID, diffVersion, IFNULL(shw.restVersion,'0') FROM show_version AS shw LEFT JOIN mpi_version AS ver ON ver.verID = shw.verID WHERE ver.softID = St ORDER BY ver.orderID DESC;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET endRow = 1;
  IF iVerID IS NULL THEN LEAVE proc_label; END IF;  -- raus wenn NULL ist
  SELECT softID INTO St FROM mpi_version WHERE verID = iVerID;
  IF St IS NULL THEN LEAVE proc_label; END IF;  -- raus wenn NULL ist
  SELECT MAX(orderID) INTO Mt FROM mpi_version WHERE softID = St;

  OPEN forSoft1;
  getSoft: LOOP
  FETCH forSoft1 INTO Vt, Ot, Lt, It;
  IF endRow = 1 THEN LEAVE getSoft; END IF;
  SET Dt = (Lt - It + Nt);
  IF Dt < '0' AND Mt != Ot THEN  -- bei hoechster Version Wert nicht aendern
    SET Pt = '0';
    SET Nt = Dt;
  ELSE
    SET Pt = Dt;
    SET Nt = '0';
  END IF;
  IF Dt < '0' THEN SET Rt = Lt; ELSE SET Rt = Lt - Dt; END IF;
  UPDATE show_version SET diffVersion = Dt, verbVersion = Rt, restVersion = Pt WHERE verID = Vt;
  END LOOP getSoft;
  CLOSE forSoft1;

  SET endRow = '0';
  OPEN forSoft2;
  getSoft: LOOP
  FETCH forSoft2 INTO Vt, Dt, Pt;
  IF endRow = 1 THEN LEAVE getSoft; END IF;
  IF Pg < 0 AND Dt < 0 THEN
    SET Tt = Pg;
  ELSE
    SET Pg = (Pg + Pt);
    SET Tt = Pg;
  END IF;
  UPDATE show_version SET freeVersion = Tt WHERE verID = Vt;
  -- IF Pt < '0' THEN Set Pg = '0'; END IF;   -- negative Werte nicht addieren
  END LOOP getSoft;
  CLOSE forSoft2;
END;
$$
DELIMITER ;


-- db::trigger     - runonce con_softInv/trigger_after_softInv.sql
--
-- Trigger AFTER insert `con_softInv`
--

DROP TRIGGER IF EXISTS `set_inv_ins`;
DELIMITER $$
CREATE TRIGGER `set_inv_ins` AFTER INSERT ON `con_softInv` FOR EACH ROW
 BEGIN
  UPDATE mpi_inventar AS inv
   INNER JOIN con_softInv AS con ON
    NEW.status = '1' AND (
    ((NEW.searchEn = '1') AND (NEW.versionEn = '0') AND (NEW.searchCmd = '0') AND (inv.software LIKE NEW.searchStr)) OR
    ((NEW.searchEn = '1') AND (NEW.versionEn = '1') AND (NEW.searchCmd = '0') AND (inv.software LIKE NEW.searchStr) AND (inv.version = NEW.version)) OR
    ((NEW.searchEn = '1') AND (NEW.versionEn = '0') AND (NEW.searchCmd = '1') AND (inv.software RLIKE IF(NEW.searchStr = '',NULL,NEW.searchStr))) OR
    ((NEW.searchEn = '1') AND (NEW.versionEn = '1') AND (NEW.searchCmd = '1') AND (inv.software RLIKE IF(NEW.searchStr = '',NULL,NEW.searchStr)) AND (inv.version = NEW.version)) OR
    ((NEW.searchEn = '0') AND (NEW.versionEn = '0') AND (inv.software = NEW.software)) OR
    ((NEW.searchEn = '0') AND (NEW.versionEn = '1') AND (inv.software = NEW.software) AND (inv.version = NEW.version))
    )
   SET inv.conID = NEW.conID, inv.softID = NEW.softID, inv.verID = NEW.verID, inv.lizenzID = NEW.LizenzID, inv.keyID = NEW.keyID;
  IF NEW.verID IS NOT NULL AND NEW.lizenzID IS NOT NULL THEN CALL proc_version(NEW.verID); END IF;
 END;
$$
DELIMITER ;

--
-- Trigger AFTER update `con_softInv`
--

DROP TRIGGER IF EXISTS `set_inv_upd`;
DELIMITER $$
CREATE TRIGGER `set_inv_upd` AFTER UPDATE ON `con_softInv` FOR EACH ROW
 BEGIN
  UPDATE mpi_inventar AS inv INNER JOIN con_softInv AS con ON
    OLD.status = '1' AND (
    ((OLD.searchEn = '1') AND (OLD.versionEn = '0') AND (OLD.searchCmd = '0') AND (inv.software LIKE OLD.searchStr)) OR
    ((OLD.searchEn = '1') AND (OLD.versionEn = '1') AND (OLD.searchCmd = '0') AND (inv.software LIKE OLD.searchStr) AND (inv.version = OLD.version)) OR
    ((OLD.searchEn = '1') AND (OLD.versionEn = '0') AND (OLD.searchCmd = '1') AND (inv.software RLIKE IF(OLD.searchStr = '',NULL,OLD.searchStr))) OR
    ((OLD.searchEn = '1') AND (OLD.versionEn = '1') AND (OLD.searchCmd = '1') AND (inv.software RLIKE IF(OLD.searchStr = '',NULL,OLD.searchStr)) AND (inv.version = OLD.version)) OR
    ((OLD.searchEn = '0') AND (OLD.versionEn = '0') AND (inv.software = OLD.software)) OR
    ((OLD.searchEn = '0') AND (OLD.versionEn = '1') AND (inv.software = OLD.software) AND (inv.version = OLD.version))
    )
   SET inv.conID = NULL, inv.softID = NULL, inv.verID = NULL, inv.lizenzID = NULL, inv.keyID = NULL;
  IF OLD.verID IS NOT NULL AND OLD.lizenzID IS NOT NULL THEN CALL proc_version(OLD.verID); END IF;
  UPDATE mpi_inventar AS inv INNER JOIN con_softInv AS con ON
    NEW.status = '1' AND (
    ((NEW.searchEn = '1') AND (NEW.versionEn = '0') AND (NEW.searchCmd = '0') AND (inv.software LIKE NEW.searchStr)) OR
    ((NEW.searchEn = '1') AND (NEW.versionEn = '1') AND (NEW.searchCmd = '0') AND (inv.software LIKE NEW.searchStr) AND (inv.version = NEW.version)) OR
    ((NEW.searchEn = '1') AND (NEW.versionEn = '0') AND (NEW.searchCmd = '1') AND (inv.software RLIKE IF(NEW.searchStr = '',NULL,NEW.searchStr))) OR
    ((NEW.searchEn = '1') AND (NEW.versionEn = '1') AND (NEW.searchCmd = '1') AND (inv.software RLIKE IF(NEW.searchStr = '',NULL,NEW.searchStr)) AND (inv.version = NEW.version)) OR
    ((NEW.searchEn = '0') AND (NEW.versionEn = '0') AND (inv.software = NEW.software)) OR
    ((NEW.searchEn = '0') AND (NEW.versionEn = '1') AND (inv.software = NEW.software) AND (inv.version = NEW.version))
    )
   SET inv.conID = NEW.conID, inv.softID = NEW.softID, inv.verID = NEW.verID, inv.lizenzID = NEW.LizenzID, inv.keyID = NEW.keyID;
  IF NEW.verID IS NOT NULL AND NEW.lizenzID IS NOT NULL THEN CALL proc_version(NEW.verID); END IF;
 END;
$$
DELIMITER ;

--
-- Trigger AFTER delete `con_softInv`
--

DROP TRIGGER IF EXISTS `set_inv_del`;
DELIMITER $$
CREATE TRIGGER `set_inv_del` AFTER DELETE ON `con_softInv` FOR EACH ROW
 BEGIN
  UPDATE mpi_inventar AS inv
   INNER JOIN con_softInv AS con ON
    OLD.status = '1' AND (
    ((OLD.searchEn = '1') AND (OLD.versionEn = '0') AND (OLD.searchCmd = '0') AND (inv.software LIKE OLD.searchStr)) OR
    ((OLD.searchEn = '1') AND (OLD.versionEn = '1') AND (OLD.searchCmd = '0') AND (inv.software LIKE OLD.searchStr) AND (inv.version = OLD.version)) OR
    ((OLD.searchEn = '1') AND (OLD.versionEn = '0') AND (OLD.searchCmd = '1') AND (inv.software RLIKE IF(OLD.searchStr = '',NULL,OLD.searchStr))) OR
    ((OLD.searchEn = '1') AND (OLD.versionEn = '1') AND (OLD.searchCmd = '1') AND (inv.software RLIKE IF(OLD.searchStr = '',NULL,OLD.searchStr)) AND (inv.version = OLD.version)) OR
    ((OLD.searchEn = '0') AND (OLD.versionEn = '0') AND (inv.software = OLD.software)) OR
    ((OLD.searchEn = '0') AND (OLD.versionEn = '1') AND (inv.software = OLD.software) AND (inv.version = OLD.version))
    )
   SET inv.conID = NULL, inv.softID = NULL, inv.verID = NULL, inv.lizenzID = NULL, inv.keyID = NULL;
  IF OLD.verID IS NOT NULL AND OLD.lizenzID IS NOT NULL THEN CALL proc_version(OLD.verID); END IF;
 END;
$$
DELIMITER ;


-- db::trigger     - runonce  con_verLic/trigger_after_verLic.sql
--
-- Trigger AFTER insert `con_verLic`
--

DROP TRIGGER IF EXISTS set_con_ins;
DELIMITER $$
CREATE TRIGGER set_con_ins AFTER INSERT ON con_verLic FOR EACH ROW
 BEGIN
  INSERT IGNORE INTO show_version (verID) VALUES (NEW.verID);
  CALL proc_version(NEW.verID);
 END;
$$
DELIMITER ;


--
-- Trigger AFTER update `con_verLic`
--

-- OLD und NEW nur notwendig, wenn wechsel zur einer anderen software und das geht eigentlich nicht
DROP TRIGGER IF EXISTS set_con_upd;
DELIMITER $$
CREATE TRIGGER set_con_upd AFTER UPDATE ON con_verLic FOR EACH ROW
 BEGIN
  CALL proc_version(OLD.verID);
 END;
$$
DELIMITER ;

--
-- Trigger AFTER delete `con_verLic`
--

DROP TRIGGER IF EXISTS set_con_del;
DELIMITER $$
CREATE TRIGGER set_con_del AFTER DELETE ON con_verLic FOR EACH ROW
 BEGIN
  DELETE FROM show_version WHERE verID = OLD.verID AND OLD.verID NOT IN (SELECT verID FROM con_verLic);
  CALL proc_version(OLD.verID);
 END;
$$
DELIMITER ;


-- db::trigger     - runonce  mpi_ablage/trigger_after_ablage.sql
--
-- Trigger AFTER update `mpi_ablage`
--

DROP TRIGGER IF EXISTS `set_abl_del`;
DELIMITER $$
CREATE TRIGGER `set_abl_del` AFTER UPDATE ON `mpi_ablage` FOR EACH ROW
 BEGIN
  DELETE FROM mpi_ablage WHERE
   ablageID = OLD.ablageID AND OLD.softID IS NULL AND OLD.vertragID IS NULL AND OLD.lizenzID IS NULL;
 END;
$$
DELIMITER ;


-- db::trigger     - runonce  mpi_install/trigger_after_install.sql
--
-- Trigger AFTER insert mpi_install
--

DROP TRIGGER IF EXISTS set_ins_ins;
DELIMITER $$
CREATE TRIGGER set_ins_ins AFTER INSERT ON mpi_install
 FOR EACH ROW BEGIN
  INSERT IGNORE INTO list_pcname SET instID = NEW.instID, instName = (SELECT name FROM view_pc WHERE tabID = NEW.instID);
  IF NEW.verID IS NOT NULL AND NEW.lizenzID IS NOT NULL THEN CALL proc_version(NEW.verID); END IF;
 END;
$$
DELIMITER ;


--
-- Trigger AFTER update mpi_install
--

DROP TRIGGER IF EXISTS set_ins_upd;
DELIMITER $$
CREATE TRIGGER set_ins_upd AFTER UPDATE ON mpi_install
 FOR EACH ROW BEGIN
  INSERT IGNORE INTO list_pcname SET instID = NEW.instID, instName = (SELECT name FROM view_pc WHERE tabID = NEW.instID);
  UPDATE list_pcname SET instName = (SELECT name FROM view_pc WHERE tabID = NEW.instID) WHERE instID = NEW.instID;
  IF NEW.verID IS NOT NULL AND NEW.lizenzID IS NOT NULL THEN CALL proc_version(NEW.verID); END IF;
  IF OLD.verID IS NOT NULL AND OLD.lizenzID IS NOT NULL THEN CALL proc_version(OLD.verID); END IF;
 END;
$$
DELIMITER ;


--
-- Trigger AFTER delete mpi_install
--

DROP TRIGGER IF EXISTS set_ins_del;
DELIMITER $$
CREATE TRIGGER set_ins_del AFTER DELETE ON mpi_install FOR EACH ROW
 BEGIN
  IF OLD.verID IS NOT NULL AND OLD.lizenzID IS NOT NULL THEN CALL proc_version(OLD.verID); END IF;
 END;
$$
DELIMITER ;


-- db::trigger     - runonce  mpi_lizenz/trigger_after_lizenz.sql
--
-- Trigger AFTER insert mpi_lizenz
--

DROP TRIGGER IF EXISTS set_lic_ins;
DELIMITER $$
CREATE TRIGGER set_lic_ins AFTER INSERT ON mpi_lizenz
 FOR EACH ROW BEGIN
  IF NEW.verID IS NOT NULL THEN CALL proc_version(NEW.verID); END IF;
 END;
$$
DELIMITER ;


--
-- Trigger AFTER update mpi_lizenz
--

DROP TRIGGER IF EXISTS set_lic_upd;
DELIMITER $$
CREATE TRIGGER set_lic_upd AFTER UPDATE ON mpi_lizenz
 FOR EACH ROW BEGIN
  IF OLD.verID IS NOT NULL THEN CALL proc_version(OLD.verID); END IF;
  IF NEW.verID IS NOT NULL THEN CALL proc_version(NEW.verID); END IF;
 END;
$$
DELIMITER ;


--
-- Trigger AFTER delete mpi_lizenz
--

DROP TRIGGER IF EXISTS set_lic_del;
DELIMITER $$
CREATE TRIGGER set_lic_del AFTER DELETE ON mpi_lizenz FOR EACH ROW
 BEGIN
  IF OLD.verID IS NOT NULL THEN CALL proc_version(OLD.verID); END IF;
 END;
$$
DELIMITER ;


