
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `mpidb_mpg_licman` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `mpidb_mpg_licman`;
DROP TABLE IF EXISTS `con_softInv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `con_softInv` (
  `conID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `software` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `versionEn` tinyint(1) NOT NULL DEFAULT '0',
  `version` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `anzahl` smallint(6) unsigned NOT NULL DEFAULT '1',
  `searchEn` tinyint(1) NOT NULL DEFAULT '0',
  `searchCmd` tinyint(1) NOT NULL DEFAULT '0',
  `searchStr` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `softID` smallint(6) unsigned zerofill NOT NULL,
  `verID` smallint(6) unsigned zerofill DEFAULT NULL,
  `lizenzID` smallint(6) unsigned zerofill DEFAULT NULL,
  `keyID` smallint(6) unsigned zerofill DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`conID`),
  UNIQUE KEY `searchStr_lizenzID` (`searchStr`,`lizenzID`),
  KEY `lizenzID` (`lizenzID`),
  KEY `verID` (`verID`),
  KEY `softID` (`softID`),
  KEY `keyID` (`keyID`),
  CONSTRAINT `con_softInv_ibfk_10` FOREIGN KEY (`keyID`) REFERENCES `mpi_key` (`keyID`) ON DELETE SET NULL,
  CONSTRAINT `con_softInv_ibfk_3` FOREIGN KEY (`lizenzID`) REFERENCES `mpi_lizenz` (`lizenzID`) ON DELETE SET NULL,
  CONSTRAINT `con_softInv_ibfk_4` FOREIGN KEY (`softID`) REFERENCES `mpi_software` (`softID`) ON DELETE CASCADE,
  CONSTRAINT `con_softInv_ibfk_5` FOREIGN KEY (`verID`) REFERENCES `mpi_version` (`verID`) ON DELETE SET NULL,
  CONSTRAINT `con_softInv_ibfk_6` FOREIGN KEY (`keyID`) REFERENCES `mpi_key` (`keyID`) ON DELETE SET NULL,
  CONSTRAINT `con_softInv_ibfk_7` FOREIGN KEY (`lizenzID`) REFERENCES `mpi_lizenz` (`lizenzID`) ON DELETE SET NULL,
  CONSTRAINT `con_softInv_ibfk_8` FOREIGN KEY (`softID`) REFERENCES `mpi_software` (`softID`) ON DELETE CASCADE,
  CONSTRAINT `con_softInv_ibfk_9` FOREIGN KEY (`verID`) REFERENCES `mpi_version` (`verID`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Zuordnungstabelle SoftwareTabelle zu InventarTabelle';
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `con_softInv` WRITE;
/*!40000 ALTER TABLE `con_softInv` DISABLE KEYS */;
/*!40000 ALTER TABLE `con_softInv` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `set_inv_ins` AFTER INSERT ON `con_softInv`
 FOR EACH ROW BEGIN
  UPDATE mpi_inventar AS inv
   INNER JOIN con_softInv AS con ON
    NEW.status = '1' AND (
    ((NEW.searchEn = '1') AND (NEW.versionEn = '0') AND (NEW.searchCmd = '0') AND (inv.software LIKE NEW.searchStr)) OR
    ((NEW.searchEn = '1') AND (NEW.versionEn = '1') AND (NEW.searchCmd = '0') AND (inv.software LIKE NEW.searchStr) AND (inv.version = NEW.version)) OR
    ((NEW.searchEn = '1') AND (NEW.versionEn = '0') AND (NEW.searchCmd = '1') AND (inv.software RLIKE IF(NEW.searchStr = '',NULL,NEW.searchStr))) OR
    ((NEW.searchEn = '1') AND (NEW.versionEn = '1') AND (NEW.searchCmd = '1') AND (inv.software RLIKE IF(NEW.searchStr = '',NULL,NEW.searchStr)) AND (inv.version = NEW.version)) OR
    ((NEW.searchEn = '0') AND (NEW.versionEn = '0') AND (inv.software = NEW.software)) OR
    ((NEW.searchEn = '0') AND (NEW.versionEn = '1') AND (inv.software = NEW.software) AND (inv.version = NEW.version))
    )
   SET inv.conID = NEW.conID, inv.softID = NEW.softID, inv.verID = NEW.verID, inv.lizenzID = NEW.LizenzID, inv.keyID = NEW.keyID;
  IF NEW.verID IS NOT NULL AND NEW.lizenzID IS NOT NULL THEN CALL proc_version(NEW.verID); END IF;
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `set_inv_upd` AFTER UPDATE ON `con_softInv`
 FOR EACH ROW BEGIN
  UPDATE mpi_inventar AS inv INNER JOIN con_softInv AS con ON
    OLD.status = '1' AND (
    ((OLD.searchEn = '1') AND (OLD.versionEn = '0') AND (OLD.searchCmd = '0') AND (inv.software LIKE OLD.searchStr)) OR
    ((OLD.searchEn = '1') AND (OLD.versionEn = '1') AND (OLD.searchCmd = '0') AND (inv.software LIKE OLD.searchStr) AND (inv.version = OLD.version)) OR
    ((OLD.searchEn = '1') AND (OLD.versionEn = '0') AND (OLD.searchCmd = '1') AND (inv.software RLIKE IF(OLD.searchStr = '',NULL,OLD.searchStr))) OR
    ((OLD.searchEn = '1') AND (OLD.versionEn = '1') AND (OLD.searchCmd = '1') AND (inv.software RLIKE IF(OLD.searchStr = '',NULL,OLD.searchStr)) AND (inv.version = OLD.version)) OR
    ((OLD.searchEn = '0') AND (OLD.versionEn = '0') AND (inv.software = OLD.software)) OR
    ((OLD.searchEn = '0') AND (OLD.versionEn = '1') AND (inv.software = OLD.software) AND (inv.version = OLD.version))
    )
   SET inv.conID = NULL, inv.softID = NULL, inv.verID = NULL, inv.lizenzID = NULL, inv.keyID = NULL;
  IF OLD.verID IS NOT NULL AND OLD.lizenzID IS NOT NULL THEN CALL proc_version(OLD.verID); END IF;
  UPDATE mpi_inventar AS inv INNER JOIN con_softInv AS con ON
    NEW.status = '1' AND (
    ((NEW.searchEn = '1') AND (NEW.versionEn = '0') AND (NEW.searchCmd = '0') AND (inv.software LIKE NEW.searchStr)) OR
    ((NEW.searchEn = '1') AND (NEW.versionEn = '1') AND (NEW.searchCmd = '0') AND (inv.software LIKE NEW.searchStr) AND (inv.version = NEW.version)) OR
    ((NEW.searchEn = '1') AND (NEW.versionEn = '0') AND (NEW.searchCmd = '1') AND (inv.software RLIKE IF(NEW.searchStr = '',NULL,NEW.searchStr))) OR
    ((NEW.searchEn = '1') AND (NEW.versionEn = '1') AND (NEW.searchCmd = '1') AND (inv.software RLIKE IF(NEW.searchStr = '',NULL,NEW.searchStr)) AND (inv.version = NEW.version)) OR
    ((NEW.searchEn = '0') AND (NEW.versionEn = '0') AND (inv.software = NEW.software)) OR
    ((NEW.searchEn = '0') AND (NEW.versionEn = '1') AND (inv.software = NEW.software) AND (inv.version = NEW.version))
    )
   SET inv.conID = NEW.conID, inv.softID = NEW.softID, inv.verID = NEW.verID, inv.lizenzID = NEW.LizenzID, inv.keyID = NEW.keyID;
  IF NEW.verID IS NOT NULL AND NEW.lizenzID IS NOT NULL THEN CALL proc_version(NEW.verID); END IF;
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `set_inv_del` AFTER DELETE ON `con_softInv`
 FOR EACH ROW BEGIN
  UPDATE mpi_inventar AS inv
   INNER JOIN con_softInv AS con ON
    OLD.status = '1' AND (
    ((OLD.searchEn = '1') AND (OLD.versionEn = '0') AND (OLD.searchCmd = '0') AND (inv.software LIKE OLD.searchStr)) OR
    ((OLD.searchEn = '1') AND (OLD.versionEn = '1') AND (OLD.searchCmd = '0') AND (inv.software LIKE OLD.searchStr) AND (inv.version = OLD.version)) OR
    ((OLD.searchEn = '1') AND (OLD.versionEn = '0') AND (OLD.searchCmd = '1') AND (inv.software RLIKE IF(OLD.searchStr = '',NULL,OLD.searchStr))) OR
    ((OLD.searchEn = '1') AND (OLD.versionEn = '1') AND (OLD.searchCmd = '1') AND (inv.software RLIKE IF(OLD.searchStr = '',NULL,OLD.searchStr)) AND (inv.version = OLD.version)) OR
    ((OLD.searchEn = '0') AND (OLD.versionEn = '0') AND (inv.software = OLD.software)) OR
    ((OLD.searchEn = '0') AND (OLD.versionEn = '1') AND (inv.software = OLD.software) AND (inv.version = OLD.version))
    )
   SET inv.conID = NULL, inv.softID = NULL, inv.verID = NULL, inv.lizenzID = NULL, inv.keyID = NULL;
  IF OLD.verID IS NOT NULL AND OLD.lizenzID IS NOT NULL THEN CALL proc_version(OLD.verID); END IF;
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
DROP TABLE IF EXISTS `con_verLic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `con_verLic` (
  `autoID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `verID` smallint(6) unsigned zerofill NOT NULL,
  `lizenzID` smallint(6) unsigned zerofill NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `softID_lizenzID` (`verID`,`lizenzID`),
  KEY `lizenzID` (`lizenzID`),
  KEY `verID` (`verID`),
  CONSTRAINT `con_verLic_ibfk_2` FOREIGN KEY (`lizenzID`) REFERENCES `mpi_lizenz` (`lizenzID`) ON DELETE CASCADE,
  CONSTRAINT `con_verLic_ibfk_4` FOREIGN KEY (`verID`) REFERENCES `mpi_version` (`verID`) ON DELETE CASCADE,
  CONSTRAINT `con_verLic_ibfk_5` FOREIGN KEY (`lizenzID`) REFERENCES `mpi_lizenz` (`lizenzID`) ON DELETE CASCADE,
  CONSTRAINT `con_verLic_ibfk_6` FOREIGN KEY (`verID`) REFERENCES `mpi_version` (`verID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `con_verLic` WRITE;
/*!40000 ALTER TABLE `con_verLic` DISABLE KEYS */;
/*!40000 ALTER TABLE `con_verLic` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `set_con_ins` AFTER INSERT ON `con_verLic`
 FOR EACH ROW BEGIN
  INSERT IGNORE INTO show_version (verID) VALUES (NEW.verID);
  CALL proc_version(NEW.verID);
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `set_con_upd` AFTER UPDATE ON `con_verLic`
 FOR EACH ROW BEGIN
  CALL proc_version(OLD.verID);
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `set_con_del` AFTER DELETE ON `con_verLic`
 FOR EACH ROW BEGIN
  DELETE FROM show_version WHERE verID = OLD.verID AND OLD.verID NOT IN (SELECT verID FROM con_verLic);
  CALL proc_version(OLD.verID);
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
DROP TABLE IF EXISTS `dataface__failed_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__failed_logins` (
  `attempt_id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `time_of_attempt` int(11) NOT NULL,
  PRIMARY KEY (`attempt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__failed_logins` WRITE;
/*!40000 ALTER TABLE `dataface__failed_logins` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__failed_logins` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__index` (
  `index_id` int(11) NOT NULL AUTO_INCREMENT,
  `table` varchar(64) NOT NULL,
  `record_id` varchar(255) NOT NULL,
  `record_url` varchar(255) NOT NULL,
  `record_title` varchar(255) NOT NULL,
  `record_description` text,
  `lang` varchar(2) NOT NULL,
  `searchable_text` text,
  PRIMARY KEY (`index_id`),
  UNIQUE KEY `record_key` (`record_id`,`lang`),
  FULLTEXT KEY `searchable_text_index` (`searchable_text`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__index` WRITE;
/*!40000 ALTER TABLE `dataface__index` DISABLE KEYS */;
INSERT INTO `dataface__index` VALUES (1,'mpi_install','mpi_install?installID=000001','https://192.168.30.128/mpg_licman/index.php?-table=mpi_install&-action=browse&installID=%3D000001','pc100','Adobe Reader','de','Adobe Reader, pc100, , , admin[/////]: A310 R360 P200 A355');
INSERT INTO `dataface__index` VALUES (2,'mpi_install','mpi_install?installID=000001','https://192.168.30.128/mpg_licman/index.php?-table=mpi_install&-action=browse&installID=%3D000001','pc100','Adobe Reader','en','Adobe Reader, pc100, , , admin[/////]: A310 R360 P200 A355');
INSERT INTO `dataface__index` VALUES (3,'mpi_lizenz','mpi_lizenz?lizenzID=000001','https://192.168.30.128/mpg_licman/index.php?-table=mpi_lizenz&-action=browse&lizenzID=%3D000001','Adobe Reader : 10 : Registrierung : 0','Adobe Reader','de','Adobe Reader, Adobe Systems Inc., Adobe Reader : 10 : Registrierung : 0, 10, Registrierung, , 0, , Win, 32, 64, , , , , , , , , test, admin[/////]: A310 R360 A310 S235 I520 A310 R360 0000 R223 0000 0000 R223 0000 W500 0000 0000 T230 A355');
INSERT INTO `dataface__index` VALUES (4,'mpi_lizenz','mpi_lizenz?lizenzID=000001','https://192.168.30.128/mpg_licman/index.php?-table=mpi_lizenz&-action=browse&lizenzID=%3D000001','Adobe Reader : 10 : Registrierung : 0','Adobe Reader','en','Adobe Reader, Adobe Systems Inc., Adobe Reader : 10 : Registrierung : 0, 10, Registrierung, , 0, , Win, 32, 64, , , , , , , , , test, admin[/////]: A310 R360 A310 S235 I520 A310 R360 0000 R223 0000 0000 R223 0000 W500 0000 0000 T230 A355');
INSERT INTO `dataface__index` VALUES (5,'mpi_software','mpi_software?softID=000001','https://192.168.30.128/mpg_licman/index.php?-table=mpi_software&-action=browse&softID=%3D000001','Adobe Reader','Adobe Reader','de','Adobe Reader, , Registrierung, Adobe Systems Inc., , , , , , test, admin[/////]: A310 R360 R223 A310 S235 I520 T230 A355');
INSERT INTO `dataface__index` VALUES (6,'mpi_software','mpi_software?softID=000001','https://192.168.30.128/mpg_licman/index.php?-table=mpi_software&-action=browse&softID=%3D000001','Adobe Reader','Adobe Reader','en','Adobe Reader, , Registrierung, Adobe Systems Inc., , , , , , test, admin[/////]: A310 R360 R223 A310 S235 I520 T230 A355');
INSERT INTO `dataface__index` VALUES (7,'mpi_version','mpi_version?verID=000001','https://192.168.30.128/mpg_licman/index.php?-table=mpi_version&-action=browse&verID=%3D000001','Adobe Reader : 10','Adobe Reader','de','Adobe Reader, 10, test, admin[/////]: A310 R360 0000 T230 A355');
INSERT INTO `dataface__index` VALUES (8,'mpi_version','mpi_version?verID=000001','https://192.168.30.128/mpg_licman/index.php?-table=mpi_version&-action=browse&verID=%3D000001','Adobe Reader : 10','Adobe Reader','en','Adobe Reader, 10, test, admin[/////]: A310 R360 0000 T230 A355');
INSERT INTO `dataface__index` VALUES (9,'mpi_version','mpi_version?verID=000002','https://192.168.30.128/mpg_licman/index.php?-table=mpi_version&-action=browse&verID=%3D000002','Adobe Reader : 11','Adobe Reader','de','Adobe Reader, 11, test, admin[/////]: A310 R360 0000 T230 A355');
INSERT INTO `dataface__index` VALUES (10,'mpi_version','mpi_version?verID=000002','https://192.168.30.128/mpg_licman/index.php?-table=mpi_version&-action=browse&verID=%3D000002','Adobe Reader : 11','Adobe Reader','en','Adobe Reader, 11, test, admin[/////]: A310 R360 0000 T230 A355');
INSERT INTO `dataface__index` VALUES (11,'mpi_vertrag','mpi_vertrag?vertragID=000001','https://192.168.30.128/mpg_licman/index.php?-table=mpi_vertrag&-action=browse&vertragID=%3D000001','Adobe Systems Inc. : 1234567','1234567','de','1234567, , Adobe Systems Inc., Adobe, , , admin[/////]: 0000 A310 S235 I520 A310 A355');
INSERT INTO `dataface__index` VALUES (12,'mpi_vertrag','mpi_vertrag?vertragID=000001','https://192.168.30.128/mpg_licman/index.php?-table=mpi_vertrag&-action=browse&vertragID=%3D000001','Adobe Systems Inc. : 1234567','1234567','en','1234567, , Adobe Systems Inc., Adobe, , , admin[/////]: 0000 A310 S235 I520 A310 A355');
/*!40000 ALTER TABLE `dataface__index` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__modules` (
  `module_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `module_version` int(11) DEFAULT NULL,
  PRIMARY KEY (`module_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__modules` WRITE;
/*!40000 ALTER TABLE `dataface__modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__modules` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__mtimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__mtimes` (
  `name` varchar(255) NOT NULL,
  `mtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__mtimes` WRITE;
/*!40000 ALTER TABLE `dataface__mtimes` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__mtimes` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__preferences` (
  `pref_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `table` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `record_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`pref_id`),
  KEY `username` (`username`),
  KEY `table` (`table`),
  KEY `record_id` (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__preferences` WRITE;
/*!40000 ALTER TABLE `dataface__preferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__preferences` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__record_mtimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__record_mtimes` (
  `recordhash` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `recordid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mtime` int(11) NOT NULL,
  PRIMARY KEY (`recordhash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__record_mtimes` WRITE;
/*!40000 ALTER TABLE `dataface__record_mtimes` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__record_mtimes` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__version` (
  `version` int(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__version` WRITE;
/*!40000 ALTER TABLE `dataface__version` DISABLE KEYS */;
INSERT INTO `dataface__version` VALUES (1100);
/*!40000 ALTER TABLE `dataface__version` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_email` (
  `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_email` WRITE;
/*!40000 ALTER TABLE `list_email` DISABLE KEYS */;
/*!40000 ALTER TABLE `list_email` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_hersteller`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_hersteller` (
  `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `hersteller` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `notiz` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `hersteller` (`hersteller`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_hersteller` WRITE;
/*!40000 ALTER TABLE `list_hersteller` DISABLE KEYS */;
INSERT INTO `list_hersteller` VALUES (1,'Adobe Systems Inc.',NULL);
/*!40000 ALTER TABLE `list_hersteller` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_inventar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_inventar` (
  `anlage` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `unterNr` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `invNummer` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `bezeichnung` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `standort` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wert` decimal(7,2) DEFAULT NULL,
  `invDatum` date DEFAULT NULL,
  `kostenstelle` decimal(5,0) DEFAULT NULL,
  `notiz` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`anlage`,`unterNr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_inventar` WRITE;
/*!40000 ALTER TABLE `list_inventar` DISABLE KEYS */;
/*!40000 ALTER TABLE `list_inventar` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_katReiter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_katReiter` (
  `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `kategorie` (`kategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_katReiter` WRITE;
/*!40000 ALTER TABLE `list_katReiter` DISABLE KEYS */;
INSERT INTO `list_katReiter` VALUES (5,'Ablage');
INSERT INTO `list_katReiter` VALUES (7,'Auswertung');
INSERT INTO `list_katReiter` VALUES (4,'Autorisierung');
INSERT INTO `list_katReiter` VALUES (1,'Haupttabelle');
INSERT INTO `list_katReiter` VALUES (8,'History');
INSERT INTO `list_katReiter` VALUES (2,'Liste');
INSERT INTO `list_katReiter` VALUES (9,'Programmierung');
INSERT INTO `list_katReiter` VALUES (3,'View');
INSERT INTO `list_katReiter` VALUES (6,'Zuordnung');
/*!40000 ALTER TABLE `list_katReiter` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_kategorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_kategorie` (
  `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `kategorie` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `kategorie` (`kategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_kategorie` WRITE;
/*!40000 ALTER TABLE `list_kategorie` DISABLE KEYS */;
INSERT INTO `list_kategorie` VALUES (1,'--');
INSERT INTO `list_kategorie` VALUES (2,'Accounting');
INSERT INTO `list_kategorie` VALUES (17,'Bueroanwendung');
INSERT INTO `list_kategorie` VALUES (3,'Cloudservice');
INSERT INTO `list_kategorie` VALUES (4,'Database');
INSERT INTO `list_kategorie` VALUES (5,'Development');
INSERT INTO `list_kategorie` VALUES (15,'Facility');
INSERT INTO `list_kategorie` VALUES (6,'Game');
INSERT INTO `list_kategorie` VALUES (7,'Graphics');
INSERT INTO `list_kategorie` VALUES (8,'Labor');
INSERT INTO `list_kategorie` VALUES (9,'Modeling');
INSERT INTO `list_kategorie` VALUES (10,'Operating System');
INSERT INTO `list_kategorie` VALUES (11,'Others');
INSERT INTO `list_kategorie` VALUES (12,'Programming');
INSERT INTO `list_kategorie` VALUES (16,'Projektmanagment');
INSERT INTO `list_kategorie` VALUES (13,'Tools');
INSERT INTO `list_kategorie` VALUES (14,'Wissenschaft');
/*!40000 ALTER TABLE `list_kategorie` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_kostenstelle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_kostenstelle` (
  `kostenstelle` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `beschreibung` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`kostenstelle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_kostenstelle` WRITE;
/*!40000 ALTER TABLE `list_kostenstelle` DISABLE KEYS */;
/*!40000 ALTER TABLE `list_kostenstelle` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_lagerort`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_lagerort` (
  `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `lagerort` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `lagerort` (`lagerort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_lagerort` WRITE;
/*!40000 ALTER TABLE `list_lagerort` DISABLE KEYS */;
/*!40000 ALTER TABLE `list_lagerort` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_lieferant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_lieferant` (
  `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `lieferant` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ansprechpartnerID` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefonID` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `positionID` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ansprechpartnerAD` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefonAD` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `positionAD` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `strasse` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plz` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ort` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `land` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `webseite` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passwort` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kundenNr` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notiz` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `lieferant` (`lieferant`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_lieferant` WRITE;
/*!40000 ALTER TABLE `list_lieferant` DISABLE KEYS */;
INSERT INTO `list_lieferant` VALUES (1,'Adobe',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2015-05-27 13:40:49');
/*!40000 ALTER TABLE `list_lieferant` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_logintyp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_logintyp` (
  `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `typ` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `typ` (`typ`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_logintyp` WRITE;
/*!40000 ALTER TABLE `list_logintyp` DISABLE KEYS */;
INSERT INTO `list_logintyp` VALUES (1,'AdminConsole');
INSERT INTO `list_logintyp` VALUES (2,'Distributor');
INSERT INTO `list_logintyp` VALUES (3,'Download');
INSERT INTO `list_logintyp` VALUES (4,'Forum');
INSERT INTO `list_logintyp` VALUES (9,'Info');
INSERT INTO `list_logintyp` VALUES (7,'Lizenzserver');
INSERT INTO `list_logintyp` VALUES (5,'Login');
INSERT INTO `list_logintyp` VALUES (8,'Registrierung');
INSERT INTO `list_logintyp` VALUES (6,'Support');
INSERT INTO `list_logintyp` VALUES (10,'Wiki');
/*!40000 ALTER TABLE `list_logintyp` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_modell`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_modell` (
  `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `modell` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `bedeutung` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `modell` (`modell`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_modell` WRITE;
/*!40000 ALTER TABLE `list_modell` DISABLE KEYS */;
INSERT INTO `list_modell` VALUES (2,'Campus','Eine Campus-Lizenz entspricht der Educational Version und bezeichnet eine Lizenz für alle Systeme der Universität und für beliebig viele gleichzeitige Nutzer auf jedem System.\r\nDas Lizenz-Modell enspricht der \"Enterprise Subscription\"');
INSERT INTO `list_modell` VALUES (3,'Concurrent','Die Software selbst kann beim Concurrent-User-Lizenzmodell auf beliebig vielen Rechnern installiert sein. Ein zentraler Server verwaltet dabei die Lizenzen, die auch Floating-Lizenzen (Siemens) oder Netzwerklizenzen (ESRI) genannt werden. Der Server registriert die Anzahl der aktuell vergebenen Lizenzen und gewährt jedem prinzipiell berechtigten Benutzer das Recht für die Benutzung. Sind alle Lizenzen vergeben, muss ein zusätzlicher konkurrierender Benutzer warten, bis ein anderer Benutzer seine Session beendet und somit wieder eine Lizenz zur Verfügung steht.');
INSERT INTO `list_modell` VALUES (4,'Educational','Bei dieser Lizenz handelt es sich um Software, welche der Hersteller speziell für Schulen, Universitäten, Bildungseinrichtungen, Lehrer, Studenten und Schüler anbietet. Es handelt sich zumeist um Versionen der Software, die gegen Nachweis der Berechtigung zu einem günstigeren Preis abgegeben wird.');
INSERT INTO `list_modell` VALUES (5,'Einzelplatz','Die jeweilige Einzellizenz darf nur auf einem Arbeitsplatzrechner installiert werden. Soll das Produkt auf mehreren Rechnern laufen, müssen auch mehrere Lizenzen erworben werden. Dies bedeutet, dass wenn beispielsweise ein Microsoft Office-Paket auf 2 oder mehr Rechnern gleichzeitig eingesetzt werden soll, muss man auch die entsprechende Anzahl von Lizenzen erwerben.');
INSERT INTO `list_modell` VALUES (6,'Institutslizenz','Grundsätzlich erwirbt man mit dem Kauf einer perpetual Lizenz ein zeitlich nicht eingeschränktes Nutzungsrecht für diese Software (perpetual = immerwährend).\r\nDie Zahl der Nutzer unbeschränkt, es besteht kein Ablaufdatum, u.U. gibt es eine Beschränkung auf eine Nutzergruppe.');
INSERT INTO `list_modell` VALUES (7,'Institutsmietlizenz','Enspricht Mietvertrag. Die Zahl der Nutzer ist unbeschränkt.\r\nEnterprise Agreement Subscription (Konzern-Abonnement-Beitritt) bietet Ihnen die Möglichkeit, die gewünschte Software nicht als dauerhafte Lizenz zu erwerben, sondern diese zu abonnieren - kurz: zu mieten.');
INSERT INTO `list_modell` VALUES (8,'Floating','Bei einer Floating-Lizenz handelt es sich um eine Lizenz, bei der die Firma/der Kunde pauschal für das Netz eine Nutzungslizenz für eine Software abgeschlossen hat, die die gleichzeitige Nutzung dieser Software durch eine bestimmte Anzahl von Benutzern erlaubt. Die Nutzung kann dabei von einem beliebigen Rechner im Netz erfolgen, sofern die Software für diese Plattform verfügbar ist. Im Netz arbeitet ein License-Manager, der die Anzahl der momentan benutzten Lizenzen überwacht. Gleichbedeutend mit concurrent.');
INSERT INTO `list_modell` VALUES (9,'Gruppe','Speziell fuer eine Gruppe konzipierte Lizenz');
INSERT INTO `list_modell` VALUES (10,'Endnutzer','Das Named-User-Lizenzmodell beschreibt in der Informationstechnologie eine Lizenzierungsform, bei der die maximale Anzahl der Nutzer festgelegt wird, die mit einem registrierten, namentlich eingetragenen Zugang auf eine Ressource zugreifen dürfen. Eine Ressource ist zum Beispiel eine Softwareanwendung, ein Batchprozessor, eine Datei oder ein Arbeitsplatz. Die Named-User-Lizenzierung unterscheidet sich damit von dem Concurrent-User-Lizenzmodell, bei dem die Anzahl der zeitgleich zugreifenden Nutzer festgelegt wird.');
INSERT INTO `list_modell` VALUES (11,'Node-Locked','Computersoftware kann auf verschiedene Art und Weise lizenziert und diese Lizenzierung mit einer Lizenzmanager-Software überwacht werden. Eine Lizenz, um eine Software auf genau einem definierten Computer zu betreiben, wird in der FLEXlm Software als Node-Locked bezeichnet, sie erlaubt die Benutzung der Software auf genau einem Rechner, einem Knoten oder node, einem genau bestimmten Rechner in einem Computernetzwerk. Die Benutzung einer solchen Lizenz ist an genau diesen Rechner gebunden.');
INSERT INTO `list_modell` VALUES (12,'OEM','Diese Lizenzform ist eigentlich nur für Hersteller von PCs und sonstigen Hardware- bzw. Software-Komponenten gedacht.\r\nUm nicht nur ein paar zusammengesteckte Platinen, Laufwerke und Kabel auszuliefern, liefern PC-Hersteller wichtige Software-Produkte meist vorinstalliert direkt mit dem Computer aus. Dies kann beispielsweise das Windows-Betriebssystem, ein Office-Paket oder eine Brenn-Software sein.\r\nAufgrund der hohen einzukaufenden Stückzahlen und der niedrigen Verkaufspreise seiner Gesamtsysteme kann kein PC Hersteller den vollen (Laden-) Kaufpreis für die mitgelieferte Software bezahlen. Hier helfen die Software-Hersteller aus und bieten so genannte OEM-Versionen ihrer Produkte an. Dabei handelt es sich in der Regel um Software-Versionen die in ihrem Funktionsumfang eingeschränkt sind (z.B. Brenn- oder Bildbearbeitungssoftware die nur mit einem bestimmten Brenner oder Drucker funktioniert). Häufig sind OEM-Versionen mit integrierten Upgrade-Angeboten versehen, um den Nutzer die Funktionen der Vollversionen anzupreisen und dadurch zusätzliche Umsätze zu erzielen.\r\nNormalerweise wird seitens der Software Hersteller für OEM Versionen kein Support geleistet. Diese Support-Aufgaben werden in der Regel dem Hardware-Hersteller, welcher das komplette System ausgeliefert hat, überlassen.\r\nBei zum Verkauf angebotenen OEM-Versionen, empfiehlt es sich genau darauf zu achten, dass die Software auch wirklich mit der eigenen Hardware zusammenarbeitet.\r\n');
INSERT INTO `list_modell` VALUES (13,'Trialversion','Eine Trialversion ist eine eingeschränkte Demoversion (Testversion) von einer kommerziellen Software, die zum Beispiel im Funktionsumfang, der Nutzungszeit und/oder anderweitig beschnitten ist.\r\nIn erster Linie soll der Anwender mittels einer Testversion die Möglichkeit bekommen, sich mit der Software auseinanderzusetzen, um sie dann bei Gefallen zu kaufen.');
INSERT INTO `list_modell` VALUES (14,'Volumen','Volumen- oder Multi-Produkt-Lizenzen sind für (Firmen-)Kunden gedacht, die eine größere Menge an Software Lizenzen für sich selbst erwerben. Erwirbt ein großes Unternehmen für mehrere tausend Computer eine Software, erhält dieses aufgrund der hohen Stückzahl einen Preisnachlass. Darüber hinaus hat das Unternehmen besondere Support-Anforderungen (z.B. Installation und Wartung über einen Server) die direkt vom Hersteller oder einem entsprechenden Partner durchgeführt werden.\r\nMicrosoft Volumenlizenzprogramme\r\nDurch den Erwerb von Volumenlizenzen wird es leichter und kostengünstiger Software auf mehreren Computern innerhalb eines Unternehmens laufen zu lassen.\r\nDurch den Erwerb von Softwarelizenzen in einem Microsoft Volumenlizenzprogramm zahlt man nur für die Softwarelizenz. Software in einem Einzelhandelspaket hingegen beinhaltet u.a. Medien (CD-ROM oder DVD) und einen Nutzerleitfaden. Der Erwerb von Volumenlizenzen ohne die Kosten für diese Paketbestandteile verringert oft die Kosten und bietet an Kundenbedürfnisse angepasste Lizenzoptionen sowie verbessertes Softwaremanagement. Abhängig vom gewählten Volumenlizenzprogramm erhält der Kunde Medien oder hat die Option auf Medien oder kann Medien (oder zusätzliche Medien) sowie Dokumentation und Produktsupport separat nach Bedarf erwerben.');
INSERT INTO `list_modell` VALUES (15,'Registrierung','persoenliche Registrierung beim Hersteller');
INSERT INTO `list_modell` VALUES (16,'Prozessor','Gleichzeitig benutzbare CPU-Kerne.');
INSERT INTO `list_modell` VALUES (17,'Token','Das Token-based-Lizenzmodell beschreibt in der Informationstechnik eine Lizenzierungsform, bei der die maximale Zahl von Tokens (Gutscheinen) festgelegt wird, die in einem Warenkorb mit bestimmten Softwareanwendungen zur Verfügung steht.\r\n\r\nJede Softwareanwendung im Warenkorb hat einen Wert von „x“ Token. Sind die Gutscheine aufgebraucht, kann keine weitere Softwareanwendung aus dem Warenkorb gestartet werden. Wird eine dem Warenkorb zugehörige Softwareanwendung beendet, stehen die Token wieder zur Verfügung. Der Wert der Softwareanwendungen wird durch den Software-Hersteller festgelegt. So können für Anwendung A zehn Token und Anwendung B drei Token und Anwendung C sieben Token fällig sein. Sind durch die Benutzer Anwendung A dreimal und Anwendung B zweimal im Einsatz so kann bei 50 Token maximal noch zweimal Anwendung  C aufgerufen werden. Der Vorteil für den Kunden liegt darin, dass er sich nicht im Vorfeld auf die Zahl der Nutzer oder Lizenzen festlegen muss. Aber auch bei Projektgeschäft mit verschiedenen Phasen (z. B. Produktentwicklung und Test) können verschiedene Softwareapplikationen zum Einsatz kommen, die von den gleichen Benutzern eingesetzt werden und somit nicht mehrfach im Vorfeld lizenziert und somit auch gekauft werden müssen.\r\n\r\nGrundlage für das Token-based-Lizenzmodell ist das Concurrent-User-Lizenzmodell. Zur Realisierung sollte man eine sogenannte Lizenzmanagement-Software einsetzten. Um die Auslastung in Echtzeit zu überwachen, aktiv die Lizenzen im Warenkorb zu managen oder die tatsächliche Anzahl von Token zu ermitteln, sollte eine Lizenzmonitoring-Software eingesetzt werden.');
INSERT INTO `list_modell` VALUES (18,'Dongle','Kopierschutzstecker, Hardlock oder (Hardware-)Key genannt, dienen dazu, Software vor unautorisierter Vervielfältigung zu schützen. Software darf meist mehrfach auf mehreren Rechnern installiert werden, kann aber nur auf dem PC mit Dongle genutzt werden.');
INSERT INTO `list_modell` VALUES (19,'MS Server Datacenter','Modell nur fuer Windows Server\r\nIst dem physischen Server die erforderliche Anzahl an Prozessorlizenzen zugewiesen, darf Windows Server auf diesem Server in beliebig vielen Betriebssystemumgebungen ausgeführt werden.');
INSERT INTO `list_modell` VALUES (20,'MS Server Standard','Jede Lizenz deckt bis zu zwei physische Prozessoren ab. Hat ein Server mehr als zwei physische Prozessoren, muss dem Server die entsprechende Anzahl von Lizenzen zugewiesen werden. Auf dem lizenzierten Server darf für jede Lizenz eine Instanz in der physischen Betriebssystemumgebung und eine Instanz in bis zu zwei virtuellen Betriebssystemumgebungen ausgeführt werden.\r\n\r\nWerden beide erlaubten virtuellen Instanzen genutzt, darf die Instanz in der physischen Betriebssystemumgebung nur zum Hosten und Verwalten der virtuellen Betriebssystemumgebung verwendet werden.\r\n\r\nWerden weitere virtuelle Instanzen benötigt, kann die Anzahl der virtuellen Instanzen erhöht werden, indem dem Server mehrere Windows Server 2012 Standard-Lizenzen zugewiesen werden. Es müssen somit alle physischen Prozessoren korrekt lizenziert sein und dem Server Lizenzen in der entsprechenden Anzahl zugewiesen sein, um die maximale Anzahl der genutzten virtuellen Instanzen abzudecken.\r\n\r\nBeispiel:\r\nEin Server verfügt über zwei Prozessoren, gleichzeitig sollen auf diesem Server sechs virtuelle Instanzen von Windows Server 2012 Standard genutzt werden. Für die zwei physischen Prozessoren ist eine Windows Server 2012 Standard-Lizenz erforderlich. Da diese aber nur zum Einsatz von zwei zusätzlichen virtuellen Instanzen berechtigt, müssen dem Server zwei weitere Windows Server 2012-Lizenzen zugewiesen werden, um die Anzahl der virtuellen Instanzen auf die erforderlich Zahl von sechs zu erhöhen.');
INSERT INTO `list_modell` VALUES (21,'CAL (MS Server)','Mit einer Client Access License (kurz: CAL), das heißt einer Zugriffslizenz, lizenzieren Sie die Zugriffe von Geräten oder Nutzern auf Microsoft-Server, deren Lizenzmodell es vorsieht, dass die Zugriffe auf die Serversoftware gesondert zu lizenzieren sind. Beispiele sind Exchange Server 2013, SharePoint Server 2013 oder auch Windows Server 2012.\r\n\r\nClient Access License, CALs werden entweder einem Gerät zugewiesen oder einem Nutzer.\r\n\r\nGeräte-CAL\r\nGeräte-CAL: Eine Geräte-CAL lizenziert ein Gerät für die Verwendung durch eine beliebige Anzahl von Nutzern, die von diesem Gerät aus auf Instanzen der Serversoftware zugreifen dürfen.\r\n\r\nDieser Lizenztyp ist dann optimal, wenn von einem Gerät mehrere Nutzer aus arbeiten. Beispielsweise PCs an einer Hotelrezeption, PCs, die von einem Team abwechselnd eingesetzt werden, PCs aus einem PC-Pool etc.\r\n \r\nNutzer-CAL\r\nNutzer-CAL: Eine Nutzer-CAL berechtigt einen bestimmten Nutzer zur Verwendung einer beliebigen Anzahl von Geräten, wie z.B. Firmen-PC, privater PC, Mobiltelefon etc., von welchen er auf die Serversoftware zugreifen darf.\r\n\r\nDiesen Lizenztyp sollten Sie dann wählen, wenn ein Nutzer nicht nur mit einem Gerät auf den entsprechenden Server zugreift und zum Beispiel neben seinem PC auch sein Smartphone nutzt, um E-Mails abzurufen, oder ein Tablet als Zweitgerät einsetzt. Gleiches gilt, wenn den Mitarbeitern die Möglichkeit gegeben wird, auch Ihre privaten Geräte zu nutzen und sich mit diesen im Netzwerk anzumelden.');
INSERT INTO `list_modell` VALUES (22,'CAL (MS RDS)','');
INSERT INTO `list_modell` VALUES (23,'CAL (MS SQL)','');
/*!40000 ALTER TABLE `list_modell` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_nutzer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_nutzer` (
  `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `nutzer` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `beschreibung` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `nutzer` (`nutzer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_nutzer` WRITE;
/*!40000 ALTER TABLE `list_nutzer` DISABLE KEYS */;
/*!40000 ALTER TABLE `list_nutzer` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_nutzrecht`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_nutzrecht` (
  `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `nutzrecht` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `bemerkung` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `nutzrecht` (`nutzrecht`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_nutzrecht` WRITE;
/*!40000 ALTER TABLE `list_nutzrecht` DISABLE KEYS */;
INSERT INTO `list_nutzrecht` VALUES (1,'Kommerziell','Man erwirbt i.d.R. nur ein Nutzungsrecht an der Software. Vor der Nutzung ist der Kauf einer Lizenz erforderlich, der Autor behält in jedem Fall das Copyright.');
INSERT INTO `list_nutzrecht` VALUES (2,'Shareware','Üblicherweise ist es bei Shareware erlaubt, die Software in unveränderter Form beliebig zu kopieren oder zu verteilen, jedoch im Gegensatz zu Freeware mit einer Aufforderung, sich nach einem Testzeitraum (üblicherweise 30 Tage) beim Autor kostenpflicht');
INSERT INTO `list_nutzrecht` VALUES (3,'Freeware','Software, die vom Urheber zur kostenlosen Nutzung zur Verfügung gestellt wird. Freeware ist meistens proprietär und darf daher nicht mit freier Software (engl. „free software“) verwechselt werden, die im Gegensatz zu Freeware weitläufigere Freihei');
INSERT INTO `list_nutzrecht` VALUES (4,'GNU GPL','Die GNU General Public License (auch GPL oder GNU GPL) ist die am weitesten verbreitete Software-Lizenz, welche den Endnutzern (Privatpersonen, Organisationen, Firmen) die Freiheiten garantiert, die Software nutzen, studieren, verbreiten (kopieren) und ändern zu dürfen. Software, die diese Freiheitsrechte gewährt, wird Freie Software genannt.\r\n\r\nDie GPL-Lizenz kann von jedem verwendet werden, um die Freiheitsrechte der Endnutzer sicherzustellen. Sie ist die erste Copyleft-Lizenz für den allgemeinen Gebrauch. Copyleft bedeutet, dass Änderungen oder Ableitungen von GPL-lizenzierten Werken nur unter den gleichen Lizenzbedingungen (also eben GPL) vertrieben werden dürfen. Damit gewährt die GPL den Empfängern eines Computerprogramms die Freiheitsrechte Freier Software und nutzt Copyleft, um sicherzustellen, dass diese Freiheiten bei Weiterverbreitung erhalten bleiben, auch wenn die Software verändert oder erweitert wird. Freizügige Lizenzen wie die BSD-Lizenz hingegen fordern nicht das Copyleft.');
INSERT INTO `list_nutzrecht` VALUES (5,'Donationware','ist Freeware, bei der eine eventuelle Bezahlung dem Benutzer freigestellt bleibt.');
INSERT INTO `list_nutzrecht` VALUES (6,'OpenSource','Open Source wird oft synonym zu Freier Software gebraucht, kann aber auch lediglich auf Quelloffenheit hinweisen.');
INSERT INTO `list_nutzrecht` VALUES (7,'Registrierung','Kostenlose Software, welche aber nur nach Registrierung im Intranet per Softwareverteilung installiert werden darf.');
INSERT INTO `list_nutzrecht` VALUES (8,'--','unbekannt');
/*!40000 ALTER TABLE `list_nutzrecht` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_ordnung`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_ordnung` (
  `orderID` tinyint(3) unsigned zerofill NOT NULL,
  `ordnung` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `orderID` (`orderID`),
  UNIQUE KEY `ordnung` (`ordnung`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_ordnung` WRITE;
/*!40000 ALTER TABLE `list_ordnung` DISABLE KEYS */;
INSERT INTO `list_ordnung` VALUES (010,'1');
INSERT INTO `list_ordnung` VALUES (020,'2');
INSERT INTO `list_ordnung` VALUES (030,'3');
INSERT INTO `list_ordnung` VALUES (040,'4');
INSERT INTO `list_ordnung` VALUES (050,'5');
INSERT INTO `list_ordnung` VALUES (060,'6');
INSERT INTO `list_ordnung` VALUES (070,'7');
INSERT INTO `list_ordnung` VALUES (080,'8');
INSERT INTO `list_ordnung` VALUES (090,'9');
INSERT INTO `list_ordnung` VALUES (099,'H');
INSERT INTO `list_ordnung` VALUES (001,'L');
/*!40000 ALTER TABLE `list_ordnung` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_pcname`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_pcname` (
  `instID` smallint(6) unsigned zerofill NOT NULL,
  `instName` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`instID`),
  KEY `instName` (`instName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_pcname` WRITE;
/*!40000 ALTER TABLE `list_pcname` DISABLE KEYS */;
INSERT INTO `list_pcname` VALUES (000001,'pc100','2015-04-23 09:21:39');
/*!40000 ALTER TABLE `list_pcname` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_quelle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_quelle` (
  `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `quelle` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `quelle` (`quelle`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_quelle` WRITE;
/*!40000 ALTER TABLE `list_quelle` DISABLE KEYS */;
INSERT INTO `list_quelle` VALUES (1,'--');
INSERT INTO `list_quelle` VALUES (2,'CD/DVD');
INSERT INTO `list_quelle` VALUES (3,'Download von Webseite (siehe links)');
INSERT INTO `list_quelle` VALUES (5,'EmpirumPaket');
INSERT INTO `list_quelle` VALUES (6,'ISO');
INSERT INTO `list_quelle` VALUES (4,'Sharefolder (siehe links)');
/*!40000 ALTER TABLE `list_quelle` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_reiter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_reiter` (
  `autoID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `reiter` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `favorit` tinyint(1) NOT NULL DEFAULT '0',
  `history` tinyint(1) NOT NULL DEFAULT '0',
  `bedeutung` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `reiter` (`reiter`),
  KEY `kategorie` (`kategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=435 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Reiterlinks fuer Xataface';
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_reiter` WRITE;
/*!40000 ALTER TABLE `list_reiter` DISABLE KEYS */;
INSERT INTO `list_reiter` VALUES (000001,'con_softInv','Zuordnung',0,1,'Zuordnung von Softwarenamen zu Inventorynamen');
INSERT INTO `list_reiter` VALUES (000002,'con_verLic','Zuordnung',1,0,'Zuordnung von Mehrfach-Lizenzen zu Softwareversionen');
INSERT INTO `list_reiter` VALUES (000003,'list_email','Liste',0,0,'Emailadresse fuer Benachrichtung Ablauf Lizenz oder Support');
INSERT INTO `list_reiter` VALUES (000004,'list_hersteller','Liste',0,0,'Liste Hersteller fuer Software und Vereinbarungen');
INSERT INTO `list_reiter` VALUES (000005,'list_kategorie','Liste',0,0,'Liste Software-Kategorien');
INSERT INTO `list_reiter` VALUES (000006,'list_katReiter','Liste',0,0,'Zugehoerigkeit DB-Tabellen');
INSERT INTO `list_reiter` VALUES (000007,'list_lagerort','Liste',0,0,'Ablageort Schlussel');
INSERT INTO `list_reiter` VALUES (000008,'list_kostenstelle','Liste',0,0,'Liste Kostenstellen');
INSERT INTO `list_reiter` VALUES (000009,'list_lieferant','Liste',0,0,'Liste Lieferanten fuer Lizenzen und Vereinbarungen');
INSERT INTO `list_reiter` VALUES (000010,'list_logintyp','Liste',0,0,'Zuordnung Links zu einer Kategorie');
INSERT INTO `list_reiter` VALUES (000011,'list_modell','Liste',0,1,'Liste Lizenz-Modelle');
INSERT INTO `list_reiter` VALUES (000012,'list_nutzer','Liste',0,0,'Liste Nutzer in Lizenzen und Installationen');
INSERT INTO `list_reiter` VALUES (000013,'list_nutzrecht','Liste',0,0,'Liste Software-Nutzrechte ');
INSERT INTO `list_reiter` VALUES (000014,'list_pcname','Liste',0,0,'Sicherheitskopie Inventarname, falls Eintrag aus it_inv geloescht wird');
INSERT INTO `list_reiter` VALUES (000015,'list_quelle','Liste',0,0,'Mediumquelle fuer Software');
INSERT INTO `list_reiter` VALUES (000016,'list_reiter','Liste',0,0,'Sammelcontainer fuer Tabbutton \"mehr ..\"');
INSERT INTO `list_reiter` VALUES (000017,'list_softart','Liste',0,0,'Zuordnung Lizenzschluessel zu einer Kategorie');
INSERT INTO `list_reiter` VALUES (000018,'list_vorgang','Liste',0,0,'Zuordnung Dateiablage zu einer Kategorie');
INSERT INTO `list_reiter` VALUES (000019,'mpi_ablage','Ablage',1,0,'Dateiablagen fuer Software und Lizenzen');
INSERT INTO `list_reiter` VALUES (000020,'mpi_install','Liste',0,1,'Installationen von Lizenzen auf Geraete');
INSERT INTO `list_reiter` VALUES (000021,'mpi_inventar','Liste',1,0,'Inventroy aktueller Software (Ist-Zustand)');
INSERT INTO `list_reiter` VALUES (000022,'mpi_key','Liste',0,1,'Liste aller verfuegbaren Schluessel');
INSERT INTO `list_reiter` VALUES (000023,'mpi_kosten','Ablage',1,0,'Ablage Kosten zu Rechung, Angebote etc. zur Statistikauswertung');
INSERT INTO `list_reiter` VALUES (000024,'mpi_links','Ablage',0,0,'Linksammlung zu diversen Webseiten oder Logins');
INSERT INTO `list_reiter` VALUES (000025,'mpi_lizenz','Haupttabelle',0,1,'Liste aller Lizenzen');
INSERT INTO `list_reiter` VALUES (000026,'mpi_notiz','Ablage',0,0,'Liste Lizenznotizen');
INSERT INTO `list_reiter` VALUES (000027,'mpi_software','Haupttabelle',0,1,'Liste aller lizenzierbaren Software');
INSERT INTO `list_reiter` VALUES (000029,'mpi_version','Haupttabelle',0,1,'Versionen der Software');
INSERT INTO `list_reiter` VALUES (000030,'mpi_vertrag','Haupttabelle',0,1,'Liste aller Vereinbarungen');
INSERT INTO `list_reiter` VALUES (000032,'view_install','Programmierung',0,0,'Zeige vorhandene und verbrauchte Lizenzen');
INSERT INTO `list_reiter` VALUES (000033,'view_instLic','Programmierung',0,0,'Zusammenfassung Inventar und Install (Man. Installation dominant)');
INSERT INTO `list_reiter` VALUES (000034,'view_invInst','Programmierung',0,0,'Zeige alle manuellen und automatische Softwareinstallationen auch ohne lizenzID');
INSERT INTO `list_reiter` VALUES (000035,'view_kosten','Auswertung',0,0,'Auswertung Buchungskosten');
INSERT INTO `list_reiter` VALUES (000036,'view_lizenz','Auswertung',1,0,'Zeige vorhandene und verbrauchte Lizenzen pro Lizenz');
INSERT INTO `list_reiter` VALUES (000037,'view_pc','View',0,0,'Auswahl Geraetename aus DB it_inv');
INSERT INTO `list_reiter` VALUES (000040,'view_user','View',0,0,'Auswahlliste fuer aktive und nicht abgelaufene Benutzer');
INSERT INTO `list_reiter` VALUES (000041,'view_version','Auswertung',1,0,'Lizenzverbrauch Softwareversionen inkl. Kreuzverbindungen');
INSERT INTO `list_reiter` VALUES (000042,'mpi_lizenz__history','History',0,0,'History Lizenzen');
INSERT INTO `list_reiter` VALUES (000043,'mpi_software__history','History',0,0,'History Lizenzen');
INSERT INTO `list_reiter` VALUES (000044,'view_invCon','Programmierung',0,0,'Zeige alle per Suchstring gefundenen Installationen vom Inventory');
INSERT INTO `list_reiter` VALUES (000045,'sort_lizenzSoft','Programmierung',0,0,'Sortierung fuer schnellfilter und Auswahlliste fuer depselect');
INSERT INTO `list_reiter` VALUES (000046,'view_keyUsed','View',0,0,'Auswertung verwendeter Schluessel in man. und auto. Installationen');
INSERT INTO `list_reiter` VALUES (000047,'view_sendMail','View',1,0,'Zeige alle Lizenzen mit aktiviertem Mailversand');
INSERT INTO `list_reiter` VALUES (000415,'sort_inventar','Programmierung',0,0,'Sortierung fuer widget:depselect');
INSERT INTO `list_reiter` VALUES (000416,'view_favorit','View',0,0,'Menueeintrag in Favorit fuer schnelleren Zugriff');
INSERT INTO `list_reiter` VALUES (000417,'view_licVer','Programmierung',0,0,'summiere versionierte Lizenzen gruppiert nach lizenzID');
INSERT INTO `list_reiter` VALUES (000418,'view_invConSoft','Programmierung',0,0,'Liste aller gefunden Inventarsoftware');
INSERT INTO `list_reiter` VALUES (000419,'view_reiter','Programmierung',0,0,'Hole alle Tabellen von Datenbank von mysql');
INSERT INTO `list_reiter` VALUES (000420,'mpi_geraete','Liste',0,0,'Interne PC-DB, wenn keine externe DB existiert (mpg-version)');
INSERT INTO `list_reiter` VALUES (000421,'view_software','Auswertung',1,0,'Zeige alle vorhandenen und verbrauchten Lizenzen');
INSERT INTO `list_reiter` VALUES (000423,'view_verLic','Programmierung',0,0,'Sammelcontainer Lizenzen pro Version');
INSERT INTO `list_reiter` VALUES (000424,'view_verSumLic','Programmierung',0,0,'Sammelcontainer summierte Lizenzen');
INSERT INTO `list_reiter` VALUES (000425,'view_verVerb','Programmierung',0,0,'Sammelcontainer Verbrauch pro Version');
INSERT INTO `list_reiter` VALUES (000426,'show_version','Programmierung',0,0,'Prozedur Berechnung versionierter Lizenzen');
INSERT INTO `list_reiter` VALUES (000427,'list_ordnung','Liste',0,0,'Sortierreihenfolge Softwareversion entsprechend Downgraderecht');
INSERT INTO `list_reiter` VALUES (000428,'mpi_licSrv','Liste',1,0,'Verwaltung Lizenzserver');
INSERT INTO `list_reiter` VALUES (000429,'list_inventar','Liste',0,0,'Import SAP Inventar');
INSERT INTO `list_reiter` VALUES (000430,'view_inventar','Auswertung',1,0,'Zeige alle Inventarnummern und deren Verlinkung zur Lizenztabelle');
INSERT INTO `list_reiter` VALUES (000431,'view_groupPSE','Auswertung',1,0,'Zeige alle Lizenzen der Gruppe PSE');
INSERT INTO `list_reiter` VALUES (000432,'view_inv_geraete','View',0,0,'Beziehung Bestellnummer zur DB Inventar');
INSERT INTO `list_reiter` VALUES (000433,'sys_user','Autorisierung',1,1,'Autorisierung und Berechtigung Benutzer');
INSERT INTO `list_reiter` VALUES (000434,'list_role','Autorisierung',1,0,'Liste aller Berechtigungen (Rollen)');
/*!40000 ALTER TABLE `list_reiter` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_role` (
  `rolID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`rolID`),
  UNIQUE KEY `role` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_role` WRITE;
/*!40000 ALTER TABLE `list_role` DISABLE KEYS */;
INSERT INTO `list_role` VALUES (000001,'NO ACCESS','No_Access');
INSERT INTO `list_role` VALUES (000002,'READ ONLY','view, list, calendar, view xml, show all, find, navigate');
INSERT INTO `list_role` VALUES (000003,'EDIT','READ_ONLY and edit, new record, remove, import, translate, copy');
INSERT INTO `list_role` VALUES (000004,'DELETE','EDIT and delete and delete found');
INSERT INTO `list_role` VALUES (000005,'OWNER','DELETE except navigate, new, and delete found');
INSERT INTO `list_role` VALUES (000006,'REVIEWER','READ_ONLY and edit and translate');
INSERT INTO `list_role` VALUES (000007,'USER','READ_ONLY and add new related record');
INSERT INTO `list_role` VALUES (000008,'ADMIN','DELETE and xml_view');
INSERT INTO `list_role` VALUES (000009,'MANAGER','ADMIN and manage, manage_migrate, manage_build_index, and install');
/*!40000 ALTER TABLE `list_role` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_softart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_softart` (
  `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `softart` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `softart` (`softart`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_softart` WRITE;
/*!40000 ALTER TABLE `list_softart` DISABLE KEYS */;
/*!40000 ALTER TABLE `list_softart` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_vorgang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_vorgang` (
  `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `vorgang` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `vorgang` (`vorgang`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_vorgang` WRITE;
/*!40000 ALTER TABLE `list_vorgang` DISABLE KEYS */;
INSERT INTO `list_vorgang` VALUES (9,'Aktivierung');
INSERT INTO `list_vorgang` VALUES (1,'Angebot');
INSERT INTO `list_vorgang` VALUES (2,'Bestellschein');
INSERT INTO `list_vorgang` VALUES (10,'Emailverkehr');
INSERT INTO `list_vorgang` VALUES (15,'Installation');
INSERT INTO `list_vorgang` VALUES (14,'Kosten');
INSERT INTO `list_vorgang` VALUES (3,'Lieferschein');
INSERT INTO `list_vorgang` VALUES (4,'Lizenz');
INSERT INTO `list_vorgang` VALUES (13,'Lizenzierung');
INSERT INTO `list_vorgang` VALUES (12,'Meldung');
INSERT INTO `list_vorgang` VALUES (5,'Rechnung');
INSERT INTO `list_vorgang` VALUES (11,'Registrierung');
INSERT INTO `list_vorgang` VALUES (6,'Software');
INSERT INTO `list_vorgang` VALUES (16,'Update');
INSERT INTO `list_vorgang` VALUES (7,'Vereinbarung');
INSERT INTO `list_vorgang` VALUES (8,'Vertrag');
/*!40000 ALTER TABLE `list_vorgang` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_ablage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_ablage` (
  `ablageID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `softID` smallint(6) unsigned zerofill DEFAULT NULL,
  `vertragID` smallint(6) unsigned zerofill DEFAULT NULL,
  `lizenzID` smallint(6) unsigned zerofill DEFAULT NULL,
  `srvID` smallint(6) unsigned zerofill DEFAULT NULL,
  `vorgang` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `file` longblob,
  `file_mimetype` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_filename` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notiz` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ablageID`),
  KEY `vorgang` (`vorgang`),
  KEY `softID` (`softID`),
  KEY `vertragID` (`vertragID`),
  KEY `lizenzID` (`lizenzID`),
  KEY `srvID` (`srvID`),
  CONSTRAINT `mpi_ablage_ibfk_10` FOREIGN KEY (`lizenzID`) REFERENCES `mpi_lizenz` (`lizenzID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_ablage_ibfk_11` FOREIGN KEY (`vertragID`) REFERENCES `mpi_vertrag` (`vertragID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_ablage_ibfk_12` FOREIGN KEY (`softID`) REFERENCES `mpi_software` (`softID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_ablage_ibfk_13` FOREIGN KEY (`srvID`) REFERENCES `mpidb_it_licman`.`mpi_licSrv` (`srvID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_ablage_ibfk_2` FOREIGN KEY (`vorgang`) REFERENCES `list_vorgang` (`vorgang`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_ablage_ibfk_6` FOREIGN KEY (`lizenzID`) REFERENCES `mpi_lizenz` (`lizenzID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_ablage_ibfk_7` FOREIGN KEY (`vertragID`) REFERENCES `mpi_vertrag` (`vertragID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_ablage_ibfk_8` FOREIGN KEY (`softID`) REFERENCES `mpi_software` (`softID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_ablage_ibfk_9` FOREIGN KEY (`vorgang`) REFERENCES `list_vorgang` (`vorgang`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_ablage` WRITE;
/*!40000 ALTER TABLE `mpi_ablage` DISABLE KEYS */;
/*!40000 ALTER TABLE `mpi_ablage` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_geraete`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_geraete` (
  `tabID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `art` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`tabID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='mpg-version anstatt it_inv db';
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_geraete` WRITE;
/*!40000 ALTER TABLE `mpi_geraete` DISABLE KEYS */;
INSERT INTO `mpi_geraete` VALUES (000001,'pc100','Computer');
/*!40000 ALTER TABLE `mpi_geraete` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_install`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_install` (
  `installID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `instID` smallint(6) unsigned zerofill NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `anzahl` smallint(6) unsigned NOT NULL DEFAULT '1',
  `softID` smallint(6) unsigned zerofill NOT NULL,
  `verID` smallint(6) unsigned zerofill DEFAULT NULL,
  `lizenzID` smallint(6) unsigned zerofill DEFAULT NULL,
  `keyID` smallint(6) unsigned zerofill DEFAULT NULL,
  `nutzer` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notiz` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`installID`),
  KEY `lizenzID` (`lizenzID`),
  KEY `nutzer` (`nutzer`),
  KEY `keyID` (`keyID`),
  KEY `verID` (`verID`),
  KEY `softID` (`softID`),
  CONSTRAINT `mpi_install_ibfk_10` FOREIGN KEY (`softID`) REFERENCES `mpi_software` (`softID`) ON DELETE CASCADE,
  CONSTRAINT `mpi_install_ibfk_11` FOREIGN KEY (`softID`) REFERENCES `mpi_software` (`softID`) ON DELETE CASCADE,
  CONSTRAINT `mpi_install_ibfk_12` FOREIGN KEY (`nutzer`) REFERENCES `list_nutzer` (`nutzer`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_install_ibfk_13` FOREIGN KEY (`lizenzID`) REFERENCES `mpi_lizenz` (`lizenzID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_install_ibfk_14` FOREIGN KEY (`keyID`) REFERENCES `mpi_key` (`keyID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_install_ibfk_15` FOREIGN KEY (`verID`) REFERENCES `mpi_version` (`verID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_install_ibfk_6` FOREIGN KEY (`nutzer`) REFERENCES `list_nutzer` (`nutzer`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_install_ibfk_7` FOREIGN KEY (`lizenzID`) REFERENCES `mpi_lizenz` (`lizenzID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_install_ibfk_8` FOREIGN KEY (`keyID`) REFERENCES `mpi_key` (`keyID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_install_ibfk_9` FOREIGN KEY (`verID`) REFERENCES `mpi_version` (`verID`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_install` WRITE;
/*!40000 ALTER TABLE `mpi_install` DISABLE KEYS */;
INSERT INTO `mpi_install` VALUES (000001,000001,1,1,000001,000001,000001,NULL,NULL,NULL,'admin','2015-05-05 07:01:40');
/*!40000 ALTER TABLE `mpi_install` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `set_ins_ins` AFTER INSERT ON `mpi_install`
 FOR EACH ROW BEGIN
  INSERT IGNORE INTO list_pcname SET instID = NEW.instID, instName = (SELECT name FROM view_pc WHERE tabID = NEW.instID);
  IF NEW.verID IS NOT NULL AND NEW.lizenzID IS NOT NULL THEN CALL proc_version(NEW.verID); END IF;
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `set_ins_upd` AFTER UPDATE ON `mpi_install`
 FOR EACH ROW BEGIN
  INSERT IGNORE INTO list_pcname SET instID = NEW.instID, instName = (SELECT name FROM view_pc WHERE tabID = NEW.instID);
  UPDATE list_pcname SET instName = (SELECT name FROM view_pc WHERE tabID = NEW.instID) WHERE instID = NEW.instID;
  IF NEW.verID IS NOT NULL AND NEW.lizenzID IS NOT NULL THEN CALL proc_version(NEW.verID); END IF;
  IF OLD.verID IS NOT NULL AND OLD.lizenzID IS NOT NULL THEN CALL proc_version(OLD.verID); END IF;
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `set_ins_del` AFTER DELETE ON `mpi_install`
 FOR EACH ROW BEGIN
  IF OLD.verID IS NOT NULL AND OLD.lizenzID IS NOT NULL THEN CALL proc_version(OLD.verID); END IF;
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
DROP TABLE IF EXISTS `mpi_inventar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_inventar` (
  `software` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `publisher` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `instDate` datetime DEFAULT NULL,
  `instPath` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `conID` smallint(6) unsigned zerofill DEFAULT NULL,
  `softID` smallint(6) unsigned zerofill DEFAULT NULL,
  `verID` smallint(6) unsigned zerofill DEFAULT NULL,
  `lizenzID` smallint(6) unsigned zerofill DEFAULT NULL,
  `keyID` smallint(6) unsigned zerofill DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`software`,`version`,`instName`),
  KEY `softID` (`softID`),
  KEY `verID` (`verID`),
  KEY `lizenzID` (`lizenzID`),
  KEY `keyID` (`keyID`),
  KEY `conID` (`conID`),
  CONSTRAINT `mpi_inventar_ibfk_1` FOREIGN KEY (`conID`) REFERENCES `con_softInv` (`conID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_inventar_ibfk_2` FOREIGN KEY (`conID`) REFERENCES `con_softInv` (`conID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_inventar_ibfk_3` FOREIGN KEY (`keyID`) REFERENCES `mpi_key` (`keyID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_inventar_ibfk_4` FOREIGN KEY (`lizenzID`) REFERENCES `mpi_lizenz` (`lizenzID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_inventar_ibfk_5` FOREIGN KEY (`softID`) REFERENCES `mpi_software` (`softID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_inventar_ibfk_6` FOREIGN KEY (`verID`) REFERENCES `mpi_version` (`verID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_inventar_keyID` FOREIGN KEY (`keyID`) REFERENCES `mpi_key` (`keyID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_inventar_lizenzID` FOREIGN KEY (`lizenzID`) REFERENCES `mpi_lizenz` (`lizenzID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_inventar_softID` FOREIGN KEY (`softID`) REFERENCES `mpi_software` (`softID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_inventar_verID` FOREIGN KEY (`verID`) REFERENCES `mpi_version` (`verID`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_inventar` WRITE;
/*!40000 ALTER TABLE `mpi_inventar` DISABLE KEYS */;
/*!40000 ALTER TABLE `mpi_inventar` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_key`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_key` (
  `keyID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `lizenzID` smallint(6) unsigned zerofill DEFAULT NULL,
  `key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `softart` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `multikey` tinyint(1) NOT NULL DEFAULT '0',
  `lagerort` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notiz` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`keyID`),
  UNIQUE KEY `key_lizenzID` (`key`,`lizenzID`),
  KEY `lizenzID` (`lizenzID`),
  KEY `lagerort` (`lagerort`),
  KEY `kategorie` (`softart`),
  CONSTRAINT `mpi_key_ibfk_10` FOREIGN KEY (`lizenzID`) REFERENCES `mpi_lizenz` (`lizenzID`),
  CONSTRAINT `mpi_key_ibfk_11` FOREIGN KEY (`lizenzID`) REFERENCES `mpi_lizenz` (`lizenzID`) ON DELETE CASCADE,
  CONSTRAINT `mpi_key_ibfk_12` FOREIGN KEY (`softart`) REFERENCES `list_softart` (`softart`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mpi_key_ibfk_13` FOREIGN KEY (`softart`) REFERENCES `list_softart` (`softart`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mpi_key_ibfk_2` FOREIGN KEY (`lagerort`) REFERENCES `list_lagerort` (`lagerort`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mpi_key_ibfk_3` FOREIGN KEY (`softart`) REFERENCES `list_softart` (`softart`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_key_ibfk_4` FOREIGN KEY (`lizenzID`) REFERENCES `mpi_lizenz` (`lizenzID`),
  CONSTRAINT `mpi_key_ibfk_5` FOREIGN KEY (`lizenzID`) REFERENCES `mpi_lizenz` (`lizenzID`) ON DELETE CASCADE,
  CONSTRAINT `mpi_key_ibfk_6` FOREIGN KEY (`softart`) REFERENCES `list_softart` (`softart`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mpi_key_ibfk_7` FOREIGN KEY (`softart`) REFERENCES `list_softart` (`softart`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mpi_key_ibfk_8` FOREIGN KEY (`lagerort`) REFERENCES `list_lagerort` (`lagerort`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mpi_key_ibfk_9` FOREIGN KEY (`softart`) REFERENCES `list_softart` (`softart`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_key` WRITE;
/*!40000 ALTER TABLE `mpi_key` DISABLE KEYS */;
/*!40000 ALTER TABLE `mpi_key` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_kosten`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_kosten` (
  `kostenID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `ablageID` smallint(6) unsigned zerofill NOT NULL,
  `buchung` year(4) NOT NULL,
  `kostenstelle` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `waehrung` enum('Euro','Dollar','Pfund') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Euro',
  `miete` decimal(8,2) DEFAULT NULL,
  `wartung` decimal(8,2) DEFAULT NULL,
  `notiz` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`kostenID`),
  KEY `ablageID` (`ablageID`),
  KEY `kostenstelle` (`kostenstelle`),
  CONSTRAINT `mpi_kosten_ibfk_1` FOREIGN KEY (`ablageID`) REFERENCES `mpi_ablage` (`ablageID`) ON DELETE CASCADE,
  CONSTRAINT `mpi_kosten_ibfk_2` FOREIGN KEY (`kostenstelle`) REFERENCES `list_kostenstelle` (`kostenstelle`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mpi_kosten_ibfk_3` FOREIGN KEY (`ablageID`) REFERENCES `mpi_ablage` (`ablageID`) ON DELETE CASCADE,
  CONSTRAINT `mpi_kosten_ibfk_4` FOREIGN KEY (`kostenstelle`) REFERENCES `list_kostenstelle` (`kostenstelle`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_kosten` WRITE;
/*!40000 ALTER TABLE `mpi_kosten` DISABLE KEYS */;
/*!40000 ALTER TABLE `mpi_kosten` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_licSrv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_licSrv` (
  `srvID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `server` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zugriff` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hardware` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `program` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `licFile` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `runCmd` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `port` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `beschreibung` text COLLATE utf8_unicode_ci,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`srvID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_licSrv` WRITE;
/*!40000 ALTER TABLE `mpi_licSrv` DISABLE KEYS */;
/*!40000 ALTER TABLE `mpi_licSrv` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_links` (
  `linksID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `softID` smallint(6) unsigned zerofill DEFAULT NULL,
  `srvID` smallint(6) unsigned zerofill DEFAULT NULL,
  `typ` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passwort` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hotline` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notiz` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`linksID`),
  KEY `softID` (`softID`),
  KEY `typ` (`typ`),
  KEY `srvID` (`srvID`),
  CONSTRAINT `mpi_links_ibfk_2` FOREIGN KEY (`typ`) REFERENCES `list_logintyp` (`typ`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_links_ibfk_4` FOREIGN KEY (`typ`) REFERENCES `list_logintyp` (`typ`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_links_ibfk_5` FOREIGN KEY (`softID`) REFERENCES `mpi_software` (`softID`) ON DELETE CASCADE,
  CONSTRAINT `mpi_links_ibfk_6` FOREIGN KEY (`softID`) REFERENCES `mpi_software` (`softID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_links_ibfk_7` FOREIGN KEY (`srvID`) REFERENCES `mpi_licSrv` (`srvID`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_links` WRITE;
/*!40000 ALTER TABLE `mpi_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `mpi_links` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_lizenz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_lizenz` (
  `lizenzID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `softID` smallint(6) unsigned zerofill NOT NULL,
  `verID` smallint(6) unsigned zerofill DEFAULT NULL,
  `vertragID` smallint(6) unsigned zerofill DEFAULT NULL,
  `modell` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `bezeichnung` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inventar` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unterNr` varchar(4) COLLATE utf8_unicode_ci DEFAULT '0',
  `anzahl` smallint(4) NOT NULL,
  `version` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `os` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arch` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expLizenz` date DEFAULT NULL,
  `nachricht` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `support` tinyint(1) NOT NULL DEFAULT '0',
  `expSupport` date DEFAULT NULL,
  `update` tinyint(1) NOT NULL DEFAULT '0',
  `privateUse` tinyint(1) DEFAULT NULL,
  `nutzer` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ansprech` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `srvID` smallint(6) unsigned zerofill DEFAULT NULL,
  `lieferant` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bestellnummer` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `artikelnummer` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kostenstelle` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ePreis` decimal(8,2) DEFAULT NULL,
  `bemerkung` tinytext COLLATE utf8_unicode_ci,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`lizenzID`),
  KEY `lieferant` (`lieferant`),
  KEY `modell` (`modell`),
  KEY `nutzer` (`nutzer`),
  KEY `snameID` (`softID`),
  KEY `ansprech` (`ansprech`),
  KEY `vertragID` (`vertragID`),
  KEY `verID` (`verID`),
  KEY `srvID` (`srvID`),
  KEY `kostenstelle` (`kostenstelle`),
  CONSTRAINT `lizenz_kosten_kostenstelle` FOREIGN KEY (`kostenstelle`) REFERENCES `list_kostenstelle` (`kostenstelle`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mpi_lizenz_ibfk_1` FOREIGN KEY (`lieferant`) REFERENCES `list_lieferant` (`lieferant`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_lizenz_ibfk_10` FOREIGN KEY (`verID`) REFERENCES `mpi_version` (`verID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_lizenz_ibfk_11` FOREIGN KEY (`verID`) REFERENCES `mpi_version` (`verID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_lizenz_ibfk_12` FOREIGN KEY (`lieferant`) REFERENCES `list_lieferant` (`lieferant`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_lizenz_ibfk_13` FOREIGN KEY (`modell`) REFERENCES `list_modell` (`modell`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_lizenz_ibfk_14` FOREIGN KEY (`nutzer`) REFERENCES `list_nutzer` (`nutzer`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_lizenz_ibfk_15` FOREIGN KEY (`softID`) REFERENCES `mpi_software` (`softID`),
  CONSTRAINT `mpi_lizenz_ibfk_16` FOREIGN KEY (`ansprech`) REFERENCES `list_nutzer` (`nutzer`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_lizenz_ibfk_17` FOREIGN KEY (`vertragID`) REFERENCES `mpi_vertrag` (`vertragID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_lizenz_ibfk_18` FOREIGN KEY (`srvID`) REFERENCES `mpi_licSrv` (`srvID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_lizenz_ibfk_2` FOREIGN KEY (`modell`) REFERENCES `list_modell` (`modell`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_lizenz_ibfk_3` FOREIGN KEY (`nutzer`) REFERENCES `list_nutzer` (`nutzer`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_lizenz_ibfk_7` FOREIGN KEY (`softID`) REFERENCES `mpi_software` (`softID`),
  CONSTRAINT `mpi_lizenz_ibfk_8` FOREIGN KEY (`ansprech`) REFERENCES `list_nutzer` (`nutzer`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_lizenz_ibfk_9` FOREIGN KEY (`vertragID`) REFERENCES `mpi_vertrag` (`vertragID`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_lizenz` WRITE;
/*!40000 ALTER TABLE `mpi_lizenz` DISABLE KEYS */;
INSERT INTO `mpi_lizenz` VALUES (000001,000001,000001,NULL,'Registrierung',1,'Adobe Reader : 10 : Registrierung : 0',NULL,'0',0,'10','Win','32\n64',NULL,0,NULL,0,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'test','admin','2015-05-05 07:00:44');
/*!40000 ALTER TABLE `mpi_lizenz` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `set_lic_ins` AFTER INSERT ON `mpi_lizenz`
 FOR EACH ROW BEGIN
  IF NEW.verID IS NOT NULL THEN CALL proc_version(NEW.verID); END IF;
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `set_lic_upd` AFTER UPDATE ON `mpi_lizenz`
 FOR EACH ROW BEGIN
  IF OLD.verID IS NOT NULL THEN CALL proc_version(OLD.verID); END IF;
  IF NEW.verID IS NOT NULL THEN CALL proc_version(NEW.verID); END IF;
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `set_lic_del` AFTER DELETE ON `mpi_lizenz`
 FOR EACH ROW BEGIN
  IF OLD.verID IS NOT NULL THEN CALL proc_version(OLD.verID); END IF;
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
DROP TABLE IF EXISTS `mpi_notiz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_notiz` (
  `autoID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `softID` smallint(6) unsigned zerofill DEFAULT NULL,
  `lizenzID` smallint(6) unsigned zerofill DEFAULT NULL,
  `stichwort` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `notiz` text COLLATE utf8_unicode_ci NOT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`autoID`),
  KEY `tabID` (`softID`),
  KEY `lizenzID` (`lizenzID`),
  CONSTRAINT `mpi_notiz_ibfk_4` FOREIGN KEY (`lizenzID`) REFERENCES `mpi_lizenz` (`lizenzID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_notiz_ibfk_5` FOREIGN KEY (`softID`) REFERENCES `mpi_software` (`softID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_notiz_ibfk_6` FOREIGN KEY (`lizenzID`) REFERENCES `mpi_lizenz` (`lizenzID`) ON DELETE SET NULL,
  CONSTRAINT `mpi_notiz_ibfk_7` FOREIGN KEY (`softID`) REFERENCES `mpi_software` (`softID`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_notiz` WRITE;
/*!40000 ALTER TABLE `mpi_notiz` DISABLE KEYS */;
/*!40000 ALTER TABLE `mpi_notiz` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_software`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_software` (
  `softID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `software` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `kategorie` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nutzrecht` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `hersteller` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quelle` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nutzer` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ansprech` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bemerkung` tinytext COLLATE utf8_unicode_ci,
  `image` longblob,
  `image_mimetype` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_filename` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`softID`),
  UNIQUE KEY `software` (`software`),
  KEY `hersteller` (`hersteller`),
  KEY `kategorie` (`kategorie`),
  KEY `nutzrecht` (`nutzrecht`),
  KEY `quelle` (`quelle`),
  KEY `nutzer` (`nutzer`),
  KEY `ansprech` (`ansprech`),
  CONSTRAINT `mpi_software_ibfk_1` FOREIGN KEY (`quelle`) REFERENCES `list_quelle` (`quelle`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_software_ibfk_10` FOREIGN KEY (`hersteller`) REFERENCES `list_hersteller` (`hersteller`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_software_ibfk_11` FOREIGN KEY (`nutzrecht`) REFERENCES `list_nutzrecht` (`nutzrecht`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_software_ibfk_12` FOREIGN KEY (`nutzer`) REFERENCES `list_nutzer` (`nutzer`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_software_ibfk_13` FOREIGN KEY (`ansprech`) REFERENCES `list_nutzer` (`nutzer`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_software_ibfk_2` FOREIGN KEY (`kategorie`) REFERENCES `list_kategorie` (`kategorie`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_software_ibfk_3` FOREIGN KEY (`hersteller`) REFERENCES `list_hersteller` (`hersteller`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_software_ibfk_4` FOREIGN KEY (`nutzrecht`) REFERENCES `list_nutzrecht` (`nutzrecht`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_software_ibfk_6` FOREIGN KEY (`nutzer`) REFERENCES `list_nutzer` (`nutzer`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_software_ibfk_7` FOREIGN KEY (`ansprech`) REFERENCES `list_nutzer` (`nutzer`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_software_ibfk_8` FOREIGN KEY (`quelle`) REFERENCES `list_quelle` (`quelle`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_software_ibfk_9` FOREIGN KEY (`kategorie`) REFERENCES `list_kategorie` (`kategorie`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_software` WRITE;
/*!40000 ALTER TABLE `mpi_software` DISABLE KEYS */;
INSERT INTO `mpi_software` VALUES (000001,'Adobe Reader',NULL,'Registrierung','Adobe Systems Inc.',NULL,NULL,NULL,'test',NULL,NULL,NULL,'admin','2015-05-05 06:59:46');
/*!40000 ALTER TABLE `mpi_software` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_version` (
  `verID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `softID` smallint(6) unsigned zerofill NOT NULL,
  `version` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `orderID` tinyint(3) unsigned zerofill NOT NULL DEFAULT '001',
  `notiz` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`verID`),
  UNIQUE KEY `softID_orderID` (`softID`,`orderID`),
  KEY `softID` (`softID`),
  KEY `orderID` (`orderID`),
  CONSTRAINT `mpi_version_ibfk_3` FOREIGN KEY (`softID`) REFERENCES `mpi_software` (`softID`) ON DELETE CASCADE,
  CONSTRAINT `mpi_version_ibfk_4` FOREIGN KEY (`softID`) REFERENCES `mpi_software` (`softID`) ON DELETE CASCADE,
  CONSTRAINT `mpi_version_ibfk_5` FOREIGN KEY (`orderID`) REFERENCES `list_ordnung` (`orderID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_version` WRITE;
/*!40000 ALTER TABLE `mpi_version` DISABLE KEYS */;
INSERT INTO `mpi_version` VALUES (000001,000001,'10',001,'test','admin','2015-05-05 06:59:46');
INSERT INTO `mpi_version` VALUES (000002,000001,'11',099,'test','admin','2015-05-05 06:59:46');
/*!40000 ALTER TABLE `mpi_version` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_vertrag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_vertrag` (
  `vertragID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `vertrag` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `hersteller` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lieferant` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `create` date DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `setExpLizenz` tinyint(1) NOT NULL DEFAULT '0',
  `nachricht` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notiz` tinytext COLLATE utf8_unicode_ci,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`vertragID`),
  UNIQUE KEY `vertrag` (`vertrag`),
  KEY `hersteller` (`hersteller`),
  KEY `lieferant` (`lieferant`),
  CONSTRAINT `mpi_vertrag_ibfk_1` FOREIGN KEY (`hersteller`) REFERENCES `list_hersteller` (`hersteller`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_vertrag_ibfk_2` FOREIGN KEY (`lieferant`) REFERENCES `list_lieferant` (`lieferant`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_vertrag_ibfk_3` FOREIGN KEY (`hersteller`) REFERENCES `list_hersteller` (`hersteller`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_vertrag_ibfk_4` FOREIGN KEY (`lieferant`) REFERENCES `list_lieferant` (`lieferant`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_vertrag` WRITE;
/*!40000 ALTER TABLE `mpi_vertrag` DISABLE KEYS */;
INSERT INTO `mpi_vertrag` VALUES (000001,'1234567','Adobe Systems Inc.','Adobe',1,NULL,NULL,0,0,NULL,NULL,'admin','2015-05-27 13:40:53');
/*!40000 ALTER TABLE `mpi_vertrag` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `show_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `show_version` (
  `verID` smallint(6) unsigned zerofill NOT NULL,
  `diffVersion` smallint(6) NOT NULL DEFAULT '0',
  `restVersion` smallint(6) NOT NULL DEFAULT '0',
  `verbVersion` smallint(6) NOT NULL DEFAULT '0',
  `freeVersion` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`verID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `show_version` WRITE;
/*!40000 ALTER TABLE `show_version` DISABLE KEYS */;
/*!40000 ALTER TABLE `show_version` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `sort_inventar`;
/*!50001 DROP VIEW IF EXISTS `sort_inventar`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `sort_inventar` (
  `software` tinyint NOT NULL,
  `version` tinyint NOT NULL,
  `publisher` tinyint NOT NULL,
  `anzahl` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `sort_lizenzSoft`;
/*!50001 DROP VIEW IF EXISTS `sort_lizenzSoft`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `sort_lizenzSoft` (
  `lizenzID` tinyint NOT NULL,
  `softID` tinyint NOT NULL,
  `verID` tinyint NOT NULL,
  `bezeichnung` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user` (
  `logID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `login` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`logID`) USING BTREE,
  UNIQUE KEY `login` (`login`) USING BTREE,
  UNIQUE KEY `email` (`email`),
  KEY `role` (`role`),
  CONSTRAINT `sysUser_listRole` FOREIGN KEY (`role`) REFERENCES `list_role` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (000001,'admin','d033e22ae348aeb5660fc2140aec35850c4da997','MANAGER','','import','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `view_favorit`;
/*!50001 DROP VIEW IF EXISTS `view_favorit`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_favorit` (
  `autoID` tinyint NOT NULL,
  `reiter` tinyint NOT NULL,
  `kategorie` tinyint NOT NULL,
  `favorit` tinyint NOT NULL,
  `history` tinyint NOT NULL,
  `bedeutung` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_groupPSE`;
/*!50001 DROP VIEW IF EXISTS `view_groupPSE`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_groupPSE` (
  `lizenzID` tinyint NOT NULL,
  `softID` tinyint NOT NULL,
  `verID` tinyint NOT NULL,
  `vertragID` tinyint NOT NULL,
  `modell` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `bezeichnung` tinyint NOT NULL,
  `inventar` tinyint NOT NULL,
  `unterNr` tinyint NOT NULL,
  `anzahl` tinyint NOT NULL,
  `version` tinyint NOT NULL,
  `os` tinyint NOT NULL,
  `arch` tinyint NOT NULL,
  `expLizenz` tinyint NOT NULL,
  `nachricht` tinyint NOT NULL,
  `email` tinyint NOT NULL,
  `support` tinyint NOT NULL,
  `expSupport` tinyint NOT NULL,
  `update` tinyint NOT NULL,
  `privateUse` tinyint NOT NULL,
  `nutzer` tinyint NOT NULL,
  `ansprech` tinyint NOT NULL,
  `srvID` tinyint NOT NULL,
  `lieferant` tinyint NOT NULL,
  `bestellnummer` tinyint NOT NULL,
  `artikelnummer` tinyint NOT NULL,
  `kostenstelle` tinyint NOT NULL,
  `ePreis` tinyint NOT NULL,
  `bemerkung` tinyint NOT NULL,
  `bearbeiter` tinyint NOT NULL,
  `zeitstempel` tinyint NOT NULL,
  `expiry` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_instLic`;
/*!50001 DROP VIEW IF EXISTS `view_instLic`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_instLic` (
  `tabID` tinyint NOT NULL,
  `instName` tinyint NOT NULL,
  `softID` tinyint NOT NULL,
  `verID` tinyint NOT NULL,
  `lizenzID` tinyint NOT NULL,
  `inventar` tinyint NOT NULL,
  `useLizenz` tinyint NOT NULL,
  `anzTreffer` tinyint NOT NULL,
  `anzLizenz` tinyint NOT NULL,
  `anzDubli` tinyint NOT NULL,
  `gPreis` tinyint NOT NULL,
  `modell` tinyint NOT NULL,
  `bearbeiter` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_install`;
/*!50001 DROP VIEW IF EXISTS `view_install`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_install` (
  `tabID` tinyint NOT NULL,
  `instName` tinyint NOT NULL,
  `softID` tinyint NOT NULL,
  `verID` tinyint NOT NULL,
  `lizenzID` tinyint NOT NULL,
  `inventar` tinyint NOT NULL,
  `useLizenz` tinyint NOT NULL,
  `anzTreffer` tinyint NOT NULL,
  `bearbeiter` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_invCon`;
/*!50001 DROP VIEW IF EXISTS `view_invCon`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_invCon` (
  `software` tinyint NOT NULL,
  `version` tinyint NOT NULL,
  `publisher` tinyint NOT NULL,
  `instName` tinyint NOT NULL,
  `instDate` tinyint NOT NULL,
  `instPath` tinyint NOT NULL,
  `conID` tinyint NOT NULL,
  `softID` tinyint NOT NULL,
  `verID` tinyint NOT NULL,
  `lizenzID` tinyint NOT NULL,
  `keyID` tinyint NOT NULL,
  `bearbeiter` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_invConSoft`;
/*!50001 DROP VIEW IF EXISTS `view_invConSoft`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_invConSoft` (
  `conID` tinyint NOT NULL,
  `software` tinyint NOT NULL,
  `version` tinyint NOT NULL,
  `publisher` tinyint NOT NULL,
  `anzahl` tinyint NOT NULL,
  `bearbeiter` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_invInst`;
/*!50001 DROP VIEW IF EXISTS `view_invInst`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_invInst` (
  `tabID` tinyint NOT NULL,
  `instID` tinyint NOT NULL,
  `instName` tinyint NOT NULL,
  `softID` tinyint NOT NULL,
  `verID` tinyint NOT NULL,
  `lizenzID` tinyint NOT NULL,
  `keyID` tinyint NOT NULL,
  `inventar` tinyint NOT NULL,
  `useLizenz` tinyint NOT NULL,
  `anzTreffer` tinyint NOT NULL,
  `bearbeiter` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_inv_geraete`;
/*!50001 DROP VIEW IF EXISTS `view_inv_geraete`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_inv_geraete` (
  `gerID` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `os` tinyint NOT NULL,
  `lagerort` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `bestellnummer` tinyint NOT NULL,
  `bearbeiter` tinyint NOT NULL,
  `zeitstempel` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_inventar`;
/*!50001 DROP VIEW IF EXISTS `view_inventar`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_inventar` (
  `anlage` tinyint NOT NULL,
  `unterNr` tinyint NOT NULL,
  `invNummer` tinyint NOT NULL,
  `bezeichnung` tinyint NOT NULL,
  `standort` tinyint NOT NULL,
  `wert` tinyint NOT NULL,
  `invDatum` tinyint NOT NULL,
  `kostenstelle` tinyint NOT NULL,
  `notiz` tinyint NOT NULL,
  `bearbeiter` tinyint NOT NULL,
  `zeitstempel` tinyint NOT NULL,
  `inventar` tinyint NOT NULL,
  `lizenzID` tinyint NOT NULL,
  `exist` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_keyUsed`;
/*!50001 DROP VIEW IF EXISTS `view_keyUsed`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_keyUsed` (
  `keyID` tinyint NOT NULL,
  `multikey` tinyint NOT NULL,
  `used` tinyint NOT NULL,
  `anz` tinyint NOT NULL,
  `freeIns` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_kosten`;
/*!50001 DROP VIEW IF EXISTS `view_kosten`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_kosten` (
  `kostenID` tinyint NOT NULL,
  `vorgang` tinyint NOT NULL,
  `software` tinyint NOT NULL,
  `vertrag` tinyint NOT NULL,
  `bezeichnung` tinyint NOT NULL,
  `buchung` tinyint NOT NULL,
  `kostenstelle` tinyint NOT NULL,
  `waehrung` tinyint NOT NULL,
  `miete` tinyint NOT NULL,
  `wartung` tinyint NOT NULL,
  `notiz` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_licVer`;
/*!50001 DROP VIEW IF EXISTS `view_licVer`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_licVer` (
  `verID` tinyint NOT NULL,
  `softID` tinyint NOT NULL,
  `lizenzID` tinyint NOT NULL,
  `anzVersion` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_lizenz`;
/*!50001 DROP VIEW IF EXISTS `view_lizenz`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_lizenz` (
  `tabID` tinyint NOT NULL,
  `softID` tinyint NOT NULL,
  `verID` tinyint NOT NULL,
  `lizenzID` tinyint NOT NULL,
  `inventar` tinyint NOT NULL,
  `anzLizenz` tinyint NOT NULL,
  `useLizenz` tinyint NOT NULL,
  `freeLizenz` tinyint NOT NULL,
  `modell` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_pc`;
/*!50001 DROP VIEW IF EXISTS `view_pc`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_pc` (
  `tabID` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `art` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_reiter`;
/*!50001 DROP VIEW IF EXISTS `view_reiter`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_reiter` (
  `reiter` tinyint NOT NULL,
  `table_type` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_sendMail`;
/*!50001 DROP VIEW IF EXISTS `view_sendMail`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_sendMail` (
  `name` tinyint NOT NULL,
  `lizenzID` tinyint NOT NULL,
  `vertragID` tinyint NOT NULL,
  `expireDate` tinyint NOT NULL,
  `anz` tinyint NOT NULL,
  `email` tinyint NOT NULL,
  `body` tinyint NOT NULL,
  `expiry` tinyint NOT NULL,
  `nachricht` tinyint NOT NULL,
  `setExpLizenz` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_software`;
/*!50001 DROP VIEW IF EXISTS `view_software`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_software` (
  `softID` tinyint NOT NULL,
  `verID` tinyint NOT NULL,
  `lizenzID` tinyint NOT NULL,
  `modell` tinyint NOT NULL,
  `anzLizenz` tinyint NOT NULL,
  `useLizenz` tinyint NOT NULL,
  `verbLizenz` tinyint NOT NULL,
  `freeLizenz` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_user`;
/*!50001 DROP VIEW IF EXISTS `view_user`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_user` (
  `userID` tinyint NOT NULL,
  `login` tinyint NOT NULL,
  `sort` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_verLic`;
/*!50001 DROP VIEW IF EXISTS `view_verLic`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_verLic` (
  `verID` tinyint NOT NULL,
  `lizenzID` tinyint NOT NULL,
  `modell` tinyint NOT NULL,
  `anzLizenz` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_verSumLic`;
/*!50001 DROP VIEW IF EXISTS `view_verSumLic`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_verSumLic` (
  `verID` tinyint NOT NULL,
  `anzLizenz` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_verVerb`;
/*!50001 DROP VIEW IF EXISTS `view_verVerb`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_verVerb` (
  `verID` tinyint NOT NULL,
  `useLizenz` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_version`;
/*!50001 DROP VIEW IF EXISTS `view_version`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_version` (
  `softID` tinyint NOT NULL,
  `verID` tinyint NOT NULL,
  `orderID` tinyint NOT NULL,
  `lizenzID` tinyint NOT NULL,
  `modell` tinyint NOT NULL,
  `anzLizenz` tinyint NOT NULL,
  `useLizenz` tinyint NOT NULL,
  `diffVersion` tinyint NOT NULL,
  `restVersion` tinyint NOT NULL,
  `verbVersion` tinyint NOT NULL,
  `freeVersion` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_vertrag`;
/*!50001 DROP VIEW IF EXISTS `view_vertrag`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_vertrag` (
  `vertragID` tinyint NOT NULL,
  `anzVertrag` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
/*!50003 DROP PROCEDURE IF EXISTS `proc_version` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_version`( IN iVerID SMALLINT(6) )
proc_label:BEGIN
  DECLARE Vt, Ot, Rt, Lt, It, St, Pt, Dt, Nt, Mt, Tt, Pg SMALLINT(6) DEFAULT '0';
  DECLARE endRow TINYINT DEFAULT '0';
  DECLARE forSoft1 CURSOR FOR
    SELECT verID, orderID, IFNULL(anzLizenz,'0'), IFNULL(useLizenz,'0') FROM view_version WHERE softID = St ORDER BY orderID ASC;
  DECLARE forSoft2 CURSOR FOR
    SELECT ver.verID, diffVersion, IFNULL(shw.restVersion,'0') FROM show_version AS shw LEFT JOIN mpi_version AS ver ON ver.verID = shw.verID WHERE ver.softID = St ORDER BY ver.orderID DESC;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET endRow = 1;
  IF iVerID IS NULL THEN LEAVE proc_label; END IF;  
  SELECT softID INTO St FROM mpi_version WHERE verID = iVerID;
  IF St IS NULL THEN LEAVE proc_label; END IF;  
  SELECT MAX(orderID) INTO Mt FROM mpi_version WHERE softID = St;

  OPEN forSoft1;
  getSoft: LOOP
  FETCH forSoft1 INTO Vt, Ot, Lt, It;
  IF endRow = 1 THEN LEAVE getSoft; END IF;
  SET Dt = (Lt - It + Nt);
  IF Dt < '0' AND Mt != Ot THEN  
    SET Pt = '0';
    SET Nt = Dt;
  ELSE
    SET Pt = Dt;
    SET Nt = '0';
  END IF;
  IF Dt < '0' THEN SET Rt = Lt; ELSE SET Rt = Lt - Dt; END IF;
  UPDATE show_version SET diffVersion = Dt, verbVersion = Rt, restVersion = Pt WHERE verID = Vt;
  END LOOP getSoft;
  CLOSE forSoft1;

  SET endRow = '0';
  OPEN forSoft2;
  getSoft: LOOP
  FETCH forSoft2 INTO Vt, Dt, Pt;
  IF endRow = 1 THEN LEAVE getSoft; END IF;
  IF Pg < 0 AND Dt < 0 THEN
    SET Tt = Pg;
  ELSE
    SET Pg = (Pg + Pt);
    SET Tt = Pg;
  END IF;
  UPDATE show_version SET freeVersion = Tt WHERE verID = Vt;
  
  END LOOP getSoft;
  CLOSE forSoft2;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

USE `mpidb_mpg_licman`;
/*!50001 DROP TABLE IF EXISTS `sort_inventar`*/;
/*!50001 DROP VIEW IF EXISTS `sort_inventar`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `sort_inventar` AS select `mpi_inventar`.`software` AS `software`,`mpi_inventar`.`version` AS `version`,`mpi_inventar`.`publisher` AS `publisher`,count(`mpi_inventar`.`software`) AS `anzahl` from `mpi_inventar` group by `mpi_inventar`.`software`,`mpi_inventar`.`version` order by `mpi_inventar`.`software` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `sort_lizenzSoft`*/;
/*!50001 DROP VIEW IF EXISTS `sort_lizenzSoft`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `sort_lizenzSoft` AS select `lic`.`lizenzID` AS `lizenzID`,`lic`.`softID` AS `softID`,ifnull(`ver`.`verID`,`lic`.`verID`) AS `verID`,`lic`.`bezeichnung` AS `bezeichnung` from ((`mpi_lizenz` `lic` left join `con_verLic` `con` on((`con`.`lizenzID` = `lic`.`lizenzID`))) left join `mpi_version` `ver` on((`ver`.`verID` = `con`.`verID`))) order by `lic`.`bezeichnung` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_favorit`*/;
/*!50001 DROP VIEW IF EXISTS `view_favorit`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_favorit` AS select `list_reiter`.`autoID` AS `autoID`,`list_reiter`.`reiter` AS `reiter`,`list_reiter`.`kategorie` AS `kategorie`,`list_reiter`.`favorit` AS `favorit`,`list_reiter`.`history` AS `history`,`list_reiter`.`bedeutung` AS `bedeutung` from `list_reiter` where (`list_reiter`.`favorit` = '1') order by `list_reiter`.`reiter` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_groupPSE`*/;
/*!50001 DROP VIEW IF EXISTS `view_groupPSE`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_groupPSE` AS select `lic`.`lizenzID` AS `lizenzID`,`lic`.`softID` AS `softID`,`lic`.`verID` AS `verID`,`lic`.`vertragID` AS `vertragID`,`lic`.`modell` AS `modell`,`lic`.`status` AS `status`,`lic`.`bezeichnung` AS `bezeichnung`,`lic`.`inventar` AS `inventar`,`lic`.`unterNr` AS `unterNr`,`lic`.`anzahl` AS `anzahl`,`lic`.`version` AS `version`,`lic`.`os` AS `os`,`lic`.`arch` AS `arch`,`lic`.`expLizenz` AS `expLizenz`,`lic`.`nachricht` AS `nachricht`,`lic`.`email` AS `email`,`lic`.`support` AS `support`,`lic`.`expSupport` AS `expSupport`,`lic`.`update` AS `update`,`lic`.`privateUse` AS `privateUse`,`lic`.`nutzer` AS `nutzer`,`lic`.`ansprech` AS `ansprech`,`lic`.`srvID` AS `srvID`,`lic`.`lieferant` AS `lieferant`,`lic`.`bestellnummer` AS `bestellnummer`,`lic`.`artikelnummer` AS `artikelnummer`,`lic`.`kostenstelle` AS `kostenstelle`,`lic`.`ePreis` AS `ePreis`,`lic`.`bemerkung` AS `bemerkung`,`lic`.`bearbeiter` AS `bearbeiter`,`lic`.`zeitstempel` AS `zeitstempel`,if(((`ver`.`status` = 1) and (`ver`.`setExpLizenz` = 1) and (`ver`.`expiry` is not null)),`ver`.`expiry`,`lic`.`expSupport`) AS `expiry` from (`mpi_lizenz` `lic` left join `mpi_vertrag` `ver` on((`lic`.`vertragID` = `ver`.`vertragID`))) where (((`lic`.`kostenstelle` = '10408') or (`lic`.`kostenstelle` = 'PSE')) and (`lic`.`status` = 1)) order by `lic`.`vertragID`,`lic`.`lizenzID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_instLic`*/;
/*!50001 DROP VIEW IF EXISTS `view_instLic`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_instLic` AS select `vins`.`tabID` AS `tabID`,`vins`.`instName` AS `instName`,`vins`.`softID` AS `softID`,`vins`.`verID` AS `verID`,`vins`.`lizenzID` AS `lizenzID`,`vins`.`inventar` AS `inventar`,`vins`.`useLizenz` AS `useLizenz`,`vins`.`anzTreffer` AS `anzTreffer`,`lic`.`anzahl` AS `anzLizenz`,(count(`lic`.`lizenzID`) - 1) AS `anzDubli`,(`lic`.`ePreis` * `lic`.`anzahl`) AS `gPreis`,`lic`.`modell` AS `modell`,`vins`.`bearbeiter` AS `bearbeiter` from (`mpi_lizenz` `lic` join `view_install` `vins` on((`lic`.`lizenzID` = `vins`.`lizenzID`))) where (`lic`.`status` = '1') group by `lic`.`lizenzID`,`vins`.`instName` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_install`*/;
/*!50001 DROP VIEW IF EXISTS `view_install`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_install` AS select `view_invInst`.`tabID` AS `tabID`,`view_invInst`.`instName` AS `instName`,`view_invInst`.`softID` AS `softID`,`view_invInst`.`verID` AS `verID`,`view_invInst`.`lizenzID` AS `lizenzID`,`view_invInst`.`inventar` AS `inventar`,`view_invInst`.`useLizenz` AS `useLizenz`,`view_invInst`.`anzTreffer` AS `anzTreffer`,`view_invInst`.`bearbeiter` AS `bearbeiter` from `view_invInst` where (`view_invInst`.`lizenzID` is not null) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_invCon`*/;
/*!50001 DROP VIEW IF EXISTS `view_invCon`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_invCon` AS select `mpi_inventar`.`software` AS `software`,`mpi_inventar`.`version` AS `version`,`mpi_inventar`.`publisher` AS `publisher`,`mpi_inventar`.`instName` AS `instName`,`mpi_inventar`.`instDate` AS `instDate`,`mpi_inventar`.`instPath` AS `instPath`,`mpi_inventar`.`conID` AS `conID`,`mpi_inventar`.`softID` AS `softID`,`mpi_inventar`.`verID` AS `verID`,`mpi_inventar`.`lizenzID` AS `lizenzID`,`mpi_inventar`.`keyID` AS `keyID`,`mpi_inventar`.`bearbeiter` AS `bearbeiter` from `mpi_inventar` where (`mpi_inventar`.`conID` is not null) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_invConSoft`*/;
/*!50001 DROP VIEW IF EXISTS `view_invConSoft`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_invConSoft` AS select `mpi_inventar`.`conID` AS `conID`,`mpi_inventar`.`software` AS `software`,`mpi_inventar`.`version` AS `version`,`mpi_inventar`.`publisher` AS `publisher`,count(`mpi_inventar`.`software`) AS `anzahl`,`mpi_inventar`.`bearbeiter` AS `bearbeiter` from `mpi_inventar` where (`mpi_inventar`.`conID` is not null) group by `mpi_inventar`.`software`,`mpi_inventar`.`version` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_invInst`*/;
/*!50001 DROP VIEW IF EXISTS `view_invInst`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_invInst` AS select `aut`.`conID` AS `tabID`,`vpc`.`tabID` AS `instID`,`aut`.`instName` AS `instName`,`aut`.`softID` AS `softID`,`aut`.`verID` AS `verID`,`aut`.`lizenzID` AS `lizenzID`,`aut`.`keyID` AS `keyID`,1 AS `inventar`,`con`.`anzahl` AS `useLizenz`,count(`aut`.`instName`) AS `anzTreffer`,`aut`.`bearbeiter` AS `bearbeiter` from ((`mpi_inventar` `aut` left join `view_pc` `vpc` on((`aut`.`instName` = `vpc`.`name`))) left join `con_softInv` `con` on((`con`.`conID` = `aut`.`conID`))) where (`aut`.`conID` is not null) group by `aut`.`conID`,`aut`.`instName`,`aut`.`softID`,`aut`.`verID`,`aut`.`lizenzID` union all select `man`.`installID` AS `tabID`,`man`.`instID` AS `instID`,`vpc`.`name` AS `instName`,`man`.`softID` AS `softID`,`man`.`verID` AS `verID`,`man`.`lizenzID` AS `lizenzID`,`man`.`keyID` AS `keyID`,0 AS `inventar`,`man`.`anzahl` AS `useLizenz`,count(`vpc`.`name`) AS `anzTreffer`,`man`.`bearbeiter` AS `bearbeiter` from (`mpi_install` `man` left join `view_pc` `vpc` on((`man`.`instID` = `vpc`.`tabID`))) where (`man`.`status` = '1') group by `man`.`installID`,`vpc`.`name`,`man`.`softID`,`man`.`verID`,`man`.`lizenzID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_inv_geraete`*/;
/*!50001 DROP VIEW IF EXISTS `view_inv_geraete`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_inv_geraete` AS select '000001' AS `gerID`,'fakeAlone' AS `name`,'none' AS `os`,'R0.0' AS `lagerort`,'none' AS `status`,'0123456789' AS `bestellnummer`,'alone' AS `bearbeiter`,'2016-07-22 12:38:40' AS `zeitstempel` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_inventar`*/;
/*!50001 DROP VIEW IF EXISTS `view_inventar`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_inventar` AS select `inv`.`anlage` AS `anlage`,`inv`.`unterNr` AS `unterNr`,`inv`.`invNummer` AS `invNummer`,`inv`.`bezeichnung` AS `bezeichnung`,`inv`.`standort` AS `standort`,`inv`.`wert` AS `wert`,`inv`.`invDatum` AS `invDatum`,`inv`.`kostenstelle` AS `kostenstelle`,`inv`.`notiz` AS `notiz`,`inv`.`bearbeiter` AS `bearbeiter`,`inv`.`zeitstempel` AS `zeitstempel`,concat(`inv`.`anlage`,':',`inv`.`unterNr`) AS `inventar`,`lic`.`lizenzID` AS `lizenzID`,if(isnull(`lic`.`lizenzID`),0,1) AS `exist` from (`list_inventar` `inv` left join `mpi_lizenz` `lic` on(((`inv`.`anlage` = `lic`.`inventar`) and (`inv`.`unterNr` = `lic`.`unterNr`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_keyUsed`*/;
/*!50001 DROP VIEW IF EXISTS `view_keyUsed`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_keyUsed` AS select `sch`.`keyID` AS `keyID`,`sch`.`multikey` AS `multikey`,(case when ((`man`.`instID` > 0) and (`aut`.`conID` > 0)) then 'AM' when (`man`.`instID` > 0) then 'M' when (`aut`.`conID` > 0) then 'A' else '-' end) AS `used`,(case when ((`man`.`instID` > 0) and (`aut`.`conID` > 0)) then (count(`man`.`keyID`) + count(`aut`.`keyID`)) when (`man`.`instID` > 0) then count(`man`.`keyID`) when (`aut`.`conID` > 0) then count(`aut`.`keyID`) else (count(`sch`.`keyID`) - 1) end) AS `anz`,if(((`sch`.`multikey` = 0) and (`man`.`instID` > 0)),0,1) AS `freeIns` from ((`mpi_key` `sch` left join `con_softInv` `aut` on((`sch`.`keyID` = `aut`.`keyID`))) left join `mpi_install` `man` on((`sch`.`keyID` = `man`.`keyID`))) group by `sch`.`keyID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_kosten`*/;
/*!50001 DROP VIEW IF EXISTS `view_kosten`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_kosten` AS select `kost`.`kostenID` AS `kostenID`,`ablg`.`vorgang` AS `vorgang`,(select `mpi_software`.`software` from `mpi_software` where (`mpi_software`.`softID` = `ablg`.`softID`)) AS `software`,(select `mpi_vertrag`.`vertrag` from `mpi_vertrag` where (`mpi_vertrag`.`vertragID` = `ablg`.`vertragID`)) AS `vertrag`,(select `mpi_lizenz`.`bezeichnung` from `mpi_lizenz` where (`mpi_lizenz`.`lizenzID` = `ablg`.`lizenzID`)) AS `bezeichnung`,`kost`.`buchung` AS `buchung`,`kost`.`kostenstelle` AS `kostenstelle`,`kost`.`waehrung` AS `waehrung`,`kost`.`miete` AS `miete`,`kost`.`wartung` AS `wartung`,`kost`.`notiz` AS `notiz` from (`mpi_kosten` `kost` left join `mpi_ablage` `ablg` on((`kost`.`ablageID` = `ablg`.`ablageID`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_licVer`*/;
/*!50001 DROP VIEW IF EXISTS `view_licVer`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_licVer` AS select `ver`.`verID` AS `verID`,`ver`.`softID` AS `softID`,`lic`.`lizenzID` AS `lizenzID`,sum(`lic`.`anzahl`) AS `anzVersion` from ((`mpi_version` `ver` join `con_verLic` `con` on((`ver`.`verID` = `con`.`verID`))) join `mpi_lizenz` `lic` on((`lic`.`lizenzID` = `con`.`lizenzID`))) where ((`lic`.`verID` is not null) and (`lic`.`status` = 1)) group by `lic`.`lizenzID` order by `lic`.`lizenzID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_lizenz`*/;
/*!50001 DROP VIEW IF EXISTS `view_lizenz`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_lizenz` AS select `view_instLic`.`tabID` AS `tabID`,`view_instLic`.`softID` AS `softID`,`view_instLic`.`verID` AS `verID`,`view_instLic`.`lizenzID` AS `lizenzID`,`view_instLic`.`inventar` AS `inventar`,`view_instLic`.`anzLizenz` AS `anzLizenz`,sum(`view_instLic`.`useLizenz`) AS `useLizenz`,if((`view_instLic`.`anzLizenz` = 0),0,(`view_instLic`.`anzLizenz` - sum(`view_instLic`.`useLizenz`))) AS `freeLizenz`,`view_instLic`.`modell` AS `modell` from `view_instLic` where (not(`view_instLic`.`lizenzID` in (select `con_verLic`.`lizenzID` from `con_verLic`))) group by `view_instLic`.`lizenzID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_pc`*/;
/*!50001 DROP VIEW IF EXISTS `view_pc`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_pc` AS select `mpi_geraete`.`tabID` AS `tabID`,`mpi_geraete`.`name` AS `name`,`mpi_geraete`.`art` AS `art` from `mpi_geraete` order by `mpi_geraete`.`name` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_reiter`*/;
/*!50001 DROP VIEW IF EXISTS `view_reiter`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_reiter` AS select (convert(`information_schema`.`tables`.`TABLE_NAME` using utf8) collate utf8_unicode_ci) AS `reiter`,`information_schema`.`tables`.`TABLE_TYPE` AS `table_type` from `information_schema`.`tables` where ((`information_schema`.`tables`.`TABLE_SCHEMA` = (select database())) and ((`information_schema`.`tables`.`TABLE_TYPE` = 'base table') or (`information_schema`.`tables`.`TABLE_TYPE` = 'view'))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_sendMail`*/;
/*!50001 DROP VIEW IF EXISTS `view_sendMail`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_sendMail` AS select `soft`.`software` AS `name`,`lic`.`lizenzID` AS `lizenzID`,`vert`.`vertragID` AS `vertragID`,`lic`.`expSupport` AS `expireDate`,1 AS `anz`,`lic`.`email` AS `email`,(cast('Support' as char charset utf8) collate utf8_unicode_ci) AS `body`,`vert`.`expiry` AS `expiry`,`vert`.`nachricht` AS `nachricht`,NULL AS `setExpLizenz` from ((`mpi_lizenz` `lic` left join `mpi_software` `soft` on((`lic`.`softID` = `soft`.`softID`))) left join `mpi_vertrag` `vert` on((`lic`.`vertragID` = `vert`.`vertragID`))) where ((`lic`.`expSupport` is not null) and (`lic`.`status` = 1) and (`lic`.`nachricht` = 1) and (`lic`.`email` like '%@%.%') and ((`vert`.`nachricht` = 0) or isnull(`vert`.`expiry`))) union all select `soft`.`software` AS `name`,`lic`.`lizenzID` AS `lizenzID`,`vert`.`vertragID` AS `vertragID`,`lic`.`expLizenz` AS `expireDate`,1 AS `anz`,`lic`.`email` AS `email`,(cast('String' as char charset utf8) collate utf8_unicode_ci) AS `body`,`vert`.`expiry` AS `expiry`,`vert`.`nachricht` AS `nachricht`,`vert`.`setExpLizenz` AS `setExpLizenz` from ((`mpi_lizenz` `lic` left join `mpi_software` `soft` on((`lic`.`softID` = `soft`.`softID`))) left join `mpi_vertrag` `vert` on((`lic`.`vertragID` = `vert`.`vertragID`))) where ((`lic`.`expLizenz` is not null) and (`lic`.`status` = 1) and (`lic`.`nachricht` = 1) and (`lic`.`email` like '%@%.%') and ((`vert`.`nachricht` = 0) or isnull(`vert`.`expiry`) or (`vert`.`setExpLizenz` = 0))) union all select `vert`.`vertrag` AS `name`,`lic`.`lizenzID` AS `lizenzID`,`vert`.`vertragID` AS `vertragID`,`vert`.`expiry` AS `expireDate`,count(`lic`.`lizenzID`) AS `anz`,`vert`.`email` AS `email`,(cast('Vertrag' as char charset utf8) collate utf8_unicode_ci) AS `body`,`vert`.`expiry` AS `expiry`,`vert`.`nachricht` AS `nachricht`,`vert`.`setExpLizenz` AS `setExpLizenz` from ((`mpi_vertrag` `vert` left join `mpi_lizenz` `lic` on((`vert`.`vertragID` = `lic`.`vertragID`))) left join `mpi_software` `soft` on((`lic`.`softID` = `soft`.`softID`))) where ((`vert`.`expiry` is not null) and (`vert`.`status` = 1) and (`vert`.`nachricht` = 1) and (`vert`.`email` like '%@%.%')) group by `vert`.`vertragID` order by `name` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_software`*/;
/*!50001 DROP VIEW IF EXISTS `view_software`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_software` AS select `view_lizenz`.`softID` AS `softID`,`view_lizenz`.`verID` AS `verID`,`view_lizenz`.`lizenzID` AS `lizenzID`,`view_lizenz`.`modell` AS `modell`,`view_lizenz`.`anzLizenz` AS `anzLizenz`,NULL AS `useLizenz`,`view_lizenz`.`useLizenz` AS `verbLizenz`,`view_lizenz`.`freeLizenz` AS `freeLizenz` from `view_lizenz` union all select `view_version`.`softID` AS `softID`,`view_version`.`verID` AS `verID`,`view_version`.`lizenzID` AS `lizenzID`,`view_version`.`modell` AS `modell`,`view_version`.`anzLizenz` AS `anzLizenz`,`view_version`.`useLizenz` AS `useLizenz`,`view_version`.`verbVersion` AS `verbLizenz`,`view_version`.`freeVersion` AS `freeLizenz` from `view_version` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_user`*/;
/*!50001 DROP VIEW IF EXISTS `view_user`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_user` AS select '000001' AS `userID`,'mpg_local' AS `login`,'MPG, version (mpg_local)' AS `sort` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_verLic`*/;
/*!50001 DROP VIEW IF EXISTS `view_verLic`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_verLic` AS select `ver`.`verID` AS `verID`,`lic`.`lizenzID` AS `lizenzID`,if((count(`lic`.`modell`) > 1),'Mixed',`lic`.`modell`) AS `modell`,sum(`lic`.`anzahl`) AS `anzLizenz` from (`mpi_version` `ver` left join `mpi_lizenz` `lic` on((`lic`.`verID` = `ver`.`verID`))) where ((`lic`.`verID` is not null) and (`lic`.`status` = 1)) group by `ver`.`verID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_verSumLic`*/;
/*!50001 DROP VIEW IF EXISTS `view_verSumLic`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_verSumLic` AS select `ver`.`verID` AS `verID`,sum(`lic`.`anzahl`) AS `anzLizenz` from ((`mpi_version` `ver` left join `con_verLic` `con` on((`con`.`verID` = `ver`.`verID`))) left join `mpi_lizenz` `lic` on((`lic`.`lizenzID` = `con`.`lizenzID`))) where (`lic`.`status` = '1') group by `ver`.`verID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_verVerb`*/;
/*!50001 DROP VIEW IF EXISTS `view_verVerb`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_verVerb` AS select `ver`.`verID` AS `verID`,sum(`vins`.`useLizenz`) AS `useLizenz` from (`mpi_version` `ver` join `view_instLic` `vins` on((`ver`.`verID` = `vins`.`verID`))) group by `ver`.`verID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_version`*/;
/*!50001 DROP VIEW IF EXISTS `view_version`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_version` AS select `ver`.`softID` AS `softID`,`ver`.`verID` AS `verID`,`ver`.`orderID` AS `orderID`,`vlic`.`lizenzID` AS `lizenzID`,`vlic`.`modell` AS `modell`,`vlic`.`anzLizenz` AS `anzLizenz`,`vuse`.`useLizenz` AS `useLizenz`,`shw`.`diffVersion` AS `diffVersion`,`shw`.`restVersion` AS `restVersion`,`shw`.`verbVersion` AS `verbVersion`,`shw`.`freeVersion` AS `freeVersion` from (((`mpi_version` `ver` left join `view_verVerb` `vuse` on((`vuse`.`verID` = `ver`.`verID`))) left join `view_verLic` `vlic` on((`vlic`.`verID` = `ver`.`verID`))) left join `show_version` `shw` on((`shw`.`verID` = `ver`.`verID`))) where `ver`.`verID` in (select `con_verLic`.`verID` from `con_verLic`) order by `ver`.`softID`,`ver`.`orderID`,`ver`.`version` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_vertrag`*/;
/*!50001 DROP VIEW IF EXISTS `view_vertrag`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_vertrag` AS select `mpi_lizenz`.`vertragID` AS `vertragID`,count(`mpi_lizenz`.`vertragID`) AS `anzVertrag` from `mpi_lizenz` where (`mpi_lizenz`.`vertragID` is not null) group by `mpi_lizenz`.`vertragID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

