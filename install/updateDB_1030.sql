-- run: mysql -u root -p mpidb_mpg_licman < updateDB_1030.sql
-- UPDATES immer von der niefrigsten bis zur hoechsten version ausfuehren
-- damit IF benutzt werden kann, wird eine prozedur erzeugt und am ende ausgefuehrt
-- 
-- views, funcs, procs nach moeglichkeit nur einmal in der max version ausfuehren

USE mpidb_mpg_licman;
DROP PROCEDURE IF EXISTS proc_update;
DELIMITER $$
CREATE PROCEDURE proc_update()
proc_label: BEGIN

-- mindest version vorhanden
IF ( SELECT MAX(version) FROM dataface__version ) < '1025' THEN
 LEAVE proc_label;
END IF;

-- CHANGES V1.0.30 :
-- *****************
-- fs::rsync      - color, stylesheet, focus, hotkey, design

IF ( SELECT MAX(version) FROM dataface__version ) < '1030' THEN

  -- keine db aenderung, nur fs

 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('1030');
END IF;


-- CHANGES V1.0.31 :
-- *****************
-- fs::rsync      - actions.ini in subdir's aufgeteilt
-- fs::rsync      - mod permission edit list_reiter nur fuer admin

 IF ( SELECT MAX(version) FROM dataface__version ) < '1031' THEN

  -- keine db aenderung, nur fs

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1031');
 END IF;


-- CHANGES V1.0.32 - 2016-05-23
-- ****************************
-- fs::rsync      - add field kostenstelle mpi_lizenz
-- db::mpi_lizenz - add field kostenstelle

 IF ( SELECT MAX(version) FROM dataface__version ) < '1032' THEN


  ALTER TABLE mpi_lizenz ADD kostenstelle VARCHAR(10) NULL AFTER artikelnummer, ADD INDEX (kostenstelle) ;
  ALTER TABLE list_kostenstelle CHANGE kostenstelle kostenstelle VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ;
  ALTER TABLE mpi_kosten CHANGE kostenstelle kostenstelle VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ;
  ALTER TABLE mpi_lizenz ADD CONSTRAINT lizenz_kosten_kostenstelle FOREIGN KEY (kostenstelle) REFERENCES list_kostenstelle (kostenstelle) ON DELETE SET NULL ON UPDATE CASCADE ;


 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('1032');
END IF;


-- CHANGES V1.0.33 - 2016-05-24
-- ****************************
 -- fs::rsync           - abgleich inventarnummer lizenz, add role and permission VIEW GROUP ..
 -- db::list_inventar   - neue Tabelle mit import SAP Inventar
 -- db::mpi_geraete     - add field unterNr
 -- db::list_reiter     - add new tables
 -- db::view_inventar   - add inventar anlage
 -- db::view_groupPSE   - special group view

IF ( SELECT MAX(version) FROM dataface__version ) < '1033' THEN

  -- add table list_inventar
  CREATE TABLE IF NOT EXISTS list_inventar (
    anlage varchar(8) COLLATE utf8_unicode_ci NOT NULL,
    unterNr varchar(4) COLLATE utf8_unicode_ci NOT NULL,
    invNummer varchar(30) COLLATE utf8_unicode_ci NOT NULL,
    bezeichnung varchar(50) COLLATE utf8_unicode_ci NOT NULL,
    standort varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
    wert decimal(7,2) DEFAULT NULL,
    invDatum date DEFAULT NULL,
    kostenstelle decimal(5,0) DEFAULT NULL,
    notiz varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    bearbeiter varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
    zeitstempel timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (anlage,`unterNr`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

  -- add feld unterNr
  -- ALTER TABLE mpi_geraete CHANGE inventar inventar VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ;
  ALTER TABLE mpi_lizenz ADD unterNr VARCHAR(4) NULL DEFAULT '0' AFTER inventar;

  -- add new tables
  INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES('list_inventar', 'Liste', 0, 0, 'Import SAP Inventar');
  INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES('view_inventar', 'Auswertung', 1, 0, 'Zeige alle Inventarnummern und deren Verlinkung zur Lizenztabelle');
 INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES('view_groupPSE', 'Auswertung', 1, 0, 'Zeige alle Lizenzen der Gruppe PSE');

  -- change view auf list_inventar, mit anlagen
  CREATE OR REPLACE VIEW view_inventar AS
  SELECT
   inv .*,
   CONCAT(inv.anlage,':',inv.unterNr) AS inventar,
   lic.lizenzID AS lizenzID,
   IF(lic.lizenzID IS NULL, 0, 1) AS exist
  FROM
   list_inventar AS inv
   LEFT JOIN mpi_lizenz AS lic ON inv.anlage = lic.inventar AND inv.unterNr = lic.unterNr
  ;

  -- add read only view group pse
  CREATE OR REPLACE VIEW view_groupPSE AS
  SELECT
   lic.*,
   IF(ver.status = 1 AND ver.setExpLizenz = 1 AND ver.expiry IS NOT NULL, ver.expiry, lic.expSupport) AS expiry
  FROM
   mpi_lizenz AS lic
   LEFT JOIN mpi_vertrag AS ver ON lic.vertragID = ver.vertragID
  WHERE
   (lic.kostenstelle = '10408' OR lic.kostenstelle = 'PSE') AND lic.status = 1
  ORDER BY
   lic.vertragID, lic.lizenzID
  ;


  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1033');
 END IF;


-- CHANGES V1.0.34 - 2016-06-03
-- ****************************
 -- fs::conf.ini        - change comment # to ;
 -- fs::cronjob         - update to mysqli driver

 IF ( SELECT MAX(version) FROM dataface__version ) < '1034' THEN

  -- nur file version

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1034');
 END IF;


-- CHANGES V1.0.35 - 2016-06-13
-- ****************************
 -- fs::conf         - link nach master geloescht, Sortierung von fields.ini nach ApplicationDelegate.php
 -- fs::fields.ini   - sql order raus

 IF ( SELECT MAX(version) FROM dataface__version ) < '1035' THEN

  -- nur file version

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1035');
 END IF;


-- CHANGES V1.0.36 - 2016-07-25
-- ****************************
 -- fs::view_inv_geraete  - verbindung bestellnummer nach DB Inventar

 IF ( SELECT MAX(version) FROM dataface__version ) < '1036' THEN

  -- add view in reiter
  INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES('view_inv_geraete', 'View', 0, 0, 'Beziehung Bestellnummer zur DB Inventar') ;
  -- create view_inv_geraete
  CREATE OR REPLACE VIEW view_inv_geraete AS
   SELECT
    '000001' AS gerID,
    'fakeAlone' AS name,
    'none' AS os,
    'R0.0' AS lagerort,
    'none' AS status,
    '0123456789' AS bestellnummer,
    'alone' AS bearbeiter,
    '2016-07-22 12:38:40' AS zeitstempel
  ;

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1036');
 END IF;


-- CHANGES V1.0.37 - 2016-11-22
-- ****************************
 -- fs::custom.css   - einige farbliche korrekturen bei relationships

 IF ( SELECT MAX(version) FROM dataface__version ) < '1037' THEN

  -- nur fs changed

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1037');
 END IF;


-- CHANGES V1.0.38 - 2017-01-19
-- ****************************
-- UPDATE - cronjob Lizenzablauf - mehr Info in Mail und Vertrag auch ohne Software und Lizenz sendbar
-- fs::view_sendMail  - neuer View
-- db::view_sendMail  - neuer View

IF ( SELECT MAX(version) FROM dataface__version ) < '1038' THEN

  -- change view_sendMail
  CREATE OR REPLACE VIEW view_sendMail AS
   SELECT
    soft.software AS name,
    lic.lizenzID,
    vert.vertragID,
    lic.expSupport AS expireDate,
    1 As anz,
    lic.email,
    CAST('Support' AS CHAR CHARACTER SET utf8) COLLATE utf8_unicode_ci AS body,
    vert.expiry, vert.nachricht, NULL AS setExpLizenz
   FROM
    mpi_lizenz AS lic
    LEFT JOIN mpi_software AS soft ON lic.softID = soft.softID
    LEFT JOIN mpi_vertrag AS vert ON lic.vertragID = vert.vertragID
   WHERE
    ( lic.expSupport IS NOT NULL ) AND
    ( lic.status = 1 ) AND
    ( lic.nachricht = 1 ) AND
    ( lic.email LIKE '%@%.%' ) AND
    (( vert.nachricht = 0 ) OR ( vert.expiry IS NULL ))
   UNION ALL
   SELECT
    soft.software AS name,
    lic.lizenzID,
    vert.vertragID,
    lic.expLizenz AS expireDate,
    1 As anz,
    lic.email,
    CAST('String' AS CHAR CHARACTER SET utf8) COLLATE utf8_unicode_ci AS body,
    vert.expiry, vert.nachricht, vert.setExpLizenz
   FROM
    mpi_lizenz AS lic
    LEFT JOIN mpi_software AS soft ON lic.softID = soft.softID
    LEFT JOIN mpi_vertrag AS vert ON lic.vertragID = vert.vertragID
   WHERE
    ( lic.expLizenz IS NOT NULL ) AND
    ( lic.status = 1 ) AND
    ( lic.nachricht = 1 ) AND
    ( lic.email LIKE '%@%.%' ) AND
    (( vert.nachricht = 0 ) OR ( vert.expiry IS NULL ) OR ( vert.setExpLizenz = 0 ))
   UNION ALL
   SELECT
    vert.vertrag AS name,
    lic.lizenzID,
    vert.vertragID,
    vert.expiry AS expireDate,
    count(lic.lizenzID) AS anz,
    vert.email,
    CAST('Vertrag' AS CHAR CHARACTER SET utf8) COLLATE utf8_unicode_ci AS body,
    vert.expiry, vert.nachricht, vert.setExpLizenz
   FROM
    mpi_vertrag AS vert
    LEFT JOIN mpi_lizenz AS lic ON vert.vertragID = lic.vertragID
    LEFT JOIN mpi_software AS soft ON lic.softID = soft.softID
   WHERE
    ( vert.expiry IS NOT NULL ) AND
    ( vert.status = 1 ) AND
    ( vert.nachricht = 1 ) AND
    ( vert.email LIKE '%@%.%' )
   GROUP BY
    vert.vertragID
   ORDER BY name
  ;

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1038');
 END IF;


-- CHANGES V1.1.00 - 2018-08-14
-- ****************************
-- UPDATE: change Autorisierung von mpi_user nach sys_user mit Rollentabelle
-- db::list_reiter - Aenderung tabellen anpassen
-- fs::sys_user,list_rolle - Anpassung neue Benutzerverwaltung

IF ( SELECT MAX(version) FROM dataface__version ) < '1100' THEN

   -- create table list_role
    -- DROP TABLE IF EXISTS `list_role`;
    CREATE TABLE IF NOT EXISTS `list_role` (
      `rolID` smallint(6) UNSIGNED ZEROFILL NOT NULL,
      `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
      `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    -- insert default roles
    INSERT IGNORE INTO `list_role` (`rolID`, `role`, `description`) VALUES
    (000001, 'NO ACCESS', 'No_Access'),
    (000002, 'READ ONLY', 'view, list, calendar, view xml, show all, find, navigate'),
    (000003, 'EDIT', 'READ_ONLY and edit, new record, remove, import, translate, copy'),
    (000004, 'DELETE', 'EDIT and delete and delete found'),
    (000005, 'OWNER', 'DELETE except navigate, new, and delete found'),
    (000006, 'REVIEWER', 'READ_ONLY and edit and translate'),
    (000007, 'USER', 'READ_ONLY and add new related record'),
    (000008, 'ADMIN', 'DELETE and xml_view'),
    (000009, 'MANAGER', 'ADMIN and manage, manage_migrate, manage_build_index, and install');
    -- unique, pri
    ALTER TABLE `list_role` ADD PRIMARY KEY IF NOT EXISTS (`rolID`), ADD UNIQUE KEY IF NOT EXISTS `role` (`role`);
    -- auto_increment
    ALTER TABLE `list_role` MODIFY `rolID` smallint(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

    -- create table sys_user
    -- DROP TABLE IF EXISTS `sys_user`;
    CREATE TABLE IF NOT EXISTS `sys_user` (
     `logID` smallint(6) UNSIGNED ZEROFILL NOT NULL,
     `login` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
     `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
     `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
     `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
     `bearbeiter` varchar(20) COLLATE utf8_unicode_ci NULL,
     `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
     ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    -- unique, pri
    ALTER TABLE `sys_user`
     ADD PRIMARY KEY IF NOT EXISTS (`logID`) USING BTREE,
     ADD UNIQUE KEY  IF NOT EXISTS `login` (`login`) USING BTREE,
     ADD UNIQUE KEY  IF NOT EXISTS `email` (`email`),
     ADD KEY         IF NOT EXISTS `role` (`role`);
    -- auto_increment
    ALTER TABLE `sys_user` MODIFY `logID` smallint(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
    -- Constraint
    ALTER TABLE `sys_user` ADD CONSTRAINT `sysUser_listRole` FOREIGN KEY IF NOT EXISTS (`role`) REFERENCES `list_role` (`role`);

    -- mpg-version ohne externe DB (licman,inv,gfk,chem,user) - Auslieferzustand
    CREATE OR REPLACE VIEW view_user AS
     SELECT
      '000001' AS userID, 'mpg_local' AS login, 'MPG, version (mpg_local)' AS sort;

    -- create if not exist list_reiter
    CREATE TABLE IF NOT EXISTS `list_reiter` (
     `autoID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
     `reiter` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
     `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
     `favorit` tinyint(1) NOT NULL DEFAULT '0',
     `history` tinyint(1) NOT NULL DEFAULT '0',
     `bedeutung` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
     PRIMARY KEY (`autoID`),
     UNIQUE KEY `reiter` (`reiter`),
     KEY `kategorie` (`kategorie`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Reiterlinks fuer Xataface' AUTO_INCREMENT=9 ;

    -- create if not exist list_katReiter
    CREATE TABLE IF NOT EXISTS `list_katReiter` (
     `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
     `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
     PRIMARY KEY (`autoID`),
     UNIQUE KEY `kategorie` (`kategorie`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;
    -- add entries in list_katReiter
    UPDATE list_katReiter SET kategorie = 'Autorisierung' WHERE kategorie = 'Authorisierung';
    INSERT IGNORE INTO `list_katReiter` (`kategorie`) VALUES ('Autorisierung');

    -- add entries in list_reiter
    INSERT IGNORE INTO `list_reiter` (`reiter`, `kategorie`, `favorit`, `history`, `bedeutung`) VALUES
     ('sys_user', 'Autorisierung', 1, 1, 'Autorisierung und Berechtigung Benutzer'),
     ('list_role', 'Autorisierung', 1, 0, 'Liste aller Berechtigungen (Rollen)');
    UPDATE `list_reiter` SET `bedeutung` = 'Auswahlliste fuer aktive und nicht abgelaufene Benutzer' WHERE `reiter` = 'view_user';

    -- copy inserts from old mpi_users
    INSERT IGNORE INTO sys_user (login, password, role, email, bearbeiter, zeitstempel) SELECT username, password, role, email, 'import', zeitstempel FROM mpi_users;

    -- del old table mpi_users (if all done and work)
    DROP TABLE IF EXISTS `mpi_users`;
    DROP TABLE IF EXISTS `mpi_users__history`;
    DELETE FROM `list_reiter` WHERE `reiter` = 'mpi_users';

 TRUNCATE dataface__version ;
 INSERT INTO dataface__version (version) VALUES ('1100') ;

END IF;


END;
$$
DELIMITER ;

-- execute updates
CALL proc_update();

-- weil nur noch unter mariadb funktioniert, sonst kein import der DB fuer mysqldb moeglich
DROP PROCEDURE IF EXISTS proc_update;

